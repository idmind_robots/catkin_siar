#!/usr/bin/env bash

echo "======" $(date) "======"

source /opt/ros/melodic/setup.bash
source /home/siar/catkin_siar/devel/setup.bash
cd /home/siar/catkin_siar
catkin_make

sleep 15

#setserial /dev/serial/by-id/usb-FTDI_MM232R_USB_MODULE_FTE48B69-if00-port0 low_latency
#setserial /dev/serial/by-id/usb-FTDI_MM232R_USB_MODULE_FTE48BAK-if00-port0 low_latency

export ROS_IP=192.168.1.240 #192.168.2.104
export ROS_HOSTNAME=siar-desktop
export ROS_MASTER_URI=http://siar-desktop:11311
roslaunch idmind_siar idmind_siar.launch
