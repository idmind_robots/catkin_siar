﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.IO.Ports;
using System.Threading;


namespace HWAPI_S.HwBoard
{
      public class SIAR_S_Board
    {
        private SerialPort serial = null;
        public int Serial_ports_open = 0;

        //Read 
        public int Elect_V;        
        public int Elect_I;
        public int Motor_V;        
        public int Motor_I;
        public int Charger_V;


        public int Elect_Bat_Voltage;
        public int Motor_Bat_Voltage;

        public int Instantaneous_Elect_Current;
        public int Instantaneous_Motor_Current;

        public int Integrated_Elect_Current;
        public int Integrated_Motor_Current;

        public byte Elect_Level;
        public byte Motor_Level;
        public int Elect_Bat_Min;
        public int Motor_Bat_Min;

        public byte Auxiliary_Pins_Values = 0;

        public byte Herkulex1_Status = 0;
        public byte Herkulex2_Status = 0;
        public byte Herkulex3_Status = 0;
        public byte Herkulex4_Status = 0;
        public byte Herkulex5_Status = 0;

        public byte Herkulex1_Torque = 0;
        public byte Herkulex2_Torque = 0;
        public byte Herkulex3_Torque = 0;
        public byte Herkulex4_Torque = 0;
        public byte Herkulex5_Torque = 0;

        public int Herkulex1_Position = 0;
        public int Herkulex2_Position = 0;
        public int Herkulex3_Position = 0;
        public int Herkulex4_Position = 0;
        public int Herkulex5_Position = 0;

        public byte Herkulex1_Temperature = 0;
        public byte Herkulex2_Temperature = 0;
        public byte Herkulex3_Temperature = 0;
        public byte Herkulex4_Temperature = 0;
        public byte Herkulex5_Temperature = 0;

        public char[] FirmwareBoard = new char[25];

        public SIAR_S_Board(string port, int baudRate) 
        {
            this.serial = new SerialPort(port, baudRate, Parity.None, 8, StopBits.One);
            this.serial.ReadTimeout = -1;
            this.serial.WriteTimeout = -1;
            Serial_ports_open = 1;            
            try
            {
                this.serial.Open();
            }
            catch (Exception ex)
            {
                Serial_ports_open = -1;
            }
        }


 
        public void Disconnect()
        {
            if (Serial_ports_open == 1)
            {
                serial.Close();
                Serial_ports_open = 0;
            }
        }

        /*********************************************/
        //             SIAR SET Function             */
        /*********************************************/

        /*********************************************/
        /*                SET_Lights                 */
        /*    [0X60][Lights]                         */
        /*    Lights=[XXXXXCBA]                      */
        /*    where:                                 */
        /*    A=1 Front Light ON                     */
        /*    A=0 Front Light OFF                    */
        /*    B=1 Rear Light ON                      */
        /*    B=0 Rear Light OFF                     */
        /*    C=1 Camera Ring Light ON               */
        /*    C=0 Camera Ring Light OFF              */
        /*    receive:                               */
        /*    [0x60][Sent_number][Chsum_H][Chsum_L]  */
        /*********************************************/
    
        public int SET_Lights(byte Lights) //SET SIAR Lights
        {
            int Check_sum = 0;
            int Bytes_sum;
            byte[] writebuffer = new byte[2];
            serial.DiscardInBuffer();

            writebuffer[0] = 0x60;
            writebuffer[1] = (byte)Lights;


            serial.Write(writebuffer, 0, writebuffer.Length);
            int contador = 0;
            while (serial.BytesToRead < 4 && contador < 6000) contador++;
            if (serial.BytesToRead == 4)
            {
                byte[] buffer = new byte[30];
                serial.Read(buffer, 0, serial.BytesToRead);
                Check_sum = (int)buffer[2] * 0x0100 + (int)buffer[3] * 0x0001;
                Bytes_sum = (int)buffer[0] + (int)buffer[1];
                if (buffer[0] == 0x60 && Check_sum == Bytes_sum)
                {
                    return (1);
                }
                else return (-1);
            }
            else return (-1);
        }

        /*********************************************/
        /*           Set_Aux_Pins_Direction          */
        /*    [0X61][Set_Aux_Pins_Direction]         */
        /*    Aux_Pins_Direction=[XXFEDCBA]          */
        /*    where:                                 */
        /*    A to F - Pin F0 to F5                  */
        /*    A to F = 0=O output                    */
        /*    A to F = 1=O  input                    */
        /*    receive:                               */
        /*    [0x61][Sent_number][Chsum_H][Chsum_L]  */
        /*********************************************/        

        public int SET_Aux_Pins_Direction(byte Aux_Pins_Direction) //SET SIAR Aux Pins Direction
        {
            int Check_sum = 0;
            int Bytes_sum;
            byte[] writebuffer = new byte[2];
            serial.DiscardInBuffer();

            writebuffer[0] = 0x61;
            writebuffer[1] = (byte)Aux_Pins_Direction;


            serial.Write(writebuffer, 0, writebuffer.Length);
            int contador = 0;
            while (serial.BytesToRead < 4 && contador < 6000) contador++;
            if (serial.BytesToRead == 4)
            {
                byte[] buffer = new byte[30];
                serial.Read(buffer, 0, serial.BytesToRead);
                Check_sum = (int)buffer[2] * 0x0100 + (int)buffer[3] * 0x0001;
                Bytes_sum = (int)buffer[0] + (int)buffer[1];
                if (buffer[0] == 0x31 && Check_sum == Bytes_sum)
                {
                    return (1);
                }
                else return (-1);
            }
            else return (-1);
        }

        /*********************************************/
        /*           Set_Aux_Pins_Values             */
        /*    [0X62][Aux_Pins_Values]                */
        /*    Aux_Pins_Values=[XXFEDCBA]             */
        /*    where:                                 */
        /*    A to F - Pin F0 to F5                  */
        /*    A to F = 0 -> 0V                       */
        /*    A to F = 1 -> 5V                       */
        /*    receive:                               */
        /*    [0x62][Sent_number][Chsum_H][Chsum_L]  */
        /*********************************************/

        public int SET_Aux_Pins_Values(byte Aux_Pins_Values) //SET SIAR Aux Pins Values
        {
            int Check_sum = 0;
            int Bytes_sum;
            byte[] writebuffer = new byte[2];
            serial.DiscardInBuffer();

            writebuffer[0] = 0x62;
            writebuffer[1] = (byte)Aux_Pins_Values;


            serial.Write(writebuffer, 0, writebuffer.Length);
            int contador = 0;
            while (serial.BytesToRead < 4 && contador < 6000) contador++;
            if (serial.BytesToRead == 4)
            {
                byte[] buffer = new byte[30];
                serial.Read(buffer, 0, serial.BytesToRead);
                Check_sum = (int)buffer[2] * 0x0100 + (int)buffer[3] * 0x0001;
                Bytes_sum = (int)buffer[0] + (int)buffer[1];
                if (buffer[0] == 0x32 && Check_sum == Bytes_sum)
                {
                    return (1);
                }
                else return (-1);
            }
            else return (-1);
        }

        /*********************************************/
        //             SIAR Set Herkulex Commands    */
        /*********************************************/

        /*********************************************/
        /*           SET_Herkulex_Torque             */
        /*    [0X30][Herkulex_ID][Torque_Value]      */
        /*    where:                                 */
        /*    Herkulex_ID=[0x00] to [0x04]           */
        /*    Torque_Value=[0x00] Torque Off         */
        /*    Torque_Value=[0x60] Torque On          */
        /*    Torque_Value=[0x40] Torque Brake       */
        /*    receive:                               */
        /*    [0x30][Sent_number][Chsum_H][Chsum_L]  */
        /*********************************************/

        public int SET_Herkulex_Torque(byte Herkulex_ID,byte Torque_Value ) //SET SIAR Herkulex Torque value
        {
            int Check_sum = 0;
            int Bytes_sum;
            byte[] writebuffer = new byte[3];
            serial.DiscardInBuffer();

            writebuffer[0] = 0x30;
            writebuffer[1] = (byte)Herkulex_ID;
            writebuffer[2] = (byte)Torque_Value;


            serial.Write(writebuffer, 0, writebuffer.Length);
            int contador = 0;
            while (serial.BytesToRead < 4 && contador < 56000) contador++;
            if (serial.BytesToRead == 4)
            {
                byte[] buffer = new byte[30];
                serial.Read(buffer, 0, serial.BytesToRead);
                Check_sum = (int)buffer[2] * 0x0100 + (int)buffer[3] * 0x0001;
                Bytes_sum = (int)buffer[0] + (int)buffer[1];
                if (buffer[0] == 0x30 && Check_sum == Bytes_sum)
                {
                    return (1);
                }
                else return (-1);
            }
            else return (-1);
        }

        /*********************************************/
        /*           SET_Herkulex_Position           */
        /*    [0X31][Herkulex_ID][Position_H]        */
        /*    [Position_L][Control_Time]             */
        /*    where:                                 */
        /*    Herkulex_ID=[0x00] to [0x04]           */
        /*    Position_H:Position_H is the position  */
        /*    ID:0x00,0x03,0x04 [0,1024]             */
        /*    ID:0x01,0x02      [0,2048]             */        
        /*    receive:                               */
        /*    [0x31][Sent_number][Chsum_H][Chsum_L]  */
        /*********************************************/


        public int SET_Herkulex_Position(byte Herkulex_ID, int Position, byte Control_Time) //SET SIAR New Position
        {
            int Check_sum = 0;
            int Bytes_sum;
            byte[] writebuffer = new byte[5];
            serial.DiscardInBuffer();

            writebuffer[0] = 0x31;
            writebuffer[1] = (byte)Herkulex_ID;
            writebuffer[2] = (byte)(Position >> 8);
            writebuffer[3] = (byte)(Position & 0x00FF);
            writebuffer[4] = (byte)Control_Time;


            serial.Write(writebuffer, 0, writebuffer.Length);
            int contador = 0;
            while (serial.BytesToRead < 4 && contador < 56000) contador++;
            //while (serial.BytesToRead < 4) contador++;
            if (serial.BytesToRead == 4)
            {
                byte[] buffer = new byte[30];
                serial.Read(buffer, 0, serial.BytesToRead);
                Check_sum = (int)buffer[2] * 0x0100 + (int)buffer[3] * 0x0001;
                Bytes_sum = (int)buffer[0] + (int)buffer[1];
                if (buffer[0] == 0x31 && Check_sum == Bytes_sum)
                {
                    return (1);
                }
                else return (-1);
            }
            else return (-1);
        }

        /*********************************************/
        /*  SET_Herkulex_Clear_Status_error          */
        /*    [0X32][Herkulex_ID]                    */        
        /*    where:                                 */
        /*    Herkulex_ID=[0x00] to [0x04]           */
        /*    receive:                               */
        /*    [0x31][Sent_number][Chsum_H][Chsum_L]  */
        /*********************************************/

        public int SET_Herkulex_Clear_Status_error(byte Herkulex_ID) //SET SIAR Clear Status Error
        {
            int Check_sum = 0;
            int Bytes_sum;
            byte[] writebuffer = new byte[2];
            serial.DiscardInBuffer();

            writebuffer[0] = 0x32;
            writebuffer[1] = (byte)Herkulex_ID;

            serial.Write(writebuffer, 0, writebuffer.Length);
            int contador = 0;
            while (serial.BytesToRead < 4 && contador < 56000) contador++;
            if (serial.BytesToRead == 4)
            {
                byte[] buffer = new byte[30];
                serial.Read(buffer, 0, serial.BytesToRead);
                Check_sum = (int)buffer[2] * 0x0100 + (int)buffer[3] * 0x0001;
                Bytes_sum = (int)buffer[0] + (int)buffer[1];
                if (buffer[0] == 0x32 && Check_sum == Bytes_sum)
                {
                    return (1);
                }
                else return (-1);
            }
            else return (-1);
        }



        /*********************************************/
        //             SIAR Get Herkulex Commands    */
        /*********************************************/
        /*********************************************/
        /*        Get_Herkulex_Status_Error          */
        /*    [0X40]                                 */
        /*    receive:                               */
        /*    [0x40][StE_0x00][StE_0x01][StE_0x02]   */
        /*    [StE_0x03][StE_0x04][Sent_number]      */
        /*    [Sent_number][Chsum_H][Chsum_L]        */
        /*    where:                                 */
        /*    [StE_0x00] to [StE_0x04] is the Status */
        /*    of each servo ID                       */
        /*********************************************/

        public int Get_Herkulex_Status_Error() //Get Herkulex Status Error
        {
            int Check_sum = 0;
            int Bytes_sum = 0;
            byte[] writebuffer = new byte[1];
            serial.DiscardInBuffer();

            writebuffer[0] = 0x40;

            serial.Write(writebuffer, 0, writebuffer.Length);
            int contador = 0;

            //while (serial.BytesToRead < 9 && contador < 6000) contador++;
            while (serial.BytesToRead < 9 && contador < 56000) contador++;
            if (serial.BytesToRead == 9)
            {
                byte[] buffer = new byte[30];
                serial.Read(buffer, 0, serial.BytesToRead);

                for (int i=0; i<7; i++) Bytes_sum+=(int)buffer[i];
                Check_sum = (int)buffer[7]*0x0100 + (int)buffer[8]*0x0001;

                if (buffer[0] == 0x40 && Check_sum == Bytes_sum)
                {
                    Herkulex1_Status = buffer[1];
                    Herkulex2_Status = buffer[2];
                    Herkulex3_Status = buffer[3];
                    Herkulex4_Status = buffer[4];
                    Herkulex5_Status = buffer[5];
                    return(1);
                }
                else
                    return (-1);
            }
            else return (-1);
        }

        /*********************************************/
        /*        Get_Herkulex_Torque                */
        /*    [0X41]                                 */
        /*    receive:                               */
        /*    [0x41][Tor_0x00][Tor_0x01][Tor_0x02]   */
        /*    [Tor_0x03][Tor_0x04][Sent_number]      */
        /*    [Sent_number][Chsum_H][Chsum_L]        */
        /*    where:                                 */
        /*    [Tor_0x0X] is the Status of each servo */
        /*    ID                                     */
        /*********************************************/

        public int Get_Herkulex_Torque() //Get Herkulex Torque
        {
            int Check_sum = 0;
            int Bytes_sum = 0;
            byte[] writebuffer = new byte[1];
            serial.DiscardInBuffer();

            writebuffer[0] = 0x41;

            serial.Write(writebuffer, 0, writebuffer.Length);
            int contador = 0;

            while (serial.BytesToRead < 9 && contador < 50000) contador++;
            if (serial.BytesToRead == 9)
            {
                byte[] buffer = new byte[30];
                serial.Read(buffer, 0, serial.BytesToRead);

                for (int i = 0; i < 7; i++) Bytes_sum += (int)buffer[i];
                Check_sum = (int)buffer[7] * 0x0100 + (int)buffer[8] * 0x0001;

                if (buffer[0] == 0x41 && Check_sum == Bytes_sum)
                {
                    Herkulex1_Torque = buffer[1];
                    Herkulex2_Torque = buffer[2];
                    Herkulex3_Torque = buffer[3];
                    Herkulex4_Torque = buffer[4];
                    Herkulex5_Torque = buffer[5];
                    return (1);
                }
                else return (-1);
            }
            else return (-1);
        }

        /*********************************************/
        /*        Get_Herkulex_Position              */
        /*    [0X42]                                 */
        /*    receive:                               */
        /*    [0x42][POH_0x00][POL_0x00][POH_0x01]   */
        /*    [POL_0x01][POH_0x02][POL_0x02]         */
        /*    [POH_0x03][POL_0x03][POH_0x04]         */
        /*    [POL_0x04][Sent_number][Chsum_H]       */
        /*    [Chsum_L]                              */
        /*    where:                                 */
        /*    [POH_0x0X:POH_0x0X] is the Position    */
        /*    of each servo ID                       */
        /*********************************************/

        public int Get_Herkulex_Position() //Get Herkulex Position
        {
            int Check_sum = 0;
            int Bytes_sum = 0;
            byte[] writebuffer = new byte[1];
            serial.DiscardInBuffer();

            writebuffer[0] = 0x42;

            serial.Write(writebuffer, 0, writebuffer.Length);
            int contador = 0;

            //while (serial.BytesToRead < 14 && contador < 6000) contador++;
            while (serial.BytesToRead < 14 && contador < 56000) contador++;
            if (serial.BytesToRead == 14)
            {
                byte[] buffer = new byte[30];
                serial.Read(buffer, 0, serial.BytesToRead);

                for (int i = 0; i < 12; i++) Bytes_sum += (int)buffer[i];
                Check_sum = (int)buffer[12] * 0x0100 + (int)buffer[13] * 0x0001;

                if (buffer[0] == 0x42 && Check_sum == Bytes_sum)
                {
                    Herkulex1_Position = (int)buffer[1] * 0x0100 + (int)buffer[2] * 0x0001;
                    Herkulex2_Position = (int)buffer[3] * 0x0100 + (int)buffer[4] * 0x0001;
                    Herkulex3_Position = (int)buffer[5] * 0x0100 + (int)buffer[6] * 0x0001;
                    Herkulex4_Position = (int)buffer[7] * 0x0100 + (int)buffer[8] * 0x0001;
                    Herkulex5_Position = (int)buffer[9] * 0x0100 + (int)buffer[10] * 0x0001;
                    return (1);
                }
                else return (-1);
            }
            else return (-1);
        }

        /*********************************************/
        /*        Get_Herkulex_Temperature           */
        /*    [0X43]                                 */
        /*    receive:                               */
        /*    [0x43][Tem_0x00][Tem_0x01][Tem_0x02]   */
        /*    [Tem_0x03][Tem_0x04][Sent_number]      */
        /*    [Chsum_H][Chsum_L]                     */
        /*    where:                                 */
        /*    [Tem_0x0X] is the Temperature          */
        /*    of each servo ID                       */
        /*********************************************/

        public int Get_Herkulex_Temperature() //Get Herkulex Status Error
        {
            int Check_sum = 0;
            int Bytes_sum = 0;
            byte[] writebuffer = new byte[1];
            serial.DiscardInBuffer();

            writebuffer[0] = 0x43;

            serial.Write(writebuffer, 0, writebuffer.Length);
            int contador = 0;

            while (serial.BytesToRead < 9 && contador<50000) contador++;
            if (serial.BytesToRead == 9)
            {
                byte[] buffer = new byte[30];
                serial.Read(buffer, 0, serial.BytesToRead);

                for (int i = 0; i < 7; i++) Bytes_sum += (int)buffer[i];
                Check_sum = (int)buffer[7] * 0x0100 + (int)buffer[8] * 0x0001;

                if (buffer[0] == 0x43 && Check_sum == Bytes_sum)
                {
                    Herkulex1_Temperature = buffer[1];
                    Herkulex2_Temperature = buffer[2];
                    Herkulex3_Temperature = buffer[3];
                    Herkulex4_Temperature = buffer[4];
                    Herkulex5_Temperature = buffer[5];
                    return (1);
                }
                else return (-1);
            }
            else return (-1);
        }

        /*********************************************/
        //             SIAR Get Commands             */
        /*********************************************/

        /*********************************************/
        /*        Get_System_Power_Supply            */
        /*    [0X51]                                 */
        /*    receive:                               */
        /*    [0x51][Elect_V_H][Elect_V_L]           */
        /*    [Elect_I_H][Elect_I_L][Motor_V_H]      */
        /*    [Motor_V_L][Motor_I_H][Motor_I_L]      */
        /*    [Cable_V_H][Cable_V_L][Sent_number]    */
        /*    [Chsum_H][Chsum_L]                     */
        /*    where:                                 */
        /*    [Elect_V_H:Elect_V_L] is El. Volt x 10 */
        /*    of each servo ID                       */
        /*********************************************/


        public int Get_System_Power_Supply() //Get System Power Supply 
        {
            int Check_sum = 0;
            int Bytes_sum = 0;
            byte[] writebuffer = new byte[1];
            serial.DiscardInBuffer();

            writebuffer[0] = 0x51;

            serial.Write(writebuffer, 0, writebuffer.Length);
            int contador = 0;

            while (serial.BytesToRead < 14 && contador < 6000) contador++;
            if (serial.BytesToRead == 14)
            {
                byte[] buffer = new byte[30];
                serial.Read(buffer, 0, serial.BytesToRead);

                for (int i = 0; i < 12; i++) Bytes_sum += (int)buffer[i];
                Check_sum = (int)buffer[12] * 0x0100 + (int)buffer[13] * 0x0001;

                if (buffer[0] == 0x51 && Check_sum == Bytes_sum)
                {
                    Elect_V = (int)buffer[1] * 0x0100 + (int)buffer[2] * 0x0001;
                    Elect_I = (int)buffer[3] * 0x0100 + (int)buffer[4] * 0x0001;
                    Motor_V = (int)buffer[5] * 0x0100 + (int)buffer[6] * 0x0001;
                    Motor_I = (int)buffer[7] * 0x0100 + (int)buffer[8] * 0x0001;
                    Charger_V = (int)buffer[9] * 0x0100 + (int)buffer[10] * 0x0001;

                    return (1);
                }
                else return (-1);
            }
            else return (-1);
        }

        /*********************************************/
        /*        Get_Batteries_Voltage              */
        /*    [0X52]                                 */
        /*    receive:                               */
        /*    [0x52][E_Bat_Volt_H][E_Bat_Volt_L]     */
        /*    [M_Bat_Volt_H][M_Bat_Volt_L]           */
        /*    [Sent_number][Chsum_H][Chsum_L]        */
        /*    where:                                 */
        /*    [E_Bat_Volt_H:E_Bat_Volt_L] is Electr. */
        /*    Battery Volt x 10                      */
        /*    [M_Bat_Volt_H:M_Bat_Volt_L] is Motor   */
        /*    Battery Volt x 10                      */        
        /*********************************************/

        public int Get_Batteries_Voltage() //Get Batteries Voltage
        {
            int Check_sum = 0;
            int Bytes_sum = 0;
            byte[] writebuffer = new byte[1];
            serial.DiscardInBuffer();

            writebuffer[0] = 0x52;

            serial.Write(writebuffer, 0, writebuffer.Length);
            int contador = 0;

            while (serial.BytesToRead < 8 && contador < 6000) contador++;
            if (serial.BytesToRead == 8)
            {
                byte[] buffer = new byte[30];
                serial.Read(buffer, 0, serial.BytesToRead);

                for (int i = 0; i < 6; i++) Bytes_sum += (int)buffer[i];
                Check_sum = (int)buffer[6] * 0x0100 + (int)buffer[7] * 0x0001;

                if (buffer[0] == 0x52 && Check_sum == Bytes_sum)
                {
                    Elect_Bat_Voltage = (int)buffer[1] * 0x0100 + (int)buffer[2] * 0x0001;
                    Motor_Bat_Voltage = (int)buffer[3] * 0x0100 + (int)buffer[4] * 0x0001;

                    return (1);
                }
                else return (-1);
            }
            else return (-1);
        }

        /*********************************************/
        /*        Get_Instantaneous_Current          */
        /*    [0X53]                                 */
        /*    receive:                               */
        /*    [0x53][Inst_E_Cur_H][Inst_E_Cur_L]     */
        /*    [Inst_M_Cur_H][Inst_M_Cur_L]           */
        /*    [Sent_number][Chsum_H][Chsum_L]        */
        /*    where:                                 */
        /*    [Inst_E_Cur_H:Inst_E_Cur_L] is Electr. */
        /*    Instantaneous Current mA               */
        /*    [Inst_M_Cur_H:Inst_M_Cur_L] is Motor   */
        /*    Instantaneous Current mA               */
        /*********************************************/

        public int Get_Instantaneous_Current() //Get Instantaneous Electronic and Motor Current
        {
            int Check_sum = 0;
            int Bytes_sum = 0;
            byte[] writebuffer = new byte[1];
            serial.DiscardInBuffer();

            writebuffer[0] = 0x53;

            serial.Write(writebuffer, 0, writebuffer.Length);
            int contador = 0;

            while (serial.BytesToRead < 8 && contador < 6000) contador++;
            if (serial.BytesToRead == 8)
            {
                byte[] buffer = new byte[30];
                serial.Read(buffer, 0, serial.BytesToRead);

                for (int i = 0; i < 6; i++) Bytes_sum += (int)buffer[i];
                Check_sum = (int)buffer[6] * 0x0100 + (int)buffer[7] * 0x0001;

                if (buffer[0] == 0x53 && Check_sum == Bytes_sum)
                {
                    Instantaneous_Elect_Current = (int)buffer[1] * 0x0100 + (int)buffer[2] * 0x0001;
                    Instantaneous_Motor_Current = (int)buffer[3] * 0x0100 + (int)buffer[4] * 0x0001;
                    return (1);
                }
                else return (-1);
            }
            else return (-1);
        }

        /*********************************************/
        /*        Get_Integrated_Current             */
        /*    [0X54]                                 */
        /*    receive:                               */
        /*    [0x54][Int_E_Cur_H][Int_E_Cur_L]       */
        /*    [Int_M_Cur_H][Int_M_Cur_L]             */
        /*    [Sent_number][Chsum_H][Chsum_L]        */
        /*    where:                                 */
        /*    [Int_E_Cur_H:Int_E_Cur_L] is Electr.   */
        /*    Integrated over time Current mA        */
        /*    [Inst_M_Cur_H:Inst_M_Cur_L] is Motor   */
        /*    Integrated over time Current mA        */
        /*********************************************/

        public int Get_Integrated_Current() //Get Integrated electronic and Motor Current
        {
            int Check_sum = 0;
            int Bytes_sum = 0;
            byte[] writebuffer = new byte[1];
            serial.DiscardInBuffer();

            writebuffer[0] = 0x54;

            serial.Write(writebuffer, 0, writebuffer.Length);
            int contador = 0;

            while (serial.BytesToRead < 8 && contador < 6000) contador++;
            if (serial.BytesToRead == 8)
            {
                byte[] buffer = new byte[30];
                serial.Read(buffer, 0, serial.BytesToRead);

                for (int i = 0; i < 6; i++) Bytes_sum += (int)buffer[i];
                Check_sum = (int)buffer[6] * 0x0100 + (int)buffer[7] * 0x0001;

                if (buffer[0] == 0x54 && Check_sum == Bytes_sum)
                {
                    Integrated_Elect_Current = (int)buffer[1] * 0x0100 + (int)buffer[2] * 0x0001;
                    Integrated_Motor_Current = (int)buffer[3] * 0x0100 + (int)buffer[4] * 0x0001;
                    return (1);
                }
                else return (-1);
            }
            else return (-1);
        }

        /*********************************************/
        /*        Get_Level                          */
        /*    [0X55]                                 */
        /*    receive:                               */
        /*    [0x55][E_Level][E_Remain_Bat]          */
        /*    [M_Level][M_Remain_Bat]                */
        /*    [Sent_number][Chsum_H][Chsum_L]        */
        /*    where:                                 */
        /*    [E_Level] is the Level of charge of    */
        /*    Elect. Battery                         */
        /*    [E_Remain_Bat] is the Elect. Battery   */
        /*    remaing working minutes                */
        /*    [M_Level] is the Level of charge of    */
        /*    Motor Battery                          */
        /*    [M_Remain_Bat] is the Motor Battery    */
        /*    remaing working minutes                */
        /*********************************************/

        public int Get_Level() //Get Level and remmaining operation time
        {
            int Check_sum = 0;
            int Bytes_sum = 0;
            byte[] writebuffer = new byte[1];
            serial.DiscardInBuffer();

            writebuffer[0] = 0x55;

            serial.Write(writebuffer, 0, writebuffer.Length);
            int contador = 0;

            while (serial.BytesToRead < 10 && contador < 6000) contador++;
            if (serial.BytesToRead == 10)
            {
                byte[] buffer = new byte[30];
                serial.Read(buffer, 0, serial.BytesToRead);

                for (int i = 0; i < 8; i++) Bytes_sum += (int)buffer[i];
                Check_sum = (int)buffer[8] * 0x0100 + (int)buffer[9] * 0x0001;

                if (buffer[0] == 0x55 && Check_sum == Bytes_sum)
                {
                    Elect_Level = (byte)buffer[1];
                    Elect_Bat_Min = (int)buffer[2] * 0x0100 + (int)buffer[3] * 0x0001;
                    Motor_Level = (byte)buffer[4];
                    Motor_Bat_Min = (int)buffer[5] * 0x0100 + (int)buffer[6] * 0x0001;
                    return (1);
                }
                else return (-1);
            }
            else return (-1);
        }

        /*********************************************/
        /*        Get_Auxiliary_Pins_Values          */
        /*    [0X56]                                 */
        /*    receive:                               */
        /*    [0x56][Aux_Pins_Values]                */
        /*    [Sent_number][Chsum_H][Chsum_L]        */
        /*    Aux_Pins_Values=[XXFEDCBA]             */
        /*    where:                                 */
        /*    A to F = 0 -> 0V                       */
        /*    A to F = 1 -> 5V                       */
        /*    [0x56][E_Level][E_Remain_Bat]          */
        /*    [M_Level][M_Remain_Bat]                */
        /*    [Sent_number][Chsum_H][Chsum_L]        */
        /*********************************************/


        public int Get_Auxiliary_Pins_Values() //Get Auxiliary Pins Values
        {
            int Check_sum = 0;
            int Bytes_sum = 0;
            byte[] writebuffer = new byte[1];
            serial.DiscardInBuffer();

            writebuffer[0] = 0x56;

            serial.Write(writebuffer, 0, writebuffer.Length);
            int contador = 0;

            while (serial.BytesToRead < 5 && contador < 6000) contador++;
            if (serial.BytesToRead == 5)
            {
                byte[] buffer = new byte[30];
                serial.Read(buffer, 0, serial.BytesToRead);

                for (int i = 0; i < 3; i++) Bytes_sum += (int)buffer[i];
                Check_sum = (int)buffer[3] * 0x0100 + (int)buffer[4] * 0x0001;

                if (buffer[0] == 0x56 && Check_sum == Bytes_sum)
                {
                    Auxiliary_Pins_Values = (byte)buffer[1];                    
                    return (1);
                }
                else return (-1);
            }
            else return (-1);
        }


        // 0x20  GET  Firmware version number

        /*********************************************/
        /*        Get_firmware_version_number        */
        /*    [0X20]                                 */
        /*    receive:                               */
        /*    [0x20][FirmWare-> 25 Bytes]            */
        /*    [Sent_number][Chsum_H][Chsum_L]        */
        /*    where FirmWare:                        */
        /*     "Board1 fw 1.00 2017/07/11"           */
        /*********************************************/

        public int Get_firmware_version_number()  //Firmware Version
        {
            int i = 0;
            serial.DiscardInBuffer();

            serial.Write(new byte[] { 0x20 }, 0, 1);
            int Comunication_counter = 0;
            int Communication_error = 0;
            while (serial.BytesToRead < 29 && Comunication_counter < 10000) Comunication_counter++;
            if (serial.BytesToRead == 29)
            {
                byte[] buffer = new byte[serial.BytesToRead];
                serial.Read(buffer, 0, (int)buffer.Length);
                if (buffer[0] == 0x20)
                {

                    Communication_error = 0;
                    for (i = 0; i < 25; i++)
                    {
                        FirmwareBoard[i] = Convert.ToChar(buffer[i + 1]);
                    }

                }
                else Communication_error = -1;
            }
            else
                Communication_error = -1;
            return (Communication_error);
        }







    }
}
