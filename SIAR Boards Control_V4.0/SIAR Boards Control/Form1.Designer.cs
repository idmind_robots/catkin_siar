﻿namespace SIAR_Boards_Control
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.Connect_Button = new System.Windows.Forms.Button();
            this.Motor_groupBox = new System.Windows.Forms.GroupBox();
            this.Linear_groupBox = new System.Windows.Forms.GroupBox();
            this.Linear_Motor_Velocity_vScrollBar = new System.Windows.Forms.VScrollBar();
            this.Linear_Motor_Velocity_label = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.Linear_Motor_Position_vScrollBar = new System.Windows.Forms.VScrollBar();
            this.Linear_Motor_Position_label = new System.Windows.Forms.Label();
            this.Hardstop_Status_checkBox = new System.Windows.Forms.CheckBox();
            this.Hardstop_Timer_label = new System.Windows.Forms.Label();
            this.Control_Stream_radioButton = new System.Windows.Forms.RadioButton();
            this.Hardstop_radioButton = new System.Windows.Forms.RadioButton();
            this.Ind_Motor_Stream_radioButton = new System.Windows.Forms.RadioButton();
            this.label61 = new System.Windows.Forms.Label();
            this.Potentiometer_label = new System.Windows.Forms.Label();
            this.Linear_Position_Ticks_label = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.Hardstop_Timer_vScrollBar = new System.Windows.Forms.VScrollBar();
            this.Rear_Right_Ticks_label = new System.Windows.Forms.Label();
            this.Mid_Right_Ticks_label = new System.Windows.Forms.Label();
            this.Front_Right_Ticks_label = new System.Windows.Forms.Label();
            this.Rear_Left_Ticks_label = new System.Windows.Forms.Label();
            this.Mid_Left_Ticks_label = new System.Windows.Forms.Label();
            this.Front_Left_Ticks_label = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.Robot_Control_groupBox = new System.Windows.Forms.GroupBox();
            this.Right_Wheel_Value_label = new System.Windows.Forms.Label();
            this.Left_Wheel_Value_label = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.Linear_Velocity_label = new System.Windows.Forms.Label();
            this.Angular_Velocity_label = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.Linear_Velocity_vScrollBar = new System.Windows.Forms.VScrollBar();
            this.Angular_Velocity_vScrollBar = new System.Windows.Forms.HScrollBar();
            this.Ind_Control_groupBox = new System.Windows.Forms.GroupBox();
            this.Rear_Right_label = new System.Windows.Forms.Label();
            this.Front_Left_hScrollBar = new System.Windows.Forms.HScrollBar();
            this.Mid_Right_label = new System.Windows.Forms.Label();
            this.Mid_Left_hScrollBar = new System.Windows.Forms.HScrollBar();
            this.Front_Right_label = new System.Windows.Forms.Label();
            this.Rear_Left_hScrollBar = new System.Windows.Forms.HScrollBar();
            this.Rear_Left_label = new System.Windows.Forms.Label();
            this.Front_Right_hScrollBar = new System.Windows.Forms.HScrollBar();
            this.Mid_Left_label = new System.Windows.Forms.Label();
            this.Mid_Right_hScrollBar = new System.Windows.Forms.HScrollBar();
            this.Front_Left_label = new System.Windows.Forms.Label();
            this.Rear_Right_hScrollBar = new System.Windows.Forms.HScrollBar();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.Elect_V_label = new System.Windows.Forms.Label();
            this.Motor_V_label = new System.Windows.Forms.Label();
            this.Charger_V_label = new System.Windows.Forms.Label();
            this.Elect_I_label = new System.Windows.Forms.Label();
            this.Motor_I_label = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.Electronic_Battery_Voltage_label = new System.Windows.Forms.Label();
            this.Motor_Battery_Voltage_label = new System.Windows.Forms.Label();
            this.Sensor_groupBox = new System.Windows.Forms.GroupBox();
            this.B3_radioButton = new System.Windows.Forms.RadioButton();
            this.B2_radioButton = new System.Windows.Forms.RadioButton();
            this.B1_radioButton = new System.Windows.Forms.RadioButton();
            this.B0_radioButton = new System.Windows.Forms.RadioButton();
            this.label79 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.Herkulex_radioButton = new System.Windows.Forms.RadioButton();
            this.F5_Set_radioButton = new System.Windows.Forms.RadioButton();
            this.F4_Set_radioButton = new System.Windows.Forms.RadioButton();
            this.F3_Set_radioButton = new System.Windows.Forms.RadioButton();
            this.F2_Set_radioButton = new System.Windows.Forms.RadioButton();
            this.F1_Set_radioButton = new System.Windows.Forms.RadioButton();
            this.F0_Set_radioButton = new System.Windows.Forms.RadioButton();
            this.label72 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.F5_Dir_radioButton = new System.Windows.Forms.RadioButton();
            this.F4_Dir_radioButton = new System.Windows.Forms.RadioButton();
            this.F3_Dir_radioButton = new System.Windows.Forms.RadioButton();
            this.F2_Dir_radioButton = new System.Windows.Forms.RadioButton();
            this.F1_Dir_radioButton = new System.Windows.Forms.RadioButton();
            this.F0_Dir_radioButton = new System.Windows.Forms.RadioButton();
            this.F5_Get_checkBox = new System.Windows.Forms.CheckBox();
            this.F4_Get_checkBox = new System.Windows.Forms.CheckBox();
            this.F3_Get_checkBox = new System.Windows.Forms.CheckBox();
            this.F2_Get_checkBox = new System.Windows.Forms.CheckBox();
            this.F1_Get_checkBox = new System.Windows.Forms.CheckBox();
            this.F0_Get_checkBox = new System.Windows.Forms.CheckBox();
            this.label70 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.Herkulex5_clear_button = new System.Windows.Forms.Button();
            this.Herkulex4_clear_button = new System.Windows.Forms.Button();
            this.Herkulex3_clear_button = new System.Windows.Forms.Button();
            this.Herkulex2_clear_button = new System.Windows.Forms.Button();
            this.Herkulex1_clear_button = new System.Windows.Forms.Button();
            this.label37 = new System.Windows.Forms.Label();
            this.Herkulex_Play_Time_label = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.Herkulex_Play_Time_hScrollBar = new System.Windows.Forms.HScrollBar();
            this.Herkulex5_Brake_radioButton = new System.Windows.Forms.RadioButton();
            this.Herkulex4_Brake_radioButton = new System.Windows.Forms.RadioButton();
            this.Herkulex3_Brake_radioButton = new System.Windows.Forms.RadioButton();
            this.Herkulex2_Brake_radioButton = new System.Windows.Forms.RadioButton();
            this.Herkulex1_Brake_radioButton = new System.Windows.Forms.RadioButton();
            this.Herkulex5_Off_radioButton = new System.Windows.Forms.RadioButton();
            this.Herkulex4_Off_radioButton = new System.Windows.Forms.RadioButton();
            this.Herkulex3_Off_radioButton = new System.Windows.Forms.RadioButton();
            this.Herkulex2_Off_radioButton = new System.Windows.Forms.RadioButton();
            this.Herkulex1_Off_radioButton = new System.Windows.Forms.RadioButton();
            this.Herkulex5_On_radioButton = new System.Windows.Forms.RadioButton();
            this.Herkulex4_On_radioButton = new System.Windows.Forms.RadioButton();
            this.Herkulex3_On_radioButton = new System.Windows.Forms.RadioButton();
            this.Herkulex2_On_radioButton = new System.Windows.Forms.RadioButton();
            this.Herkulex1_On_radioButton = new System.Windows.Forms.RadioButton();
            this.Herkulex5_Position_Value_Label = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.Herkulex4_Temperature_label = new System.Windows.Forms.Label();
            this.Herkulex4_Position_Value_Label = new System.Windows.Forms.Label();
            this.Herkulex3_Temperature_label = new System.Windows.Forms.Label();
            this.Herkulex5_Temperature_label = new System.Windows.Forms.Label();
            this.Herkulex3_Position_Value_Label = new System.Windows.Forms.Label();
            this.Herkulex2_Temperature_label = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.Herkulex2_Position_Value_Label = new System.Windows.Forms.Label();
            this.Herkulex1_Temperature_label = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.Herkulex1_Position_Value_Label = new System.Windows.Forms.Label();
            this.Herkulex5_Position_label = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.Herkulex5_Position_hScrollBar = new System.Windows.Forms.HScrollBar();
            this.Herkulex4_Position_label = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.Herkulex4_Position_hScrollBar = new System.Windows.Forms.HScrollBar();
            this.Herkulex3_Position_label = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.Herkulex3_Position_hScrollBar = new System.Windows.Forms.HScrollBar();
            this.Herkulex2_Position_label = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.Herkulex2_Position_hScrollBar = new System.Windows.Forms.HScrollBar();
            this.Herkulex1_Position_label = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.Herkulex1_Position_hScrollBar = new System.Windows.Forms.HScrollBar();
            this.Herkulex5_Torque_label = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.Herkulex4_Torque_label = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.Herkulex3_Torque_label = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.Herkulex2_Torque_label = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.Herkulex1_Torque_label = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.Herkulex5_Status_label = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.Herkulex4_Status_label = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.Herkulex3_Status_label = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.Herkulex2_Status_label = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.Herkulex1_Status_label = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.Motor_Bat_Min_label = new System.Windows.Forms.Label();
            this.Integrated_Motor_Current_label = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.Integrated_Elect_Current_label = new System.Windows.Forms.Label();
            this.Motor_Level_label = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.Elect_Bat_Min_label = new System.Windows.Forms.Label();
            this.Instant_Motor_Current_label = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.Instant_Elect_Current_label = new System.Windows.Forms.Label();
            this.Elect_Level_label = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.Sensor_Firmware_Number_label = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.Motor_Firmware_Number_label = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.Sensor_radioButton = new System.Windows.Forms.RadioButton();
            this.Motor_radioButton = new System.Windows.Forms.RadioButton();
            this.Find_Serial_button = new System.Windows.Forms.Button();
            this.Sensor_Board_COM_label = new System.Windows.Forms.Label();
            this.Motor_Board_COM_label = new System.Windows.Forms.Label();
            this.Motor_groupBox.SuspendLayout();
            this.Linear_groupBox.SuspendLayout();
            this.Robot_Control_groupBox.SuspendLayout();
            this.Ind_Control_groupBox.SuspendLayout();
            this.Sensor_groupBox.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // Connect_Button
            // 
            this.Connect_Button.Enabled = false;
            this.Connect_Button.Location = new System.Drawing.Point(639, 634);
            this.Connect_Button.Name = "Connect_Button";
            this.Connect_Button.Size = new System.Drawing.Size(75, 23);
            this.Connect_Button.TabIndex = 0;
            this.Connect_Button.Text = "Connect";
            this.Connect_Button.UseVisualStyleBackColor = true;
            this.Connect_Button.Click += new System.EventHandler(this.Connect_Button_Click);
            // 
            // Motor_groupBox
            // 
            this.Motor_groupBox.Controls.Add(this.Linear_groupBox);
            this.Motor_groupBox.Controls.Add(this.Hardstop_Status_checkBox);
            this.Motor_groupBox.Controls.Add(this.Hardstop_Timer_label);
            this.Motor_groupBox.Controls.Add(this.Control_Stream_radioButton);
            this.Motor_groupBox.Controls.Add(this.Hardstop_radioButton);
            this.Motor_groupBox.Controls.Add(this.Ind_Motor_Stream_radioButton);
            this.Motor_groupBox.Controls.Add(this.label61);
            this.Motor_groupBox.Controls.Add(this.Potentiometer_label);
            this.Motor_groupBox.Controls.Add(this.Linear_Position_Ticks_label);
            this.Motor_groupBox.Controls.Add(this.label62);
            this.Motor_groupBox.Controls.Add(this.label59);
            this.Motor_groupBox.Controls.Add(this.label60);
            this.Motor_groupBox.Controls.Add(this.Hardstop_Timer_vScrollBar);
            this.Motor_groupBox.Controls.Add(this.Rear_Right_Ticks_label);
            this.Motor_groupBox.Controls.Add(this.Mid_Right_Ticks_label);
            this.Motor_groupBox.Controls.Add(this.Front_Right_Ticks_label);
            this.Motor_groupBox.Controls.Add(this.Rear_Left_Ticks_label);
            this.Motor_groupBox.Controls.Add(this.Mid_Left_Ticks_label);
            this.Motor_groupBox.Controls.Add(this.Front_Left_Ticks_label);
            this.Motor_groupBox.Controls.Add(this.label55);
            this.Motor_groupBox.Controls.Add(this.label56);
            this.Motor_groupBox.Controls.Add(this.label53);
            this.Motor_groupBox.Controls.Add(this.label54);
            this.Motor_groupBox.Controls.Add(this.label52);
            this.Motor_groupBox.Controls.Add(this.label51);
            this.Motor_groupBox.Controls.Add(this.Robot_Control_groupBox);
            this.Motor_groupBox.Controls.Add(this.Ind_Control_groupBox);
            this.Motor_groupBox.Enabled = false;
            this.Motor_groupBox.Location = new System.Drawing.Point(379, 12);
            this.Motor_groupBox.Name = "Motor_groupBox";
            this.Motor_groupBox.Size = new System.Drawing.Size(346, 582);
            this.Motor_groupBox.TabIndex = 4;
            this.Motor_groupBox.TabStop = false;
            this.Motor_groupBox.Text = "Motor Board";
            // 
            // Linear_groupBox
            // 
            this.Linear_groupBox.Controls.Add(this.Linear_Motor_Velocity_vScrollBar);
            this.Linear_groupBox.Controls.Add(this.Linear_Motor_Velocity_label);
            this.Linear_groupBox.Controls.Add(this.label48);
            this.Linear_groupBox.Controls.Add(this.label47);
            this.Linear_groupBox.Controls.Add(this.Linear_Motor_Position_vScrollBar);
            this.Linear_groupBox.Controls.Add(this.Linear_Motor_Position_label);
            this.Linear_groupBox.Location = new System.Drawing.Point(37, 237);
            this.Linear_groupBox.Name = "Linear_groupBox";
            this.Linear_groupBox.Size = new System.Drawing.Size(130, 152);
            this.Linear_groupBox.TabIndex = 44;
            this.Linear_groupBox.TabStop = false;
            this.Linear_groupBox.Text = "Linear Motor";
            // 
            // Linear_Motor_Velocity_vScrollBar
            // 
            this.Linear_Motor_Velocity_vScrollBar.LargeChange = 1;
            this.Linear_Motor_Velocity_vScrollBar.Location = new System.Drawing.Point(77, 50);
            this.Linear_Motor_Velocity_vScrollBar.Maximum = 300;
            this.Linear_Motor_Velocity_vScrollBar.Name = "Linear_Motor_Velocity_vScrollBar";
            this.Linear_Motor_Velocity_vScrollBar.Size = new System.Drawing.Size(17, 94);
            this.Linear_Motor_Velocity_vScrollBar.TabIndex = 17;
            this.Linear_Motor_Velocity_vScrollBar.Value = 100;
            this.Linear_Motor_Velocity_vScrollBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.Linear_Motor_Velocity_vScrollBar_Scroll);
            // 
            // Linear_Motor_Velocity_label
            // 
            this.Linear_Motor_Velocity_label.AutoSize = true;
            this.Linear_Motor_Velocity_label.Location = new System.Drawing.Point(81, 32);
            this.Linear_Motor_Velocity_label.Name = "Linear_Motor_Velocity_label";
            this.Linear_Motor_Velocity_label.Size = new System.Drawing.Size(13, 13);
            this.Linear_Motor_Velocity_label.TabIndex = 16;
            this.Linear_Motor_Velocity_label.Text = "0";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(68, 19);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(47, 13);
            this.label48.TabIndex = 15;
            this.label48.Text = "Velocity:";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(12, 19);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(50, 13);
            this.label47.TabIndex = 13;
            this.label47.Text = "Position: ";
            // 
            // Linear_Motor_Position_vScrollBar
            // 
            this.Linear_Motor_Position_vScrollBar.LargeChange = 1;
            this.Linear_Motor_Position_vScrollBar.Location = new System.Drawing.Point(25, 50);
            this.Linear_Motor_Position_vScrollBar.Maximum = 78;
            this.Linear_Motor_Position_vScrollBar.Name = "Linear_Motor_Position_vScrollBar";
            this.Linear_Motor_Position_vScrollBar.Size = new System.Drawing.Size(17, 94);
            this.Linear_Motor_Position_vScrollBar.TabIndex = 0;
            this.Linear_Motor_Position_vScrollBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.Linear_Motor_Position_vScrollBar_Scroll);
            // 
            // Linear_Motor_Position_label
            // 
            this.Linear_Motor_Position_label.AutoSize = true;
            this.Linear_Motor_Position_label.Location = new System.Drawing.Point(29, 32);
            this.Linear_Motor_Position_label.Name = "Linear_Motor_Position_label";
            this.Linear_Motor_Position_label.Size = new System.Drawing.Size(13, 13);
            this.Linear_Motor_Position_label.TabIndex = 14;
            this.Linear_Motor_Position_label.Text = "0";
            // 
            // Hardstop_Status_checkBox
            // 
            this.Hardstop_Status_checkBox.AutoCheck = false;
            this.Hardstop_Status_checkBox.AutoSize = true;
            this.Hardstop_Status_checkBox.Location = new System.Drawing.Point(142, 516);
            this.Hardstop_Status_checkBox.Name = "Hardstop_Status_checkBox";
            this.Hardstop_Status_checkBox.Size = new System.Drawing.Size(102, 17);
            this.Hardstop_Status_checkBox.TabIndex = 43;
            this.Hardstop_Status_checkBox.Text = "Hardstop Status";
            this.Hardstop_Status_checkBox.UseVisualStyleBackColor = true;
            // 
            // Hardstop_Timer_label
            // 
            this.Hardstop_Timer_label.AutoSize = true;
            this.Hardstop_Timer_label.Location = new System.Drawing.Point(234, 262);
            this.Hardstop_Timer_label.Name = "Hardstop_Timer_label";
            this.Hardstop_Timer_label.Size = new System.Drawing.Size(13, 13);
            this.Hardstop_Timer_label.TabIndex = 32;
            this.Hardstop_Timer_label.Text = "0";
            // 
            // Control_Stream_radioButton
            // 
            this.Control_Stream_radioButton.AutoCheck = false;
            this.Control_Stream_radioButton.AutoSize = true;
            this.Control_Stream_radioButton.Checked = true;
            this.Control_Stream_radioButton.Location = new System.Drawing.Point(230, 198);
            this.Control_Stream_radioButton.Name = "Control_Stream_radioButton";
            this.Control_Stream_radioButton.Size = new System.Drawing.Size(94, 17);
            this.Control_Stream_radioButton.TabIndex = 42;
            this.Control_Stream_radioButton.TabStop = true;
            this.Control_Stream_radioButton.Text = "Control Stream";
            this.Control_Stream_radioButton.UseVisualStyleBackColor = true;
            this.Control_Stream_radioButton.Click += new System.EventHandler(this.Control_Stream_radioButton_Click);
            // 
            // Hardstop_radioButton
            // 
            this.Hardstop_radioButton.AutoCheck = false;
            this.Hardstop_radioButton.AutoSize = true;
            this.Hardstop_radioButton.Location = new System.Drawing.Point(246, 287);
            this.Hardstop_radioButton.Name = "Hardstop_radioButton";
            this.Hardstop_radioButton.Size = new System.Drawing.Size(68, 17);
            this.Hardstop_radioButton.TabIndex = 34;
            this.Hardstop_radioButton.TabStop = true;
            this.Hardstop_radioButton.Text = "Hardstop";
            this.Hardstop_radioButton.UseVisualStyleBackColor = true;
            this.Hardstop_radioButton.Click += new System.EventHandler(this.Hardstop_radioButton_Click);
            // 
            // Ind_Motor_Stream_radioButton
            // 
            this.Ind_Motor_Stream_radioButton.AutoCheck = false;
            this.Ind_Motor_Stream_radioButton.AutoSize = true;
            this.Ind_Motor_Stream_radioButton.Location = new System.Drawing.Point(61, 198);
            this.Ind_Motor_Stream_radioButton.Name = "Ind_Motor_Stream_radioButton";
            this.Ind_Motor_Stream_radioButton.Size = new System.Drawing.Size(106, 17);
            this.Ind_Motor_Stream_radioButton.TabIndex = 41;
            this.Ind_Motor_Stream_radioButton.TabStop = true;
            this.Ind_Motor_Stream_radioButton.Text = "Individual Stream";
            this.Ind_Motor_Stream_radioButton.UseVisualStyleBackColor = true;
            this.Ind_Motor_Stream_radioButton.Click += new System.EventHandler(this.Ind_Motor_Stream_radioButton_Click);
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(197, 260);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(33, 13);
            this.label61.TabIndex = 31;
            this.label61.Text = "Time:";
            // 
            // Potentiometer_label
            // 
            this.Potentiometer_label.AutoSize = true;
            this.Potentiometer_label.Location = new System.Drawing.Point(298, 492);
            this.Potentiometer_label.Name = "Potentiometer_label";
            this.Potentiometer_label.Size = new System.Drawing.Size(13, 13);
            this.Potentiometer_label.TabIndex = 39;
            this.Potentiometer_label.Text = "0";
            // 
            // Linear_Position_Ticks_label
            // 
            this.Linear_Position_Ticks_label.AutoSize = true;
            this.Linear_Position_Ticks_label.Location = new System.Drawing.Point(139, 491);
            this.Linear_Position_Ticks_label.Name = "Linear_Position_Ticks_label";
            this.Linear_Position_Ticks_label.Size = new System.Drawing.Size(13, 13);
            this.Linear_Position_Ticks_label.TabIndex = 38;
            this.Linear_Position_Ticks_label.Text = "0";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(191, 243);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(50, 13);
            this.label62.TabIndex = 30;
            this.label62.Text = "Hardstop";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(212, 492);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(75, 13);
            this.label59.TabIndex = 37;
            this.label59.Text = "Potentiometer:";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(13, 492);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(108, 13);
            this.label60.TabIndex = 36;
            this.label60.Text = "Linear Position Ticks:";
            // 
            // Hardstop_Timer_vScrollBar
            // 
            this.Hardstop_Timer_vScrollBar.LargeChange = 1;
            this.Hardstop_Timer_vScrollBar.Location = new System.Drawing.Point(203, 287);
            this.Hardstop_Timer_vScrollBar.Maximum = 26;
            this.Hardstop_Timer_vScrollBar.Minimum = 1;
            this.Hardstop_Timer_vScrollBar.Name = "Hardstop_Timer_vScrollBar";
            this.Hardstop_Timer_vScrollBar.Size = new System.Drawing.Size(17, 101);
            this.Hardstop_Timer_vScrollBar.TabIndex = 29;
            this.Hardstop_Timer_vScrollBar.Value = 26;
            this.Hardstop_Timer_vScrollBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.Hardstop_Timer_vScrollBar_Scroll);
            // 
            // Rear_Right_Ticks_label
            // 
            this.Rear_Right_Ticks_label.AutoSize = true;
            this.Rear_Right_Ticks_label.Location = new System.Drawing.Point(298, 471);
            this.Rear_Right_Ticks_label.Name = "Rear_Right_Ticks_label";
            this.Rear_Right_Ticks_label.Size = new System.Drawing.Size(13, 13);
            this.Rear_Right_Ticks_label.TabIndex = 35;
            this.Rear_Right_Ticks_label.Text = "0";
            // 
            // Mid_Right_Ticks_label
            // 
            this.Mid_Right_Ticks_label.AutoSize = true;
            this.Mid_Right_Ticks_label.Location = new System.Drawing.Point(298, 453);
            this.Mid_Right_Ticks_label.Name = "Mid_Right_Ticks_label";
            this.Mid_Right_Ticks_label.Size = new System.Drawing.Size(13, 13);
            this.Mid_Right_Ticks_label.TabIndex = 34;
            this.Mid_Right_Ticks_label.Text = "0";
            // 
            // Front_Right_Ticks_label
            // 
            this.Front_Right_Ticks_label.AutoSize = true;
            this.Front_Right_Ticks_label.Location = new System.Drawing.Point(298, 431);
            this.Front_Right_Ticks_label.Name = "Front_Right_Ticks_label";
            this.Front_Right_Ticks_label.Size = new System.Drawing.Size(13, 13);
            this.Front_Right_Ticks_label.TabIndex = 33;
            this.Front_Right_Ticks_label.Text = "0";
            // 
            // Rear_Left_Ticks_label
            // 
            this.Rear_Left_Ticks_label.AutoSize = true;
            this.Rear_Left_Ticks_label.Location = new System.Drawing.Point(139, 470);
            this.Rear_Left_Ticks_label.Name = "Rear_Left_Ticks_label";
            this.Rear_Left_Ticks_label.Size = new System.Drawing.Size(13, 13);
            this.Rear_Left_Ticks_label.TabIndex = 32;
            this.Rear_Left_Ticks_label.Text = "0";
            // 
            // Mid_Left_Ticks_label
            // 
            this.Mid_Left_Ticks_label.AutoSize = true;
            this.Mid_Left_Ticks_label.Location = new System.Drawing.Point(139, 451);
            this.Mid_Left_Ticks_label.Name = "Mid_Left_Ticks_label";
            this.Mid_Left_Ticks_label.Size = new System.Drawing.Size(13, 13);
            this.Mid_Left_Ticks_label.TabIndex = 31;
            this.Mid_Left_Ticks_label.Text = "0";
            // 
            // Front_Left_Ticks_label
            // 
            this.Front_Left_Ticks_label.AutoSize = true;
            this.Front_Left_Ticks_label.Location = new System.Drawing.Point(139, 430);
            this.Front_Left_Ticks_label.Name = "Front_Left_Ticks_label";
            this.Front_Left_Ticks_label.Size = new System.Drawing.Size(13, 13);
            this.Front_Left_Ticks_label.TabIndex = 21;
            this.Front_Left_Ticks_label.Text = "0";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(197, 471);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(90, 13);
            this.label55.TabIndex = 30;
            this.label55.Text = "Rear Right Ticks:";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(38, 471);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(83, 13);
            this.label56.TabIndex = 29;
            this.label56.Text = "Rear Left Ticks:";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(203, 451);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(84, 13);
            this.label53.TabIndex = 28;
            this.label53.Text = "Mid Right Ticks:";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(44, 451);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(77, 13);
            this.label54.TabIndex = 27;
            this.label54.Text = "Mid Left Ticks:";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(197, 430);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(91, 13);
            this.label52.TabIndex = 26;
            this.label52.Text = "Front Right Ticks:";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(37, 430);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(84, 13);
            this.label51.TabIndex = 25;
            this.label51.Text = "Front Left Ticks:";
            // 
            // Robot_Control_groupBox
            // 
            this.Robot_Control_groupBox.Controls.Add(this.Right_Wheel_Value_label);
            this.Robot_Control_groupBox.Controls.Add(this.Left_Wheel_Value_label);
            this.Robot_Control_groupBox.Controls.Add(this.label74);
            this.Robot_Control_groupBox.Controls.Add(this.label73);
            this.Robot_Control_groupBox.Controls.Add(this.Linear_Velocity_label);
            this.Robot_Control_groupBox.Controls.Add(this.Angular_Velocity_label);
            this.Robot_Control_groupBox.Controls.Add(this.label50);
            this.Robot_Control_groupBox.Controls.Add(this.label49);
            this.Robot_Control_groupBox.Controls.Add(this.Linear_Velocity_vScrollBar);
            this.Robot_Control_groupBox.Controls.Add(this.Angular_Velocity_vScrollBar);
            this.Robot_Control_groupBox.Location = new System.Drawing.Point(207, 19);
            this.Robot_Control_groupBox.Name = "Robot_Control_groupBox";
            this.Robot_Control_groupBox.Size = new System.Drawing.Size(139, 173);
            this.Robot_Control_groupBox.TabIndex = 23;
            this.Robot_Control_groupBox.TabStop = false;
            this.Robot_Control_groupBox.Text = "Robot Control";
            // 
            // Right_Wheel_Value_label
            // 
            this.Right_Wheel_Value_label.AutoSize = true;
            this.Right_Wheel_Value_label.Location = new System.Drawing.Point(113, 131);
            this.Right_Wheel_Value_label.Name = "Right_Wheel_Value_label";
            this.Right_Wheel_Value_label.Size = new System.Drawing.Size(13, 13);
            this.Right_Wheel_Value_label.TabIndex = 38;
            this.Right_Wheel_Value_label.Text = "0";
            // 
            // Left_Wheel_Value_label
            // 
            this.Left_Wheel_Value_label.AutoSize = true;
            this.Left_Wheel_Value_label.Location = new System.Drawing.Point(113, 111);
            this.Left_Wheel_Value_label.Name = "Left_Wheel_Value_label";
            this.Left_Wheel_Value_label.Size = new System.Drawing.Size(13, 13);
            this.Left_Wheel_Value_label.TabIndex = 37;
            this.Left_Wheel_Value_label.Text = "0";
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Location = new System.Drawing.Point(41, 131);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(69, 13);
            this.label74.TabIndex = 36;
            this.label74.Text = "Right Wheel:";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(48, 111);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(62, 13);
            this.label73.TabIndex = 35;
            this.label73.Text = "Left Wheel:";
            // 
            // Linear_Velocity_label
            // 
            this.Linear_Velocity_label.AutoSize = true;
            this.Linear_Velocity_label.Location = new System.Drawing.Point(75, 87);
            this.Linear_Velocity_label.Name = "Linear_Velocity_label";
            this.Linear_Velocity_label.Size = new System.Drawing.Size(13, 13);
            this.Linear_Velocity_label.TabIndex = 27;
            this.Linear_Velocity_label.Text = "0";
            // 
            // Angular_Velocity_label
            // 
            this.Angular_Velocity_label.AutoSize = true;
            this.Angular_Velocity_label.Location = new System.Drawing.Point(112, 16);
            this.Angular_Velocity_label.Name = "Angular_Velocity_label";
            this.Angular_Velocity_label.Size = new System.Drawing.Size(13, 13);
            this.Angular_Velocity_label.TabIndex = 21;
            this.Angular_Velocity_label.Text = "0";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(47, 68);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(79, 13);
            this.label50.TabIndex = 26;
            this.label50.Text = "Linear Velocity:";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(20, 16);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(86, 13);
            this.label49.TabIndex = 25;
            this.label49.Text = "Angular Velocity:";
            // 
            // Linear_Velocity_vScrollBar
            // 
            this.Linear_Velocity_vScrollBar.LargeChange = 1;
            this.Linear_Velocity_vScrollBar.Location = new System.Drawing.Point(17, 68);
            this.Linear_Velocity_vScrollBar.Maximum = 500;
            this.Linear_Velocity_vScrollBar.Minimum = -500;
            this.Linear_Velocity_vScrollBar.Name = "Linear_Velocity_vScrollBar";
            this.Linear_Velocity_vScrollBar.Size = new System.Drawing.Size(17, 94);
            this.Linear_Velocity_vScrollBar.TabIndex = 21;
            this.Linear_Velocity_vScrollBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.Linear_Velocity_vScrollBar_Scroll);
            // 
            // Angular_Velocity_vScrollBar
            // 
            this.Angular_Velocity_vScrollBar.Location = new System.Drawing.Point(13, 33);
            this.Angular_Velocity_vScrollBar.Maximum = 500;
            this.Angular_Velocity_vScrollBar.Minimum = -500;
            this.Angular_Velocity_vScrollBar.Name = "Angular_Velocity_vScrollBar";
            this.Angular_Velocity_vScrollBar.Size = new System.Drawing.Size(113, 17);
            this.Angular_Velocity_vScrollBar.TabIndex = 21;
            this.Angular_Velocity_vScrollBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.Angular_Velocity_vScrollBar_Scroll);
            // 
            // Ind_Control_groupBox
            // 
            this.Ind_Control_groupBox.Controls.Add(this.Rear_Right_label);
            this.Ind_Control_groupBox.Controls.Add(this.Front_Left_hScrollBar);
            this.Ind_Control_groupBox.Controls.Add(this.Mid_Right_label);
            this.Ind_Control_groupBox.Controls.Add(this.Mid_Left_hScrollBar);
            this.Ind_Control_groupBox.Controls.Add(this.Front_Right_label);
            this.Ind_Control_groupBox.Controls.Add(this.Rear_Left_hScrollBar);
            this.Ind_Control_groupBox.Controls.Add(this.Rear_Left_label);
            this.Ind_Control_groupBox.Controls.Add(this.Front_Right_hScrollBar);
            this.Ind_Control_groupBox.Controls.Add(this.Mid_Left_label);
            this.Ind_Control_groupBox.Controls.Add(this.Mid_Right_hScrollBar);
            this.Ind_Control_groupBox.Controls.Add(this.Front_Left_label);
            this.Ind_Control_groupBox.Controls.Add(this.Rear_Right_hScrollBar);
            this.Ind_Control_groupBox.Controls.Add(this.label39);
            this.Ind_Control_groupBox.Controls.Add(this.label40);
            this.Ind_Control_groupBox.Controls.Add(this.label44);
            this.Ind_Control_groupBox.Controls.Add(this.label41);
            this.Ind_Control_groupBox.Controls.Add(this.label43);
            this.Ind_Control_groupBox.Controls.Add(this.label42);
            this.Ind_Control_groupBox.Location = new System.Drawing.Point(6, 19);
            this.Ind_Control_groupBox.Name = "Ind_Control_groupBox";
            this.Ind_Control_groupBox.Size = new System.Drawing.Size(195, 173);
            this.Ind_Control_groupBox.TabIndex = 22;
            this.Ind_Control_groupBox.TabStop = false;
            this.Ind_Control_groupBox.Text = "Individual Control";
            // 
            // Rear_Right_label
            // 
            this.Rear_Right_label.AutoSize = true;
            this.Rear_Right_label.Location = new System.Drawing.Point(164, 115);
            this.Rear_Right_label.Name = "Rear_Right_label";
            this.Rear_Right_label.Size = new System.Drawing.Size(13, 13);
            this.Rear_Right_label.TabIndex = 20;
            this.Rear_Right_label.Text = "0";
            // 
            // Front_Left_hScrollBar
            // 
            this.Front_Left_hScrollBar.Location = new System.Drawing.Point(7, 38);
            this.Front_Left_hScrollBar.Maximum = 1100;
            this.Front_Left_hScrollBar.Minimum = -1100;
            this.Front_Left_hScrollBar.Name = "Front_Left_hScrollBar";
            this.Front_Left_hScrollBar.Size = new System.Drawing.Size(80, 17);
            this.Front_Left_hScrollBar.TabIndex = 1;
            this.Front_Left_hScrollBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.Front_Left_hScrollBar_Scroll);
            // 
            // Mid_Right_label
            // 
            this.Mid_Right_label.AutoSize = true;
            this.Mid_Right_label.Location = new System.Drawing.Point(164, 62);
            this.Mid_Right_label.Name = "Mid_Right_label";
            this.Mid_Right_label.Size = new System.Drawing.Size(13, 13);
            this.Mid_Right_label.TabIndex = 19;
            this.Mid_Right_label.Text = "0";
            // 
            // Mid_Left_hScrollBar
            // 
            this.Mid_Left_hScrollBar.Location = new System.Drawing.Point(7, 88);
            this.Mid_Left_hScrollBar.Maximum = 1100;
            this.Mid_Left_hScrollBar.Minimum = -1100;
            this.Mid_Left_hScrollBar.Name = "Mid_Left_hScrollBar";
            this.Mid_Left_hScrollBar.Size = new System.Drawing.Size(80, 17);
            this.Mid_Left_hScrollBar.TabIndex = 2;
            this.Mid_Left_hScrollBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.Mid_Left_hScrollBar_Scroll);
            // 
            // Front_Right_label
            // 
            this.Front_Right_label.AutoSize = true;
            this.Front_Right_label.Location = new System.Drawing.Point(164, 16);
            this.Front_Right_label.Name = "Front_Right_label";
            this.Front_Right_label.Size = new System.Drawing.Size(13, 13);
            this.Front_Right_label.TabIndex = 18;
            this.Front_Right_label.Text = "0";
            // 
            // Rear_Left_hScrollBar
            // 
            this.Rear_Left_hScrollBar.Location = new System.Drawing.Point(7, 136);
            this.Rear_Left_hScrollBar.Maximum = 1100;
            this.Rear_Left_hScrollBar.Minimum = -1100;
            this.Rear_Left_hScrollBar.Name = "Rear_Left_hScrollBar";
            this.Rear_Left_hScrollBar.Size = new System.Drawing.Size(80, 17);
            this.Rear_Left_hScrollBar.TabIndex = 3;
            this.Rear_Left_hScrollBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.Rear_Left_hScrollBar_Scroll);
            // 
            // Rear_Left_label
            // 
            this.Rear_Left_label.AutoSize = true;
            this.Rear_Left_label.Location = new System.Drawing.Point(64, 115);
            this.Rear_Left_label.Name = "Rear_Left_label";
            this.Rear_Left_label.Size = new System.Drawing.Size(13, 13);
            this.Rear_Left_label.TabIndex = 17;
            this.Rear_Left_label.Text = "0";
            // 
            // Front_Right_hScrollBar
            // 
            this.Front_Right_hScrollBar.Location = new System.Drawing.Point(107, 38);
            this.Front_Right_hScrollBar.Maximum = 1100;
            this.Front_Right_hScrollBar.Minimum = -1100;
            this.Front_Right_hScrollBar.Name = "Front_Right_hScrollBar";
            this.Front_Right_hScrollBar.Size = new System.Drawing.Size(80, 17);
            this.Front_Right_hScrollBar.TabIndex = 4;
            this.Front_Right_hScrollBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.Front_Right_hScrollBar_Scroll);
            // 
            // Mid_Left_label
            // 
            this.Mid_Left_label.AutoSize = true;
            this.Mid_Left_label.Location = new System.Drawing.Point(64, 63);
            this.Mid_Left_label.Name = "Mid_Left_label";
            this.Mid_Left_label.Size = new System.Drawing.Size(13, 13);
            this.Mid_Left_label.TabIndex = 16;
            this.Mid_Left_label.Text = "0";
            // 
            // Mid_Right_hScrollBar
            // 
            this.Mid_Right_hScrollBar.Location = new System.Drawing.Point(107, 88);
            this.Mid_Right_hScrollBar.Maximum = 1100;
            this.Mid_Right_hScrollBar.Minimum = -1100;
            this.Mid_Right_hScrollBar.Name = "Mid_Right_hScrollBar";
            this.Mid_Right_hScrollBar.Size = new System.Drawing.Size(80, 17);
            this.Mid_Right_hScrollBar.TabIndex = 5;
            this.Mid_Right_hScrollBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.Mid_Right_hScrollBar_Scroll);
            // 
            // Front_Left_label
            // 
            this.Front_Left_label.AutoSize = true;
            this.Front_Left_label.Location = new System.Drawing.Point(64, 16);
            this.Front_Left_label.Name = "Front_Left_label";
            this.Front_Left_label.Size = new System.Drawing.Size(13, 13);
            this.Front_Left_label.TabIndex = 15;
            this.Front_Left_label.Text = "0";
            // 
            // Rear_Right_hScrollBar
            // 
            this.Rear_Right_hScrollBar.Location = new System.Drawing.Point(107, 136);
            this.Rear_Right_hScrollBar.Maximum = 1100;
            this.Rear_Right_hScrollBar.Minimum = -1100;
            this.Rear_Right_hScrollBar.Name = "Rear_Right_hScrollBar";
            this.Rear_Right_hScrollBar.Size = new System.Drawing.Size(80, 17);
            this.Rear_Right_hScrollBar.TabIndex = 6;
            this.Rear_Right_hScrollBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.Rear_Right_hScrollBar_Scroll);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(10, 16);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(58, 13);
            this.label39.TabIndex = 7;
            this.label39.Text = "Front Left: ";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(103, 16);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(65, 13);
            this.label40.TabIndex = 8;
            this.label40.Text = "Front Right: ";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(104, 115);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(64, 13);
            this.label44.TabIndex = 12;
            this.label44.Text = "Rear Right: ";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(10, 63);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(51, 13);
            this.label41.TabIndex = 9;
            this.label41.Text = "Mid Left: ";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(10, 115);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(57, 13);
            this.label43.TabIndex = 11;
            this.label43.Text = "Rear Left: ";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(103, 62);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(58, 13);
            this.label42.TabIndex = 10;
            this.label42.Text = "Mid Right: ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Elect_V:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(120, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Elect_I:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(26, 48);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Motor_V:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(117, 48);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Motor_I:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 65);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(60, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Charger_V:";
            // 
            // Elect_V_label
            // 
            this.Elect_V_label.AutoSize = true;
            this.Elect_V_label.Location = new System.Drawing.Point(78, 31);
            this.Elect_V_label.Name = "Elect_V_label";
            this.Elect_V_label.Size = new System.Drawing.Size(23, 13);
            this.Elect_V_label.TabIndex = 5;
            this.Elect_V_label.Text = "0 V";
            // 
            // Motor_V_label
            // 
            this.Motor_V_label.AutoSize = true;
            this.Motor_V_label.Location = new System.Drawing.Point(78, 48);
            this.Motor_V_label.Name = "Motor_V_label";
            this.Motor_V_label.Size = new System.Drawing.Size(23, 13);
            this.Motor_V_label.TabIndex = 6;
            this.Motor_V_label.Text = "0 V";
            // 
            // Charger_V_label
            // 
            this.Charger_V_label.AutoSize = true;
            this.Charger_V_label.Location = new System.Drawing.Point(78, 65);
            this.Charger_V_label.Name = "Charger_V_label";
            this.Charger_V_label.Size = new System.Drawing.Size(23, 13);
            this.Charger_V_label.TabIndex = 7;
            this.Charger_V_label.Text = "0 V";
            // 
            // Elect_I_label
            // 
            this.Elect_I_label.AutoSize = true;
            this.Elect_I_label.Location = new System.Drawing.Point(169, 31);
            this.Elect_I_label.Name = "Elect_I_label";
            this.Elect_I_label.Size = new System.Drawing.Size(31, 13);
            this.Elect_I_label.TabIndex = 8;
            this.Elect_I_label.Text = "0 mA";
            // 
            // Motor_I_label
            // 
            this.Motor_I_label.AutoSize = true;
            this.Motor_I_label.Location = new System.Drawing.Point(169, 48);
            this.Motor_I_label.Name = "Motor_I_label";
            this.Motor_I_label.Size = new System.Drawing.Size(31, 13);
            this.Motor_I_label.TabIndex = 9;
            this.Motor_I_label.Text = "0 mA";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(33, 112);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(112, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Motor Battery Voltage:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(13, 95);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(132, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "Electronic Battery Voltage:";
            // 
            // Electronic_Battery_Voltage_label
            // 
            this.Electronic_Battery_Voltage_label.AutoSize = true;
            this.Electronic_Battery_Voltage_label.Location = new System.Drawing.Point(161, 95);
            this.Electronic_Battery_Voltage_label.Name = "Electronic_Battery_Voltage_label";
            this.Electronic_Battery_Voltage_label.Size = new System.Drawing.Size(23, 13);
            this.Electronic_Battery_Voltage_label.TabIndex = 12;
            this.Electronic_Battery_Voltage_label.Text = "0 V";
            // 
            // Motor_Battery_Voltage_label
            // 
            this.Motor_Battery_Voltage_label.AutoSize = true;
            this.Motor_Battery_Voltage_label.Location = new System.Drawing.Point(161, 112);
            this.Motor_Battery_Voltage_label.Name = "Motor_Battery_Voltage_label";
            this.Motor_Battery_Voltage_label.Size = new System.Drawing.Size(23, 13);
            this.Motor_Battery_Voltage_label.TabIndex = 13;
            this.Motor_Battery_Voltage_label.Text = "0 V";
            // 
            // Sensor_groupBox
            // 
            this.Sensor_groupBox.Controls.Add(this.B3_radioButton);
            this.Sensor_groupBox.Controls.Add(this.B2_radioButton);
            this.Sensor_groupBox.Controls.Add(this.B1_radioButton);
            this.Sensor_groupBox.Controls.Add(this.B0_radioButton);
            this.Sensor_groupBox.Controls.Add(this.label79);
            this.Sensor_groupBox.Controls.Add(this.label80);
            this.Sensor_groupBox.Controls.Add(this.label77);
            this.Sensor_groupBox.Controls.Add(this.label78);
            this.Sensor_groupBox.Controls.Add(this.label76);
            this.Sensor_groupBox.Controls.Add(this.Herkulex_radioButton);
            this.Sensor_groupBox.Controls.Add(this.F5_Set_radioButton);
            this.Sensor_groupBox.Controls.Add(this.F4_Set_radioButton);
            this.Sensor_groupBox.Controls.Add(this.F3_Set_radioButton);
            this.Sensor_groupBox.Controls.Add(this.F2_Set_radioButton);
            this.Sensor_groupBox.Controls.Add(this.F1_Set_radioButton);
            this.Sensor_groupBox.Controls.Add(this.F0_Set_radioButton);
            this.Sensor_groupBox.Controls.Add(this.label72);
            this.Sensor_groupBox.Controls.Add(this.label71);
            this.Sensor_groupBox.Controls.Add(this.label13);
            this.Sensor_groupBox.Controls.Add(this.F5_Dir_radioButton);
            this.Sensor_groupBox.Controls.Add(this.F4_Dir_radioButton);
            this.Sensor_groupBox.Controls.Add(this.F3_Dir_radioButton);
            this.Sensor_groupBox.Controls.Add(this.F2_Dir_radioButton);
            this.Sensor_groupBox.Controls.Add(this.F1_Dir_radioButton);
            this.Sensor_groupBox.Controls.Add(this.F0_Dir_radioButton);
            this.Sensor_groupBox.Controls.Add(this.F5_Get_checkBox);
            this.Sensor_groupBox.Controls.Add(this.F4_Get_checkBox);
            this.Sensor_groupBox.Controls.Add(this.F3_Get_checkBox);
            this.Sensor_groupBox.Controls.Add(this.F2_Get_checkBox);
            this.Sensor_groupBox.Controls.Add(this.F1_Get_checkBox);
            this.Sensor_groupBox.Controls.Add(this.F0_Get_checkBox);
            this.Sensor_groupBox.Controls.Add(this.label70);
            this.Sensor_groupBox.Controls.Add(this.label69);
            this.Sensor_groupBox.Controls.Add(this.label68);
            this.Sensor_groupBox.Controls.Add(this.label67);
            this.Sensor_groupBox.Controls.Add(this.label66);
            this.Sensor_groupBox.Controls.Add(this.label65);
            this.Sensor_groupBox.Controls.Add(this.groupBox4);
            this.Sensor_groupBox.Controls.Add(this.label12);
            this.Sensor_groupBox.Controls.Add(this.Motor_Bat_Min_label);
            this.Sensor_groupBox.Controls.Add(this.Integrated_Motor_Current_label);
            this.Sensor_groupBox.Controls.Add(this.label17);
            this.Sensor_groupBox.Controls.Add(this.Integrated_Elect_Current_label);
            this.Sensor_groupBox.Controls.Add(this.Motor_Level_label);
            this.Sensor_groupBox.Controls.Add(this.label10);
            this.Sensor_groupBox.Controls.Add(this.label19);
            this.Sensor_groupBox.Controls.Add(this.label11);
            this.Sensor_groupBox.Controls.Add(this.Elect_Bat_Min_label);
            this.Sensor_groupBox.Controls.Add(this.Instant_Motor_Current_label);
            this.Sensor_groupBox.Controls.Add(this.label14);
            this.Sensor_groupBox.Controls.Add(this.Instant_Elect_Current_label);
            this.Sensor_groupBox.Controls.Add(this.Elect_Level_label);
            this.Sensor_groupBox.Controls.Add(this.label9);
            this.Sensor_groupBox.Controls.Add(this.label15);
            this.Sensor_groupBox.Controls.Add(this.Motor_Battery_Voltage_label);
            this.Sensor_groupBox.Controls.Add(this.label8);
            this.Sensor_groupBox.Controls.Add(this.Electronic_Battery_Voltage_label);
            this.Sensor_groupBox.Controls.Add(this.label7);
            this.Sensor_groupBox.Controls.Add(this.label6);
            this.Sensor_groupBox.Controls.Add(this.Motor_I_label);
            this.Sensor_groupBox.Controls.Add(this.Elect_I_label);
            this.Sensor_groupBox.Controls.Add(this.Charger_V_label);
            this.Sensor_groupBox.Controls.Add(this.Motor_V_label);
            this.Sensor_groupBox.Controls.Add(this.Elect_V_label);
            this.Sensor_groupBox.Controls.Add(this.label5);
            this.Sensor_groupBox.Controls.Add(this.label3);
            this.Sensor_groupBox.Controls.Add(this.label4);
            this.Sensor_groupBox.Controls.Add(this.label2);
            this.Sensor_groupBox.Controls.Add(this.label1);
            this.Sensor_groupBox.Enabled = false;
            this.Sensor_groupBox.Location = new System.Drawing.Point(13, 13);
            this.Sensor_groupBox.Name = "Sensor_groupBox";
            this.Sensor_groupBox.Size = new System.Drawing.Size(344, 581);
            this.Sensor_groupBox.TabIndex = 3;
            this.Sensor_groupBox.TabStop = false;
            this.Sensor_groupBox.Text = "Sensor Board";
            // 
            // B3_radioButton
            // 
            this.B3_radioButton.AutoCheck = false;
            this.B3_radioButton.AutoSize = true;
            this.B3_radioButton.Location = new System.Drawing.Point(310, 70);
            this.B3_radioButton.Name = "B3_radioButton";
            this.B3_radioButton.Size = new System.Drawing.Size(14, 13);
            this.B3_radioButton.TabIndex = 78;
            this.B3_radioButton.UseVisualStyleBackColor = true;
            this.B3_radioButton.Click += new System.EventHandler(this.B3_radioButton_Click);
            // 
            // B2_radioButton
            // 
            this.B2_radioButton.AutoCheck = false;
            this.B2_radioButton.AutoSize = true;
            this.B2_radioButton.Location = new System.Drawing.Point(310, 48);
            this.B2_radioButton.Name = "B2_radioButton";
            this.B2_radioButton.Size = new System.Drawing.Size(14, 13);
            this.B2_radioButton.TabIndex = 77;
            this.B2_radioButton.UseVisualStyleBackColor = true;
            this.B2_radioButton.Click += new System.EventHandler(this.B2_radioButton_Click);
            // 
            // B1_radioButton
            // 
            this.B1_radioButton.AutoCheck = false;
            this.B1_radioButton.AutoSize = true;
            this.B1_radioButton.Location = new System.Drawing.Point(251, 70);
            this.B1_radioButton.Name = "B1_radioButton";
            this.B1_radioButton.Size = new System.Drawing.Size(14, 13);
            this.B1_radioButton.TabIndex = 76;
            this.B1_radioButton.UseVisualStyleBackColor = true;
            this.B1_radioButton.Click += new System.EventHandler(this.B1_radioButton_Click);
            // 
            // B0_radioButton
            // 
            this.B0_radioButton.AutoCheck = false;
            this.B0_radioButton.AutoSize = true;
            this.B0_radioButton.Location = new System.Drawing.Point(251, 48);
            this.B0_radioButton.Name = "B0_radioButton";
            this.B0_radioButton.Size = new System.Drawing.Size(14, 13);
            this.B0_radioButton.TabIndex = 75;
            this.B0_radioButton.UseVisualStyleBackColor = true;
            this.B0_radioButton.Click += new System.EventHandler(this.B0_radioButton_Click);
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Location = new System.Drawing.Point(285, 70);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(20, 13);
            this.label79.TabIndex = 74;
            this.label79.Text = "B3";
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Location = new System.Drawing.Point(285, 48);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(20, 13);
            this.label80.TabIndex = 73;
            this.label80.Text = "B2";
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Location = new System.Drawing.Point(224, 70);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(20, 13);
            this.label77.TabIndex = 72;
            this.label77.Text = "B1";
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Location = new System.Drawing.Point(224, 48);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(20, 13);
            this.label78.TabIndex = 71;
            this.label78.Text = "B0";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Location = new System.Drawing.Point(250, 18);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(35, 13);
            this.label76.TabIndex = 70;
            this.label76.Text = "Lights";
            // 
            // Herkulex_radioButton
            // 
            this.Herkulex_radioButton.AutoCheck = false;
            this.Herkulex_radioButton.AutoSize = true;
            this.Herkulex_radioButton.Location = new System.Drawing.Point(235, 558);
            this.Herkulex_radioButton.Name = "Herkulex_radioButton";
            this.Herkulex_radioButton.Size = new System.Drawing.Size(103, 17);
            this.Herkulex_radioButton.TabIndex = 69;
            this.Herkulex_radioButton.Text = "Herkulex Stream";
            this.Herkulex_radioButton.UseVisualStyleBackColor = true;
            this.Herkulex_radioButton.Click += new System.EventHandler(this.Herkulex_radioButton_Click);
            // 
            // F5_Set_radioButton
            // 
            this.F5_Set_radioButton.AutoCheck = false;
            this.F5_Set_radioButton.AutoSize = true;
            this.F5_Set_radioButton.Enabled = false;
            this.F5_Set_radioButton.Location = new System.Drawing.Point(281, 238);
            this.F5_Set_radioButton.Name = "F5_Set_radioButton";
            this.F5_Set_radioButton.Size = new System.Drawing.Size(14, 13);
            this.F5_Set_radioButton.TabIndex = 68;
            this.F5_Set_radioButton.UseVisualStyleBackColor = true;
            this.F5_Set_radioButton.Click += new System.EventHandler(this.F5_Set_radioButton_Click);
            // 
            // F4_Set_radioButton
            // 
            this.F4_Set_radioButton.AutoCheck = false;
            this.F4_Set_radioButton.AutoSize = true;
            this.F4_Set_radioButton.Enabled = false;
            this.F4_Set_radioButton.Location = new System.Drawing.Point(281, 214);
            this.F4_Set_radioButton.Name = "F4_Set_radioButton";
            this.F4_Set_radioButton.Size = new System.Drawing.Size(14, 13);
            this.F4_Set_radioButton.TabIndex = 67;
            this.F4_Set_radioButton.UseVisualStyleBackColor = true;
            this.F4_Set_radioButton.Click += new System.EventHandler(this.F4_Set_radioButton_Click);
            // 
            // F3_Set_radioButton
            // 
            this.F3_Set_radioButton.AutoCheck = false;
            this.F3_Set_radioButton.AutoSize = true;
            this.F3_Set_radioButton.Enabled = false;
            this.F3_Set_radioButton.Location = new System.Drawing.Point(281, 191);
            this.F3_Set_radioButton.Name = "F3_Set_radioButton";
            this.F3_Set_radioButton.Size = new System.Drawing.Size(14, 13);
            this.F3_Set_radioButton.TabIndex = 66;
            this.F3_Set_radioButton.UseVisualStyleBackColor = true;
            this.F3_Set_radioButton.Click += new System.EventHandler(this.F3_Set_radioButton_Click);
            // 
            // F2_Set_radioButton
            // 
            this.F2_Set_radioButton.AutoCheck = false;
            this.F2_Set_radioButton.AutoSize = true;
            this.F2_Set_radioButton.Enabled = false;
            this.F2_Set_radioButton.Location = new System.Drawing.Point(281, 168);
            this.F2_Set_radioButton.Name = "F2_Set_radioButton";
            this.F2_Set_radioButton.Size = new System.Drawing.Size(14, 13);
            this.F2_Set_radioButton.TabIndex = 65;
            this.F2_Set_radioButton.UseVisualStyleBackColor = true;
            this.F2_Set_radioButton.Click += new System.EventHandler(this.F2_Set_radioButton_Click);
            // 
            // F1_Set_radioButton
            // 
            this.F1_Set_radioButton.AutoCheck = false;
            this.F1_Set_radioButton.AutoSize = true;
            this.F1_Set_radioButton.Enabled = false;
            this.F1_Set_radioButton.Location = new System.Drawing.Point(281, 148);
            this.F1_Set_radioButton.Name = "F1_Set_radioButton";
            this.F1_Set_radioButton.Size = new System.Drawing.Size(14, 13);
            this.F1_Set_radioButton.TabIndex = 64;
            this.F1_Set_radioButton.UseVisualStyleBackColor = true;
            this.F1_Set_radioButton.Click += new System.EventHandler(this.F1_Set_radioButton_Click);
            // 
            // F0_Set_radioButton
            // 
            this.F0_Set_radioButton.AutoCheck = false;
            this.F0_Set_radioButton.AutoSize = true;
            this.F0_Set_radioButton.Enabled = false;
            this.F0_Set_radioButton.Location = new System.Drawing.Point(281, 124);
            this.F0_Set_radioButton.Name = "F0_Set_radioButton";
            this.F0_Set_radioButton.Size = new System.Drawing.Size(14, 13);
            this.F0_Set_radioButton.TabIndex = 63;
            this.F0_Set_radioButton.UseVisualStyleBackColor = true;
            this.F0_Set_radioButton.Click += new System.EventHandler(this.F0_Set_radioButton_Click);
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Location = new System.Drawing.Point(305, 108);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(24, 13);
            this.label72.TabIndex = 62;
            this.label72.Text = "Get";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Location = new System.Drawing.Point(272, 108);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(23, 13);
            this.label71.TabIndex = 61;
            this.label71.Text = "Set";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(250, 107);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(16, 13);
            this.label13.TabIndex = 60;
            this.label13.Text = "In";
            // 
            // F5_Dir_radioButton
            // 
            this.F5_Dir_radioButton.AutoCheck = false;
            this.F5_Dir_radioButton.AutoSize = true;
            this.F5_Dir_radioButton.Checked = true;
            this.F5_Dir_radioButton.Location = new System.Drawing.Point(251, 238);
            this.F5_Dir_radioButton.Name = "F5_Dir_radioButton";
            this.F5_Dir_radioButton.Size = new System.Drawing.Size(14, 13);
            this.F5_Dir_radioButton.TabIndex = 59;
            this.F5_Dir_radioButton.UseVisualStyleBackColor = true;
            this.F5_Dir_radioButton.Click += new System.EventHandler(this.F5_Dir_radioButton_Click);
            // 
            // F4_Dir_radioButton
            // 
            this.F4_Dir_radioButton.AutoCheck = false;
            this.F4_Dir_radioButton.AutoSize = true;
            this.F4_Dir_radioButton.Checked = true;
            this.F4_Dir_radioButton.Location = new System.Drawing.Point(251, 214);
            this.F4_Dir_radioButton.Name = "F4_Dir_radioButton";
            this.F4_Dir_radioButton.Size = new System.Drawing.Size(14, 13);
            this.F4_Dir_radioButton.TabIndex = 58;
            this.F4_Dir_radioButton.UseVisualStyleBackColor = true;
            this.F4_Dir_radioButton.Click += new System.EventHandler(this.F4_Dir_radioButton_Click);
            // 
            // F3_Dir_radioButton
            // 
            this.F3_Dir_radioButton.AutoCheck = false;
            this.F3_Dir_radioButton.AutoSize = true;
            this.F3_Dir_radioButton.Checked = true;
            this.F3_Dir_radioButton.Location = new System.Drawing.Point(251, 191);
            this.F3_Dir_radioButton.Name = "F3_Dir_radioButton";
            this.F3_Dir_radioButton.Size = new System.Drawing.Size(14, 13);
            this.F3_Dir_radioButton.TabIndex = 57;
            this.F3_Dir_radioButton.UseVisualStyleBackColor = true;
            this.F3_Dir_radioButton.Click += new System.EventHandler(this.F3_Dir_radioButton_Click);
            // 
            // F2_Dir_radioButton
            // 
            this.F2_Dir_radioButton.AutoCheck = false;
            this.F2_Dir_radioButton.AutoSize = true;
            this.F2_Dir_radioButton.Checked = true;
            this.F2_Dir_radioButton.Location = new System.Drawing.Point(251, 168);
            this.F2_Dir_radioButton.Name = "F2_Dir_radioButton";
            this.F2_Dir_radioButton.Size = new System.Drawing.Size(14, 13);
            this.F2_Dir_radioButton.TabIndex = 56;
            this.F2_Dir_radioButton.UseVisualStyleBackColor = true;
            this.F2_Dir_radioButton.Click += new System.EventHandler(this.F2_Dir_radioButton_Click);
            // 
            // F1_Dir_radioButton
            // 
            this.F1_Dir_radioButton.AutoCheck = false;
            this.F1_Dir_radioButton.AutoSize = true;
            this.F1_Dir_radioButton.Checked = true;
            this.F1_Dir_radioButton.Location = new System.Drawing.Point(251, 148);
            this.F1_Dir_radioButton.Name = "F1_Dir_radioButton";
            this.F1_Dir_radioButton.Size = new System.Drawing.Size(14, 13);
            this.F1_Dir_radioButton.TabIndex = 55;
            this.F1_Dir_radioButton.UseVisualStyleBackColor = true;
            this.F1_Dir_radioButton.Click += new System.EventHandler(this.F1_Dir_radioButton_Click);
            // 
            // F0_Dir_radioButton
            // 
            this.F0_Dir_radioButton.AutoCheck = false;
            this.F0_Dir_radioButton.AutoSize = true;
            this.F0_Dir_radioButton.Checked = true;
            this.F0_Dir_radioButton.Location = new System.Drawing.Point(251, 124);
            this.F0_Dir_radioButton.Name = "F0_Dir_radioButton";
            this.F0_Dir_radioButton.Size = new System.Drawing.Size(14, 13);
            this.F0_Dir_radioButton.TabIndex = 54;
            this.F0_Dir_radioButton.UseVisualStyleBackColor = true;
            this.F0_Dir_radioButton.Click += new System.EventHandler(this.F0_Dir_radioButton_Click);
            // 
            // F5_Get_checkBox
            // 
            this.F5_Get_checkBox.AutoCheck = false;
            this.F5_Get_checkBox.AutoSize = true;
            this.F5_Get_checkBox.Location = new System.Drawing.Point(309, 238);
            this.F5_Get_checkBox.Name = "F5_Get_checkBox";
            this.F5_Get_checkBox.Size = new System.Drawing.Size(15, 14);
            this.F5_Get_checkBox.TabIndex = 53;
            this.F5_Get_checkBox.UseVisualStyleBackColor = true;
            // 
            // F4_Get_checkBox
            // 
            this.F4_Get_checkBox.AutoCheck = false;
            this.F4_Get_checkBox.AutoSize = true;
            this.F4_Get_checkBox.Location = new System.Drawing.Point(309, 214);
            this.F4_Get_checkBox.Name = "F4_Get_checkBox";
            this.F4_Get_checkBox.Size = new System.Drawing.Size(15, 14);
            this.F4_Get_checkBox.TabIndex = 52;
            this.F4_Get_checkBox.UseVisualStyleBackColor = true;
            // 
            // F3_Get_checkBox
            // 
            this.F3_Get_checkBox.AutoCheck = false;
            this.F3_Get_checkBox.AutoSize = true;
            this.F3_Get_checkBox.Location = new System.Drawing.Point(309, 191);
            this.F3_Get_checkBox.Name = "F3_Get_checkBox";
            this.F3_Get_checkBox.Size = new System.Drawing.Size(15, 14);
            this.F3_Get_checkBox.TabIndex = 51;
            this.F3_Get_checkBox.UseVisualStyleBackColor = true;
            // 
            // F2_Get_checkBox
            // 
            this.F2_Get_checkBox.AutoCheck = false;
            this.F2_Get_checkBox.AutoSize = true;
            this.F2_Get_checkBox.Location = new System.Drawing.Point(309, 168);
            this.F2_Get_checkBox.Name = "F2_Get_checkBox";
            this.F2_Get_checkBox.Size = new System.Drawing.Size(15, 14);
            this.F2_Get_checkBox.TabIndex = 50;
            this.F2_Get_checkBox.UseVisualStyleBackColor = true;
            // 
            // F1_Get_checkBox
            // 
            this.F1_Get_checkBox.AutoCheck = false;
            this.F1_Get_checkBox.AutoSize = true;
            this.F1_Get_checkBox.Location = new System.Drawing.Point(309, 148);
            this.F1_Get_checkBox.Name = "F1_Get_checkBox";
            this.F1_Get_checkBox.Size = new System.Drawing.Size(15, 14);
            this.F1_Get_checkBox.TabIndex = 49;
            this.F1_Get_checkBox.UseVisualStyleBackColor = true;
            // 
            // F0_Get_checkBox
            // 
            this.F0_Get_checkBox.AutoCheck = false;
            this.F0_Get_checkBox.AutoSize = true;
            this.F0_Get_checkBox.Location = new System.Drawing.Point(309, 124);
            this.F0_Get_checkBox.Name = "F0_Get_checkBox";
            this.F0_Get_checkBox.Size = new System.Drawing.Size(15, 14);
            this.F0_Get_checkBox.TabIndex = 48;
            this.F0_Get_checkBox.UseVisualStyleBackColor = true;
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(224, 237);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(19, 13);
            this.label70.TabIndex = 47;
            this.label70.Text = "F5";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(224, 214);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(19, 13);
            this.label69.TabIndex = 46;
            this.label69.Text = "F4";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(224, 190);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(19, 13);
            this.label68.TabIndex = 45;
            this.label68.Text = "F3";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(224, 168);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(19, 13);
            this.label67.TabIndex = 44;
            this.label67.Text = "F2";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(224, 147);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(19, 13);
            this.label66.TabIndex = 43;
            this.label66.Text = "F1";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(224, 125);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(19, 13);
            this.label65.TabIndex = 42;
            this.label65.Text = "F0";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.Herkulex5_clear_button);
            this.groupBox4.Controls.Add(this.Herkulex4_clear_button);
            this.groupBox4.Controls.Add(this.Herkulex3_clear_button);
            this.groupBox4.Controls.Add(this.Herkulex2_clear_button);
            this.groupBox4.Controls.Add(this.Herkulex1_clear_button);
            this.groupBox4.Controls.Add(this.label37);
            this.groupBox4.Controls.Add(this.Herkulex_Play_Time_label);
            this.groupBox4.Controls.Add(this.label75);
            this.groupBox4.Controls.Add(this.Herkulex_Play_Time_hScrollBar);
            this.groupBox4.Controls.Add(this.Herkulex5_Brake_radioButton);
            this.groupBox4.Controls.Add(this.Herkulex4_Brake_radioButton);
            this.groupBox4.Controls.Add(this.Herkulex3_Brake_radioButton);
            this.groupBox4.Controls.Add(this.Herkulex2_Brake_radioButton);
            this.groupBox4.Controls.Add(this.Herkulex1_Brake_radioButton);
            this.groupBox4.Controls.Add(this.Herkulex5_Off_radioButton);
            this.groupBox4.Controls.Add(this.Herkulex4_Off_radioButton);
            this.groupBox4.Controls.Add(this.Herkulex3_Off_radioButton);
            this.groupBox4.Controls.Add(this.Herkulex2_Off_radioButton);
            this.groupBox4.Controls.Add(this.Herkulex1_Off_radioButton);
            this.groupBox4.Controls.Add(this.Herkulex5_On_radioButton);
            this.groupBox4.Controls.Add(this.Herkulex4_On_radioButton);
            this.groupBox4.Controls.Add(this.Herkulex3_On_radioButton);
            this.groupBox4.Controls.Add(this.Herkulex2_On_radioButton);
            this.groupBox4.Controls.Add(this.Herkulex1_On_radioButton);
            this.groupBox4.Controls.Add(this.Herkulex5_Position_Value_Label);
            this.groupBox4.Controls.Add(this.label20);
            this.groupBox4.Controls.Add(this.Herkulex4_Temperature_label);
            this.groupBox4.Controls.Add(this.Herkulex4_Position_Value_Label);
            this.groupBox4.Controls.Add(this.Herkulex3_Temperature_label);
            this.groupBox4.Controls.Add(this.Herkulex5_Temperature_label);
            this.groupBox4.Controls.Add(this.Herkulex3_Position_Value_Label);
            this.groupBox4.Controls.Add(this.Herkulex2_Temperature_label);
            this.groupBox4.Controls.Add(this.label31);
            this.groupBox4.Controls.Add(this.Herkulex2_Position_Value_Label);
            this.groupBox4.Controls.Add(this.Herkulex1_Temperature_label);
            this.groupBox4.Controls.Add(this.label30);
            this.groupBox4.Controls.Add(this.Herkulex1_Position_Value_Label);
            this.groupBox4.Controls.Add(this.Herkulex5_Position_label);
            this.groupBox4.Controls.Add(this.label29);
            this.groupBox4.Controls.Add(this.Herkulex5_Position_hScrollBar);
            this.groupBox4.Controls.Add(this.Herkulex4_Position_label);
            this.groupBox4.Controls.Add(this.label28);
            this.groupBox4.Controls.Add(this.Herkulex4_Position_hScrollBar);
            this.groupBox4.Controls.Add(this.Herkulex3_Position_label);
            this.groupBox4.Controls.Add(this.label27);
            this.groupBox4.Controls.Add(this.Herkulex3_Position_hScrollBar);
            this.groupBox4.Controls.Add(this.Herkulex2_Position_label);
            this.groupBox4.Controls.Add(this.label33);
            this.groupBox4.Controls.Add(this.Herkulex2_Position_hScrollBar);
            this.groupBox4.Controls.Add(this.Herkulex1_Position_label);
            this.groupBox4.Controls.Add(this.label32);
            this.groupBox4.Controls.Add(this.Herkulex1_Position_hScrollBar);
            this.groupBox4.Controls.Add(this.Herkulex5_Torque_label);
            this.groupBox4.Controls.Add(this.label34);
            this.groupBox4.Controls.Add(this.Herkulex4_Torque_label);
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Controls.Add(this.label35);
            this.groupBox4.Controls.Add(this.Herkulex3_Torque_label);
            this.groupBox4.Controls.Add(this.label18);
            this.groupBox4.Controls.Add(this.label36);
            this.groupBox4.Controls.Add(this.Herkulex2_Torque_label);
            this.groupBox4.Controls.Add(this.label21);
            this.groupBox4.Controls.Add(this.Herkulex1_Torque_label);
            this.groupBox4.Controls.Add(this.label22);
            this.groupBox4.Controls.Add(this.Herkulex5_Status_label);
            this.groupBox4.Controls.Add(this.label23);
            this.groupBox4.Controls.Add(this.Herkulex4_Status_label);
            this.groupBox4.Controls.Add(this.label24);
            this.groupBox4.Controls.Add(this.Herkulex3_Status_label);
            this.groupBox4.Controls.Add(this.label25);
            this.groupBox4.Controls.Add(this.Herkulex2_Status_label);
            this.groupBox4.Controls.Add(this.label26);
            this.groupBox4.Controls.Add(this.Herkulex1_Status_label);
            this.groupBox4.Enabled = false;
            this.groupBox4.Location = new System.Drawing.Point(7, 264);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(331, 288);
            this.groupBox4.TabIndex = 38;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Herkulex Control";
            // 
            // Herkulex5_clear_button
            // 
            this.Herkulex5_clear_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Herkulex5_clear_button.Location = new System.Drawing.Point(274, 111);
            this.Herkulex5_clear_button.Name = "Herkulex5_clear_button";
            this.Herkulex5_clear_button.Size = new System.Drawing.Size(35, 17);
            this.Herkulex5_clear_button.TabIndex = 127;
            this.Herkulex5_clear_button.Text = "clear";
            this.Herkulex5_clear_button.UseVisualStyleBackColor = true;
            this.Herkulex5_clear_button.Click += new System.EventHandler(this.Herkulex5_clear_button_Click);
            // 
            // Herkulex4_clear_button
            // 
            this.Herkulex4_clear_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Herkulex4_clear_button.Location = new System.Drawing.Point(274, 91);
            this.Herkulex4_clear_button.Name = "Herkulex4_clear_button";
            this.Herkulex4_clear_button.Size = new System.Drawing.Size(35, 17);
            this.Herkulex4_clear_button.TabIndex = 126;
            this.Herkulex4_clear_button.Text = "clear";
            this.Herkulex4_clear_button.UseVisualStyleBackColor = true;
            this.Herkulex4_clear_button.Click += new System.EventHandler(this.Herkulex4_clear_button_Click);
            // 
            // Herkulex3_clear_button
            // 
            this.Herkulex3_clear_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Herkulex3_clear_button.Location = new System.Drawing.Point(274, 71);
            this.Herkulex3_clear_button.Name = "Herkulex3_clear_button";
            this.Herkulex3_clear_button.Size = new System.Drawing.Size(35, 17);
            this.Herkulex3_clear_button.TabIndex = 125;
            this.Herkulex3_clear_button.Text = "clear";
            this.Herkulex3_clear_button.UseVisualStyleBackColor = true;
            this.Herkulex3_clear_button.Click += new System.EventHandler(this.Herkulex3_clear_button_Click);
            // 
            // Herkulex2_clear_button
            // 
            this.Herkulex2_clear_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Herkulex2_clear_button.Location = new System.Drawing.Point(274, 51);
            this.Herkulex2_clear_button.Name = "Herkulex2_clear_button";
            this.Herkulex2_clear_button.Size = new System.Drawing.Size(35, 17);
            this.Herkulex2_clear_button.TabIndex = 124;
            this.Herkulex2_clear_button.Text = "clear";
            this.Herkulex2_clear_button.UseVisualStyleBackColor = true;
            this.Herkulex2_clear_button.Click += new System.EventHandler(this.Herkulex2_clear_button_Click);
            // 
            // Herkulex1_clear_button
            // 
            this.Herkulex1_clear_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Herkulex1_clear_button.Location = new System.Drawing.Point(274, 32);
            this.Herkulex1_clear_button.Name = "Herkulex1_clear_button";
            this.Herkulex1_clear_button.Size = new System.Drawing.Size(35, 17);
            this.Herkulex1_clear_button.TabIndex = 123;
            this.Herkulex1_clear_button.Text = "clear";
            this.Herkulex1_clear_button.UseVisualStyleBackColor = true;
            this.Herkulex1_clear_button.Click += new System.EventHandler(this.Herkulex1_clear_button_Click);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(260, 16);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(64, 13);
            this.label37.TabIndex = 122;
            this.label37.Text = "Clear Status";
            // 
            // Herkulex_Play_Time_label
            // 
            this.Herkulex_Play_Time_label.AutoSize = true;
            this.Herkulex_Play_Time_label.Location = new System.Drawing.Point(298, 263);
            this.Herkulex_Play_Time_label.Name = "Herkulex_Play_Time_label";
            this.Herkulex_Play_Time_label.Size = new System.Drawing.Size(25, 13);
            this.Herkulex_Play_Time_label.TabIndex = 121;
            this.Herkulex_Play_Time_label.Text = "100";
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Location = new System.Drawing.Point(126, 267);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(56, 13);
            this.label75.TabIndex = 120;
            this.label75.Text = "Play Time:";
            // 
            // Herkulex_Play_Time_hScrollBar
            // 
            this.Herkulex_Play_Time_hScrollBar.Location = new System.Drawing.Point(194, 262);
            this.Herkulex_Play_Time_hScrollBar.Maximum = 255;
            this.Herkulex_Play_Time_hScrollBar.Name = "Herkulex_Play_Time_hScrollBar";
            this.Herkulex_Play_Time_hScrollBar.Size = new System.Drawing.Size(80, 17);
            this.Herkulex_Play_Time_hScrollBar.TabIndex = 119;
            this.Herkulex_Play_Time_hScrollBar.Value = 100;
            this.Herkulex_Play_Time_hScrollBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.Herkulex_Play_Time_hScrollBar_Scroll);
            // 
            // Herkulex5_Brake_radioButton
            // 
            this.Herkulex5_Brake_radioButton.AutoCheck = false;
            this.Herkulex5_Brake_radioButton.AutoSize = true;
            this.Herkulex5_Brake_radioButton.Location = new System.Drawing.Point(149, 246);
            this.Herkulex5_Brake_radioButton.Name = "Herkulex5_Brake_radioButton";
            this.Herkulex5_Brake_radioButton.Size = new System.Drawing.Size(14, 13);
            this.Herkulex5_Brake_radioButton.TabIndex = 118;
            this.Herkulex5_Brake_radioButton.TabStop = true;
            this.Herkulex5_Brake_radioButton.UseVisualStyleBackColor = true;
            this.Herkulex5_Brake_radioButton.Click += new System.EventHandler(this.Herkulex5_Brake_radioButton_Click);
            // 
            // Herkulex4_Brake_radioButton
            // 
            this.Herkulex4_Brake_radioButton.AutoCheck = false;
            this.Herkulex4_Brake_radioButton.AutoSize = true;
            this.Herkulex4_Brake_radioButton.Location = new System.Drawing.Point(149, 226);
            this.Herkulex4_Brake_radioButton.Name = "Herkulex4_Brake_radioButton";
            this.Herkulex4_Brake_radioButton.Size = new System.Drawing.Size(14, 13);
            this.Herkulex4_Brake_radioButton.TabIndex = 117;
            this.Herkulex4_Brake_radioButton.TabStop = true;
            this.Herkulex4_Brake_radioButton.UseVisualStyleBackColor = true;
            this.Herkulex4_Brake_radioButton.Click += new System.EventHandler(this.Herkulex4_Brake_radioButton_Click);
            // 
            // Herkulex3_Brake_radioButton
            // 
            this.Herkulex3_Brake_radioButton.AutoCheck = false;
            this.Herkulex3_Brake_radioButton.AutoSize = true;
            this.Herkulex3_Brake_radioButton.Location = new System.Drawing.Point(149, 204);
            this.Herkulex3_Brake_radioButton.Name = "Herkulex3_Brake_radioButton";
            this.Herkulex3_Brake_radioButton.Size = new System.Drawing.Size(14, 13);
            this.Herkulex3_Brake_radioButton.TabIndex = 116;
            this.Herkulex3_Brake_radioButton.TabStop = true;
            this.Herkulex3_Brake_radioButton.UseVisualStyleBackColor = true;
            this.Herkulex3_Brake_radioButton.Click += new System.EventHandler(this.Herkulex3_Brake_radioButton_Click);
            // 
            // Herkulex2_Brake_radioButton
            // 
            this.Herkulex2_Brake_radioButton.AutoCheck = false;
            this.Herkulex2_Brake_radioButton.AutoSize = true;
            this.Herkulex2_Brake_radioButton.Location = new System.Drawing.Point(149, 184);
            this.Herkulex2_Brake_radioButton.Name = "Herkulex2_Brake_radioButton";
            this.Herkulex2_Brake_radioButton.Size = new System.Drawing.Size(14, 13);
            this.Herkulex2_Brake_radioButton.TabIndex = 115;
            this.Herkulex2_Brake_radioButton.TabStop = true;
            this.Herkulex2_Brake_radioButton.UseVisualStyleBackColor = true;
            this.Herkulex2_Brake_radioButton.Click += new System.EventHandler(this.Herkulex2_Brake_radioButton_Click);
            // 
            // Herkulex1_Brake_radioButton
            // 
            this.Herkulex1_Brake_radioButton.AutoCheck = false;
            this.Herkulex1_Brake_radioButton.AutoSize = true;
            this.Herkulex1_Brake_radioButton.Location = new System.Drawing.Point(149, 165);
            this.Herkulex1_Brake_radioButton.Name = "Herkulex1_Brake_radioButton";
            this.Herkulex1_Brake_radioButton.Size = new System.Drawing.Size(14, 13);
            this.Herkulex1_Brake_radioButton.TabIndex = 114;
            this.Herkulex1_Brake_radioButton.TabStop = true;
            this.Herkulex1_Brake_radioButton.UseVisualStyleBackColor = true;
            this.Herkulex1_Brake_radioButton.Click += new System.EventHandler(this.Herkulex1_Brake_radioButton_Click);
            // 
            // Herkulex5_Off_radioButton
            // 
            this.Herkulex5_Off_radioButton.AutoCheck = false;
            this.Herkulex5_Off_radioButton.AutoSize = true;
            this.Herkulex5_Off_radioButton.Checked = true;
            this.Herkulex5_Off_radioButton.Location = new System.Drawing.Point(120, 246);
            this.Herkulex5_Off_radioButton.Name = "Herkulex5_Off_radioButton";
            this.Herkulex5_Off_radioButton.Size = new System.Drawing.Size(14, 13);
            this.Herkulex5_Off_radioButton.TabIndex = 113;
            this.Herkulex5_Off_radioButton.TabStop = true;
            this.Herkulex5_Off_radioButton.UseVisualStyleBackColor = true;
            this.Herkulex5_Off_radioButton.Click += new System.EventHandler(this.Herkulex5_Off_radioButton_Click);
            // 
            // Herkulex4_Off_radioButton
            // 
            this.Herkulex4_Off_radioButton.AutoCheck = false;
            this.Herkulex4_Off_radioButton.AutoSize = true;
            this.Herkulex4_Off_radioButton.Checked = true;
            this.Herkulex4_Off_radioButton.Location = new System.Drawing.Point(120, 226);
            this.Herkulex4_Off_radioButton.Name = "Herkulex4_Off_radioButton";
            this.Herkulex4_Off_radioButton.Size = new System.Drawing.Size(14, 13);
            this.Herkulex4_Off_radioButton.TabIndex = 112;
            this.Herkulex4_Off_radioButton.TabStop = true;
            this.Herkulex4_Off_radioButton.UseVisualStyleBackColor = true;
            this.Herkulex4_Off_radioButton.Click += new System.EventHandler(this.Herkulex4_Off_radioButton_Click);
            // 
            // Herkulex3_Off_radioButton
            // 
            this.Herkulex3_Off_radioButton.AutoCheck = false;
            this.Herkulex3_Off_radioButton.AutoSize = true;
            this.Herkulex3_Off_radioButton.Checked = true;
            this.Herkulex3_Off_radioButton.Location = new System.Drawing.Point(120, 204);
            this.Herkulex3_Off_radioButton.Name = "Herkulex3_Off_radioButton";
            this.Herkulex3_Off_radioButton.Size = new System.Drawing.Size(14, 13);
            this.Herkulex3_Off_radioButton.TabIndex = 111;
            this.Herkulex3_Off_radioButton.TabStop = true;
            this.Herkulex3_Off_radioButton.UseVisualStyleBackColor = true;
            this.Herkulex3_Off_radioButton.Click += new System.EventHandler(this.Herkulex3_Off_radioButton_Click);
            // 
            // Herkulex2_Off_radioButton
            // 
            this.Herkulex2_Off_radioButton.AutoCheck = false;
            this.Herkulex2_Off_radioButton.AutoSize = true;
            this.Herkulex2_Off_radioButton.Checked = true;
            this.Herkulex2_Off_radioButton.Location = new System.Drawing.Point(120, 184);
            this.Herkulex2_Off_radioButton.Name = "Herkulex2_Off_radioButton";
            this.Herkulex2_Off_radioButton.Size = new System.Drawing.Size(14, 13);
            this.Herkulex2_Off_radioButton.TabIndex = 110;
            this.Herkulex2_Off_radioButton.TabStop = true;
            this.Herkulex2_Off_radioButton.UseVisualStyleBackColor = true;
            this.Herkulex2_Off_radioButton.Click += new System.EventHandler(this.Herkulex2_Off_radioButton_Click);
            // 
            // Herkulex1_Off_radioButton
            // 
            this.Herkulex1_Off_radioButton.AutoCheck = false;
            this.Herkulex1_Off_radioButton.AutoSize = true;
            this.Herkulex1_Off_radioButton.Checked = true;
            this.Herkulex1_Off_radioButton.Location = new System.Drawing.Point(120, 165);
            this.Herkulex1_Off_radioButton.Name = "Herkulex1_Off_radioButton";
            this.Herkulex1_Off_radioButton.Size = new System.Drawing.Size(14, 13);
            this.Herkulex1_Off_radioButton.TabIndex = 109;
            this.Herkulex1_Off_radioButton.TabStop = true;
            this.Herkulex1_Off_radioButton.UseVisualStyleBackColor = true;
            this.Herkulex1_Off_radioButton.Click += new System.EventHandler(this.Herkulex1_Off_radioButton_Click);
            // 
            // Herkulex5_On_radioButton
            // 
            this.Herkulex5_On_radioButton.AutoCheck = false;
            this.Herkulex5_On_radioButton.AutoSize = true;
            this.Herkulex5_On_radioButton.Location = new System.Drawing.Point(91, 246);
            this.Herkulex5_On_radioButton.Name = "Herkulex5_On_radioButton";
            this.Herkulex5_On_radioButton.Size = new System.Drawing.Size(14, 13);
            this.Herkulex5_On_radioButton.TabIndex = 108;
            this.Herkulex5_On_radioButton.TabStop = true;
            this.Herkulex5_On_radioButton.UseVisualStyleBackColor = true;
            this.Herkulex5_On_radioButton.Click += new System.EventHandler(this.Herkulex5_On_radioButton_Click);
            // 
            // Herkulex4_On_radioButton
            // 
            this.Herkulex4_On_radioButton.AutoCheck = false;
            this.Herkulex4_On_radioButton.AutoSize = true;
            this.Herkulex4_On_radioButton.Location = new System.Drawing.Point(91, 226);
            this.Herkulex4_On_radioButton.Name = "Herkulex4_On_radioButton";
            this.Herkulex4_On_radioButton.Size = new System.Drawing.Size(14, 13);
            this.Herkulex4_On_radioButton.TabIndex = 107;
            this.Herkulex4_On_radioButton.TabStop = true;
            this.Herkulex4_On_radioButton.UseVisualStyleBackColor = true;
            this.Herkulex4_On_radioButton.Click += new System.EventHandler(this.Herkulex4_On_radioButton_Click);
            // 
            // Herkulex3_On_radioButton
            // 
            this.Herkulex3_On_radioButton.AutoCheck = false;
            this.Herkulex3_On_radioButton.AutoSize = true;
            this.Herkulex3_On_radioButton.Location = new System.Drawing.Point(91, 204);
            this.Herkulex3_On_radioButton.Name = "Herkulex3_On_radioButton";
            this.Herkulex3_On_radioButton.Size = new System.Drawing.Size(14, 13);
            this.Herkulex3_On_radioButton.TabIndex = 106;
            this.Herkulex3_On_radioButton.TabStop = true;
            this.Herkulex3_On_radioButton.UseVisualStyleBackColor = true;
            this.Herkulex3_On_radioButton.Click += new System.EventHandler(this.Herkulex3_On_radioButton_Click);
            // 
            // Herkulex2_On_radioButton
            // 
            this.Herkulex2_On_radioButton.AutoCheck = false;
            this.Herkulex2_On_radioButton.AutoSize = true;
            this.Herkulex2_On_radioButton.Location = new System.Drawing.Point(91, 184);
            this.Herkulex2_On_radioButton.Name = "Herkulex2_On_radioButton";
            this.Herkulex2_On_radioButton.Size = new System.Drawing.Size(14, 13);
            this.Herkulex2_On_radioButton.TabIndex = 105;
            this.Herkulex2_On_radioButton.TabStop = true;
            this.Herkulex2_On_radioButton.UseVisualStyleBackColor = true;
            this.Herkulex2_On_radioButton.Click += new System.EventHandler(this.Herkulex2_On_radioButton_Click);
            // 
            // Herkulex1_On_radioButton
            // 
            this.Herkulex1_On_radioButton.AutoCheck = false;
            this.Herkulex1_On_radioButton.AutoSize = true;
            this.Herkulex1_On_radioButton.Location = new System.Drawing.Point(91, 165);
            this.Herkulex1_On_radioButton.Name = "Herkulex1_On_radioButton";
            this.Herkulex1_On_radioButton.Size = new System.Drawing.Size(14, 13);
            this.Herkulex1_On_radioButton.TabIndex = 104;
            this.Herkulex1_On_radioButton.TabStop = true;
            this.Herkulex1_On_radioButton.UseVisualStyleBackColor = true;
            this.Herkulex1_On_radioButton.Click += new System.EventHandler(this.Herkulex1_On_radioButton_Click);
            // 
            // Herkulex5_Position_Value_Label
            // 
            this.Herkulex5_Position_Value_Label.AutoSize = true;
            this.Herkulex5_Position_Value_Label.Location = new System.Drawing.Point(299, 243);
            this.Herkulex5_Position_Value_Label.Name = "Herkulex5_Position_Value_Label";
            this.Herkulex5_Position_Value_Label.Size = new System.Drawing.Size(25, 13);
            this.Herkulex5_Position_Value_Label.TabIndex = 103;
            this.Herkulex5_Position_Value_Label.Text = "512";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(83, 16);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(37, 13);
            this.label20.TabIndex = 44;
            this.label20.Text = "Status";
            // 
            // Herkulex4_Temperature_label
            // 
            this.Herkulex4_Temperature_label.AutoSize = true;
            this.Herkulex4_Temperature_label.Location = new System.Drawing.Point(235, 95);
            this.Herkulex4_Temperature_label.Name = "Herkulex4_Temperature_label";
            this.Herkulex4_Temperature_label.Size = new System.Drawing.Size(13, 13);
            this.Herkulex4_Temperature_label.TabIndex = 66;
            this.Herkulex4_Temperature_label.Text = "0";
            // 
            // Herkulex4_Position_Value_Label
            // 
            this.Herkulex4_Position_Value_Label.AutoSize = true;
            this.Herkulex4_Position_Value_Label.Location = new System.Drawing.Point(299, 223);
            this.Herkulex4_Position_Value_Label.Name = "Herkulex4_Position_Value_Label";
            this.Herkulex4_Position_Value_Label.Size = new System.Drawing.Size(25, 13);
            this.Herkulex4_Position_Value_Label.TabIndex = 102;
            this.Herkulex4_Position_Value_Label.Text = "512";
            // 
            // Herkulex3_Temperature_label
            // 
            this.Herkulex3_Temperature_label.AutoSize = true;
            this.Herkulex3_Temperature_label.Location = new System.Drawing.Point(235, 75);
            this.Herkulex3_Temperature_label.Name = "Herkulex3_Temperature_label";
            this.Herkulex3_Temperature_label.Size = new System.Drawing.Size(13, 13);
            this.Herkulex3_Temperature_label.TabIndex = 65;
            this.Herkulex3_Temperature_label.Text = "0";
            // 
            // Herkulex5_Temperature_label
            // 
            this.Herkulex5_Temperature_label.AutoSize = true;
            this.Herkulex5_Temperature_label.Location = new System.Drawing.Point(235, 115);
            this.Herkulex5_Temperature_label.Name = "Herkulex5_Temperature_label";
            this.Herkulex5_Temperature_label.Size = new System.Drawing.Size(13, 13);
            this.Herkulex5_Temperature_label.TabIndex = 67;
            this.Herkulex5_Temperature_label.Text = "0";
            // 
            // Herkulex3_Position_Value_Label
            // 
            this.Herkulex3_Position_Value_Label.AutoSize = true;
            this.Herkulex3_Position_Value_Label.Location = new System.Drawing.Point(299, 204);
            this.Herkulex3_Position_Value_Label.Name = "Herkulex3_Position_Value_Label";
            this.Herkulex3_Position_Value_Label.Size = new System.Drawing.Size(25, 13);
            this.Herkulex3_Position_Value_Label.TabIndex = 101;
            this.Herkulex3_Position_Value_Label.Text = "512";
            // 
            // Herkulex2_Temperature_label
            // 
            this.Herkulex2_Temperature_label.AutoSize = true;
            this.Herkulex2_Temperature_label.Location = new System.Drawing.Point(235, 55);
            this.Herkulex2_Temperature_label.Name = "Herkulex2_Temperature_label";
            this.Herkulex2_Temperature_label.Size = new System.Drawing.Size(13, 13);
            this.Herkulex2_Temperature_label.TabIndex = 64;
            this.Herkulex2_Temperature_label.Text = "0";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(16, 165);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(55, 13);
            this.label31.TabIndex = 68;
            this.label31.Text = "Herkulex1";
            // 
            // Herkulex2_Position_Value_Label
            // 
            this.Herkulex2_Position_Value_Label.AutoSize = true;
            this.Herkulex2_Position_Value_Label.Location = new System.Drawing.Point(299, 185);
            this.Herkulex2_Position_Value_Label.Name = "Herkulex2_Position_Value_Label";
            this.Herkulex2_Position_Value_Label.Size = new System.Drawing.Size(25, 13);
            this.Herkulex2_Position_Value_Label.TabIndex = 100;
            this.Herkulex2_Position_Value_Label.Text = "512";
            // 
            // Herkulex1_Temperature_label
            // 
            this.Herkulex1_Temperature_label.AutoSize = true;
            this.Herkulex1_Temperature_label.Location = new System.Drawing.Point(235, 35);
            this.Herkulex1_Temperature_label.Name = "Herkulex1_Temperature_label";
            this.Herkulex1_Temperature_label.Size = new System.Drawing.Size(13, 13);
            this.Herkulex1_Temperature_label.TabIndex = 63;
            this.Herkulex1_Temperature_label.Text = "0";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(16, 185);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(55, 13);
            this.label30.TabIndex = 69;
            this.label30.Text = "Herkulex2";
            // 
            // Herkulex1_Position_Value_Label
            // 
            this.Herkulex1_Position_Value_Label.AutoSize = true;
            this.Herkulex1_Position_Value_Label.Location = new System.Drawing.Point(299, 166);
            this.Herkulex1_Position_Value_Label.Name = "Herkulex1_Position_Value_Label";
            this.Herkulex1_Position_Value_Label.Size = new System.Drawing.Size(25, 13);
            this.Herkulex1_Position_Value_Label.TabIndex = 99;
            this.Herkulex1_Position_Value_Label.Text = "512";
            // 
            // Herkulex5_Position_label
            // 
            this.Herkulex5_Position_label.AutoSize = true;
            this.Herkulex5_Position_label.Location = new System.Drawing.Point(191, 115);
            this.Herkulex5_Position_label.Name = "Herkulex5_Position_label";
            this.Herkulex5_Position_label.Size = new System.Drawing.Size(13, 13);
            this.Herkulex5_Position_label.TabIndex = 62;
            this.Herkulex5_Position_label.Text = "0";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(16, 205);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(55, 13);
            this.label29.TabIndex = 70;
            this.label29.Text = "Herkulex3";
            // 
            // Herkulex5_Position_hScrollBar
            // 
            this.Herkulex5_Position_hScrollBar.Location = new System.Drawing.Point(194, 241);
            this.Herkulex5_Position_hScrollBar.Maximum = 1024;
            this.Herkulex5_Position_hScrollBar.Minimum = 24;
            this.Herkulex5_Position_hScrollBar.Name = "Herkulex5_Position_hScrollBar";
            this.Herkulex5_Position_hScrollBar.Size = new System.Drawing.Size(80, 17);
            this.Herkulex5_Position_hScrollBar.TabIndex = 98;
            this.Herkulex5_Position_hScrollBar.Value = 512;
            this.Herkulex5_Position_hScrollBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.Herkulex5_Position_hScrollBar_Scroll);
            // 
            // Herkulex4_Position_label
            // 
            this.Herkulex4_Position_label.AutoSize = true;
            this.Herkulex4_Position_label.Location = new System.Drawing.Point(191, 95);
            this.Herkulex4_Position_label.Name = "Herkulex4_Position_label";
            this.Herkulex4_Position_label.Size = new System.Drawing.Size(13, 13);
            this.Herkulex4_Position_label.TabIndex = 61;
            this.Herkulex4_Position_label.Text = "0";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(16, 225);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(55, 13);
            this.label28.TabIndex = 71;
            this.label28.Text = "Herkulex4";
            // 
            // Herkulex4_Position_hScrollBar
            // 
            this.Herkulex4_Position_hScrollBar.Location = new System.Drawing.Point(194, 222);
            this.Herkulex4_Position_hScrollBar.Maximum = 1024;
            this.Herkulex4_Position_hScrollBar.Minimum = 24;
            this.Herkulex4_Position_hScrollBar.Name = "Herkulex4_Position_hScrollBar";
            this.Herkulex4_Position_hScrollBar.Size = new System.Drawing.Size(80, 17);
            this.Herkulex4_Position_hScrollBar.TabIndex = 97;
            this.Herkulex4_Position_hScrollBar.Value = 512;
            this.Herkulex4_Position_hScrollBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.Herkulex4_Position_hScrollBar_Scroll);
            // 
            // Herkulex3_Position_label
            // 
            this.Herkulex3_Position_label.AutoSize = true;
            this.Herkulex3_Position_label.Location = new System.Drawing.Point(191, 75);
            this.Herkulex3_Position_label.Name = "Herkulex3_Position_label";
            this.Herkulex3_Position_label.Size = new System.Drawing.Size(13, 13);
            this.Herkulex3_Position_label.TabIndex = 60;
            this.Herkulex3_Position_label.Text = "0";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(16, 245);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(55, 13);
            this.label27.TabIndex = 72;
            this.label27.Text = "Herkulex5";
            // 
            // Herkulex3_Position_hScrollBar
            // 
            this.Herkulex3_Position_hScrollBar.Location = new System.Drawing.Point(194, 203);
            this.Herkulex3_Position_hScrollBar.Maximum = 2048;
            this.Herkulex3_Position_hScrollBar.Minimum = 24;
            this.Herkulex3_Position_hScrollBar.Name = "Herkulex3_Position_hScrollBar";
            this.Herkulex3_Position_hScrollBar.Size = new System.Drawing.Size(80, 17);
            this.Herkulex3_Position_hScrollBar.TabIndex = 96;
            this.Herkulex3_Position_hScrollBar.Value = 512;
            this.Herkulex3_Position_hScrollBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.Herkulex3_Position_hScrollBar_Scroll);
            // 
            // Herkulex2_Position_label
            // 
            this.Herkulex2_Position_label.AutoSize = true;
            this.Herkulex2_Position_label.Location = new System.Drawing.Point(191, 55);
            this.Herkulex2_Position_label.Name = "Herkulex2_Position_label";
            this.Herkulex2_Position_label.Size = new System.Drawing.Size(13, 13);
            this.Herkulex2_Position_label.TabIndex = 59;
            this.Herkulex2_Position_label.Text = "0";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(107, 136);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(41, 13);
            this.label33.TabIndex = 74;
            this.label33.Text = "Torque";
            // 
            // Herkulex2_Position_hScrollBar
            // 
            this.Herkulex2_Position_hScrollBar.Location = new System.Drawing.Point(194, 184);
            this.Herkulex2_Position_hScrollBar.Maximum = 2048;
            this.Herkulex2_Position_hScrollBar.Minimum = 24;
            this.Herkulex2_Position_hScrollBar.Name = "Herkulex2_Position_hScrollBar";
            this.Herkulex2_Position_hScrollBar.Size = new System.Drawing.Size(80, 17);
            this.Herkulex2_Position_hScrollBar.TabIndex = 95;
            this.Herkulex2_Position_hScrollBar.Value = 512;
            this.Herkulex2_Position_hScrollBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.Herkulex2_Position_hScrollBar_Scroll);
            // 
            // Herkulex1_Position_label
            // 
            this.Herkulex1_Position_label.AutoSize = true;
            this.Herkulex1_Position_label.Location = new System.Drawing.Point(191, 35);
            this.Herkulex1_Position_label.Name = "Herkulex1_Position_label";
            this.Herkulex1_Position_label.Size = new System.Drawing.Size(13, 13);
            this.Herkulex1_Position_label.TabIndex = 58;
            this.Herkulex1_Position_label.Text = "0";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(217, 136);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(44, 13);
            this.label32.TabIndex = 75;
            this.label32.Text = "Position";
            // 
            // Herkulex1_Position_hScrollBar
            // 
            this.Herkulex1_Position_hScrollBar.Location = new System.Drawing.Point(194, 165);
            this.Herkulex1_Position_hScrollBar.Maximum = 1024;
            this.Herkulex1_Position_hScrollBar.Minimum = 24;
            this.Herkulex1_Position_hScrollBar.Name = "Herkulex1_Position_hScrollBar";
            this.Herkulex1_Position_hScrollBar.Size = new System.Drawing.Size(80, 17);
            this.Herkulex1_Position_hScrollBar.TabIndex = 94;
            this.Herkulex1_Position_hScrollBar.Value = 512;
            this.Herkulex1_Position_hScrollBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.Herkulex1_Position_hScrollBar_Scroll);
            // 
            // Herkulex5_Torque_label
            // 
            this.Herkulex5_Torque_label.AutoSize = true;
            this.Herkulex5_Torque_label.Location = new System.Drawing.Point(146, 115);
            this.Herkulex5_Torque_label.Name = "Herkulex5_Torque_label";
            this.Herkulex5_Torque_label.Size = new System.Drawing.Size(13, 13);
            this.Herkulex5_Torque_label.TabIndex = 57;
            this.Herkulex5_Torque_label.Text = "0";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(88, 149);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(21, 13);
            this.label34.TabIndex = 76;
            this.label34.Text = "On";
            // 
            // Herkulex4_Torque_label
            // 
            this.Herkulex4_Torque_label.AutoSize = true;
            this.Herkulex4_Torque_label.Location = new System.Drawing.Point(146, 95);
            this.Herkulex4_Torque_label.Name = "Herkulex4_Torque_label";
            this.Herkulex4_Torque_label.Size = new System.Drawing.Size(13, 13);
            this.Herkulex4_Torque_label.TabIndex = 56;
            this.Herkulex4_Torque_label.Text = "0";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(16, 35);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(55, 13);
            this.label16.TabIndex = 38;
            this.label16.Text = "Herkulex1";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(117, 149);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(21, 13);
            this.label35.TabIndex = 77;
            this.label35.Text = "Off";
            // 
            // Herkulex3_Torque_label
            // 
            this.Herkulex3_Torque_label.AutoSize = true;
            this.Herkulex3_Torque_label.Location = new System.Drawing.Point(146, 75);
            this.Herkulex3_Torque_label.Name = "Herkulex3_Torque_label";
            this.Herkulex3_Torque_label.Size = new System.Drawing.Size(13, 13);
            this.Herkulex3_Torque_label.TabIndex = 55;
            this.Herkulex3_Torque_label.Text = "0";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(16, 55);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(55, 13);
            this.label18.TabIndex = 39;
            this.label18.Text = "Herkulex2";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(146, 149);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(35, 13);
            this.label36.TabIndex = 78;
            this.label36.Text = "Brake";
            // 
            // Herkulex2_Torque_label
            // 
            this.Herkulex2_Torque_label.AutoSize = true;
            this.Herkulex2_Torque_label.Location = new System.Drawing.Point(146, 55);
            this.Herkulex2_Torque_label.Name = "Herkulex2_Torque_label";
            this.Herkulex2_Torque_label.Size = new System.Drawing.Size(13, 13);
            this.Herkulex2_Torque_label.TabIndex = 54;
            this.Herkulex2_Torque_label.Text = "0";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(16, 75);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(55, 13);
            this.label21.TabIndex = 41;
            this.label21.Text = "Herkulex3";
            // 
            // Herkulex1_Torque_label
            // 
            this.Herkulex1_Torque_label.AutoSize = true;
            this.Herkulex1_Torque_label.Location = new System.Drawing.Point(146, 35);
            this.Herkulex1_Torque_label.Name = "Herkulex1_Torque_label";
            this.Herkulex1_Torque_label.Size = new System.Drawing.Size(13, 13);
            this.Herkulex1_Torque_label.TabIndex = 53;
            this.Herkulex1_Torque_label.Text = "0";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(16, 95);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(55, 13);
            this.label22.TabIndex = 42;
            this.label22.Text = "Herkulex4";
            // 
            // Herkulex5_Status_label
            // 
            this.Herkulex5_Status_label.AutoSize = true;
            this.Herkulex5_Status_label.Location = new System.Drawing.Point(91, 115);
            this.Herkulex5_Status_label.Name = "Herkulex5_Status_label";
            this.Herkulex5_Status_label.Size = new System.Drawing.Size(13, 13);
            this.Herkulex5_Status_label.TabIndex = 52;
            this.Herkulex5_Status_label.Text = "0";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(16, 115);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(55, 13);
            this.label23.TabIndex = 43;
            this.label23.Text = "Herkulex5";
            // 
            // Herkulex4_Status_label
            // 
            this.Herkulex4_Status_label.AutoSize = true;
            this.Herkulex4_Status_label.Location = new System.Drawing.Point(91, 95);
            this.Herkulex4_Status_label.Name = "Herkulex4_Status_label";
            this.Herkulex4_Status_label.Size = new System.Drawing.Size(13, 13);
            this.Herkulex4_Status_label.TabIndex = 51;
            this.Herkulex4_Status_label.Text = "0";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(126, 16);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(41, 13);
            this.label24.TabIndex = 45;
            this.label24.Text = "Torque";
            // 
            // Herkulex3_Status_label
            // 
            this.Herkulex3_Status_label.AutoSize = true;
            this.Herkulex3_Status_label.Location = new System.Drawing.Point(91, 75);
            this.Herkulex3_Status_label.Name = "Herkulex3_Status_label";
            this.Herkulex3_Status_label.Size = new System.Drawing.Size(13, 13);
            this.Herkulex3_Status_label.TabIndex = 50;
            this.Herkulex3_Status_label.Text = "0";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(173, 16);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(44, 13);
            this.label25.TabIndex = 46;
            this.label25.Text = "Position";
            // 
            // Herkulex2_Status_label
            // 
            this.Herkulex2_Status_label.AutoSize = true;
            this.Herkulex2_Status_label.Location = new System.Drawing.Point(91, 55);
            this.Herkulex2_Status_label.Name = "Herkulex2_Status_label";
            this.Herkulex2_Status_label.Size = new System.Drawing.Size(13, 13);
            this.Herkulex2_Status_label.TabIndex = 49;
            this.Herkulex2_Status_label.Text = "0";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(223, 16);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(34, 13);
            this.label26.TabIndex = 47;
            this.label26.Text = "Temp";
            // 
            // Herkulex1_Status_label
            // 
            this.Herkulex1_Status_label.AutoSize = true;
            this.Herkulex1_Status_label.Location = new System.Drawing.Point(91, 35);
            this.Herkulex1_Status_label.Name = "Herkulex1_Status_label";
            this.Herkulex1_Status_label.Size = new System.Drawing.Size(13, 13);
            this.Herkulex1_Status_label.TabIndex = 48;
            this.Herkulex1_Status_label.Text = "0";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(237, 94);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(68, 13);
            this.label12.TabIndex = 30;
            this.label12.Text = "Auxiliary Pins";
            // 
            // Motor_Bat_Min_label
            // 
            this.Motor_Bat_Min_label.AutoSize = true;
            this.Motor_Bat_Min_label.Location = new System.Drawing.Point(181, 236);
            this.Motor_Bat_Min_label.Name = "Motor_Bat_Min_label";
            this.Motor_Bat_Min_label.Size = new System.Drawing.Size(32, 13);
            this.Motor_Bat_Min_label.TabIndex = 29;
            this.Motor_Bat_Min_label.Text = "0 min";
            // 
            // Integrated_Motor_Current_label
            // 
            this.Integrated_Motor_Current_label.AutoSize = true;
            this.Integrated_Motor_Current_label.Location = new System.Drawing.Point(161, 195);
            this.Integrated_Motor_Current_label.Name = "Integrated_Motor_Current_label";
            this.Integrated_Motor_Current_label.Size = new System.Drawing.Size(31, 13);
            this.Integrated_Motor_Current_label.TabIndex = 21;
            this.Integrated_Motor_Current_label.Text = "0 mA";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(102, 236);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(75, 13);
            this.label17.TabIndex = 27;
            this.label17.Text = "Motor Bat min:";
            // 
            // Integrated_Elect_Current_label
            // 
            this.Integrated_Elect_Current_label.AutoSize = true;
            this.Integrated_Elect_Current_label.Location = new System.Drawing.Point(161, 178);
            this.Integrated_Elect_Current_label.Name = "Integrated_Elect_Current_label";
            this.Integrated_Elect_Current_label.Size = new System.Drawing.Size(31, 13);
            this.Integrated_Elect_Current_label.TabIndex = 20;
            this.Integrated_Elect_Current_label.Text = "0 mA";
            // 
            // Motor_Level_label
            // 
            this.Motor_Level_label.AutoSize = true;
            this.Motor_Level_label.Location = new System.Drawing.Point(78, 236);
            this.Motor_Level_label.Name = "Motor_Level_label";
            this.Motor_Level_label.Size = new System.Drawing.Size(23, 13);
            this.Motor_Level_label.TabIndex = 28;
            this.Motor_Level_label.Text = "0 V";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(20, 195);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(125, 13);
            this.label10.TabIndex = 19;
            this.label10.Text = "Integrated Motor Current:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(13, 236);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(66, 13);
            this.label19.TabIndex = 26;
            this.label19.Text = "Motor Level:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(23, 178);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(122, 13);
            this.label11.TabIndex = 18;
            this.label11.Text = "Integrated Elect Current:";
            // 
            // Elect_Bat_Min_label
            // 
            this.Elect_Bat_Min_label.AutoSize = true;
            this.Elect_Bat_Min_label.Location = new System.Drawing.Point(181, 220);
            this.Elect_Bat_Min_label.Name = "Elect_Bat_Min_label";
            this.Elect_Bat_Min_label.Size = new System.Drawing.Size(32, 13);
            this.Elect_Bat_Min_label.TabIndex = 25;
            this.Elect_Bat_Min_label.Text = "0 min";
            // 
            // Instant_Motor_Current_label
            // 
            this.Instant_Motor_Current_label.AutoSize = true;
            this.Instant_Motor_Current_label.Location = new System.Drawing.Point(161, 153);
            this.Instant_Motor_Current_label.Name = "Instant_Motor_Current_label";
            this.Instant_Motor_Current_label.Size = new System.Drawing.Size(31, 13);
            this.Instant_Motor_Current_label.TabIndex = 17;
            this.Instant_Motor_Current_label.Text = "0 mA";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(105, 219);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(72, 13);
            this.label14.TabIndex = 23;
            this.label14.Text = "Elect Bat min:";
            // 
            // Instant_Elect_Current_label
            // 
            this.Instant_Elect_Current_label.AutoSize = true;
            this.Instant_Elect_Current_label.Location = new System.Drawing.Point(161, 136);
            this.Instant_Elect_Current_label.Name = "Instant_Elect_Current_label";
            this.Instant_Elect_Current_label.Size = new System.Drawing.Size(31, 13);
            this.Instant_Elect_Current_label.TabIndex = 16;
            this.Instant_Elect_Current_label.Text = "0 mA";
            // 
            // Elect_Level_label
            // 
            this.Elect_Level_label.AutoSize = true;
            this.Elect_Level_label.Location = new System.Drawing.Point(78, 219);
            this.Elect_Level_label.Name = "Elect_Level_label";
            this.Elect_Level_label.Size = new System.Drawing.Size(23, 13);
            this.Elect_Level_label.TabIndex = 24;
            this.Elect_Level_label.Text = "0 V";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(36, 153);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(109, 13);
            this.label9.TabIndex = 15;
            this.label9.Text = "Instant Motor Current:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(13, 219);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(63, 13);
            this.label15.TabIndex = 22;
            this.label15.Text = "Elect Level:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(39, 136);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(106, 13);
            this.label8.TabIndex = 14;
            this.label8.Text = "Instant Elect Current:";
            // 
            // Sensor_Firmware_Number_label
            // 
            this.Sensor_Firmware_Number_label.AutoSize = true;
            this.Sensor_Firmware_Number_label.Location = new System.Drawing.Point(130, 634);
            this.Sensor_Firmware_Number_label.Name = "Sensor_Firmware_Number_label";
            this.Sensor_Firmware_Number_label.Size = new System.Drawing.Size(148, 13);
            this.Sensor_Firmware_Number_label.TabIndex = 71;
            this.Sensor_Firmware_Number_label.Text = "-----------------------------------------------";
            this.Sensor_Firmware_Number_label.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(166, 604);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(93, 13);
            this.label46.TabIndex = 70;
            this.label46.Text = "Firmware Version: ";
            // 
            // Motor_Firmware_Number_label
            // 
            this.Motor_Firmware_Number_label.AutoSize = true;
            this.Motor_Firmware_Number_label.Location = new System.Drawing.Point(485, 634);
            this.Motor_Firmware_Number_label.Name = "Motor_Firmware_Number_label";
            this.Motor_Firmware_Number_label.Size = new System.Drawing.Size(148, 13);
            this.Motor_Firmware_Number_label.TabIndex = 73;
            this.Motor_Firmware_Number_label.Text = "-----------------------------------------------";
            this.Motor_Firmware_Number_label.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(516, 601);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(93, 13);
            this.label38.TabIndex = 72;
            this.label38.Text = "Firmware Version: ";
            this.label38.Click += new System.EventHandler(this.label38_Click);
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(13, 624);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(63, 13);
            this.label63.TabIndex = 74;
            this.label63.Text = "Serial COM:";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(376, 624);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(63, 13);
            this.label64.TabIndex = 75;
            this.label64.Text = "Serial COM:";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(288, 616);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(76, 21);
            this.comboBox1.TabIndex = 76;
            this.comboBox1.Visible = false;
            // 
            // timer1
            // 
            this.timer1.Interval = 50;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Sensor_radioButton
            // 
            this.Sensor_radioButton.AutoCheck = false;
            this.Sensor_radioButton.AutoSize = true;
            this.Sensor_radioButton.Enabled = false;
            this.Sensor_radioButton.Location = new System.Drawing.Point(13, 599);
            this.Sensor_radioButton.Name = "Sensor_radioButton";
            this.Sensor_radioButton.Size = new System.Drawing.Size(89, 17);
            this.Sensor_radioButton.TabIndex = 78;
            this.Sensor_radioButton.TabStop = true;
            this.Sensor_radioButton.Text = "Sensor Board";
            this.Sensor_radioButton.UseVisualStyleBackColor = true;
            this.Sensor_radioButton.Click += new System.EventHandler(this.Sensor_radioButton_Click);
            // 
            // Motor_radioButton
            // 
            this.Motor_radioButton.AutoCheck = false;
            this.Motor_radioButton.AutoSize = true;
            this.Motor_radioButton.Enabled = false;
            this.Motor_radioButton.Location = new System.Drawing.Point(379, 599);
            this.Motor_radioButton.Name = "Motor_radioButton";
            this.Motor_radioButton.Size = new System.Drawing.Size(83, 17);
            this.Motor_radioButton.TabIndex = 79;
            this.Motor_radioButton.TabStop = true;
            this.Motor_radioButton.Text = "Motor Board";
            this.Motor_radioButton.UseVisualStyleBackColor = true;
            this.Motor_radioButton.Click += new System.EventHandler(this.Motor_radioButton_Click);
            // 
            // Find_Serial_button
            // 
            this.Find_Serial_button.Location = new System.Drawing.Point(639, 604);
            this.Find_Serial_button.Name = "Find_Serial_button";
            this.Find_Serial_button.Size = new System.Drawing.Size(75, 23);
            this.Find_Serial_button.TabIndex = 80;
            this.Find_Serial_button.Text = "Find Serial";
            this.Find_Serial_button.UseVisualStyleBackColor = true;
            this.Find_Serial_button.Click += new System.EventHandler(this.Find_Serial_button_Click);
            // 
            // Sensor_Board_COM_label
            // 
            this.Sensor_Board_COM_label.AutoSize = true;
            this.Sensor_Board_COM_label.Location = new System.Drawing.Point(76, 624);
            this.Sensor_Board_COM_label.Name = "Sensor_Board_COM_label";
            this.Sensor_Board_COM_label.Size = new System.Drawing.Size(28, 13);
            this.Sensor_Board_COM_label.TabIndex = 81;
            this.Sensor_Board_COM_label.Text = "-------";
            // 
            // Motor_Board_COM_label
            // 
            this.Motor_Board_COM_label.AutoSize = true;
            this.Motor_Board_COM_label.Location = new System.Drawing.Point(438, 624);
            this.Motor_Board_COM_label.Name = "Motor_Board_COM_label";
            this.Motor_Board_COM_label.Size = new System.Drawing.Size(28, 13);
            this.Motor_Board_COM_label.TabIndex = 82;
            this.Motor_Board_COM_label.Text = "-------";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(763, 674);
            this.Controls.Add(this.Motor_Board_COM_label);
            this.Controls.Add(this.Sensor_Board_COM_label);
            this.Controls.Add(this.Find_Serial_button);
            this.Controls.Add(this.Motor_radioButton);
            this.Controls.Add(this.Sensor_radioButton);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label64);
            this.Controls.Add(this.label63);
            this.Controls.Add(this.Motor_Firmware_Number_label);
            this.Controls.Add(this.label38);
            this.Controls.Add(this.Sensor_Firmware_Number_label);
            this.Controls.Add(this.label46);
            this.Controls.Add(this.Motor_groupBox);
            this.Controls.Add(this.Sensor_groupBox);
            this.Controls.Add(this.Connect_Button);
            this.Name = "Form1";
            this.Text = "SIAR Boards Control";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Motor_groupBox.ResumeLayout(false);
            this.Motor_groupBox.PerformLayout();
            this.Linear_groupBox.ResumeLayout(false);
            this.Linear_groupBox.PerformLayout();
            this.Robot_Control_groupBox.ResumeLayout(false);
            this.Robot_Control_groupBox.PerformLayout();
            this.Ind_Control_groupBox.ResumeLayout(false);
            this.Ind_Control_groupBox.PerformLayout();
            this.Sensor_groupBox.ResumeLayout(false);
            this.Sensor_groupBox.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Connect_Button;
        private System.Windows.Forms.GroupBox Motor_groupBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label Elect_V_label;
        private System.Windows.Forms.Label Motor_V_label;
        private System.Windows.Forms.Label Charger_V_label;
        private System.Windows.Forms.Label Elect_I_label;
        private System.Windows.Forms.Label Motor_I_label;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label Electronic_Battery_Voltage_label;
        private System.Windows.Forms.Label Motor_Battery_Voltage_label;
        private System.Windows.Forms.GroupBox Sensor_groupBox;
        private System.Windows.Forms.Label Integrated_Motor_Current_label;
        private System.Windows.Forms.Label Integrated_Elect_Current_label;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label Instant_Motor_Current_label;
        private System.Windows.Forms.Label Instant_Elect_Current_label;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label Elect_Bat_Min_label;
        private System.Windows.Forms.Label Elect_Level_label;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label Motor_Bat_Min_label;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label Motor_Level_label;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label Herkulex5_Position_Value_Label;
        private System.Windows.Forms.Label Herkulex4_Position_Value_Label;
        private System.Windows.Forms.Label Herkulex3_Position_Value_Label;
        private System.Windows.Forms.Label Herkulex2_Position_Value_Label;
        private System.Windows.Forms.Label Herkulex1_Position_Value_Label;
        private System.Windows.Forms.HScrollBar Herkulex5_Position_hScrollBar;
        private System.Windows.Forms.HScrollBar Herkulex4_Position_hScrollBar;
        private System.Windows.Forms.HScrollBar Herkulex3_Position_hScrollBar;
        private System.Windows.Forms.HScrollBar Herkulex2_Position_hScrollBar;
        private System.Windows.Forms.HScrollBar Herkulex1_Position_hScrollBar;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label Herkulex5_Temperature_label;
        private System.Windows.Forms.Label Herkulex4_Temperature_label;
        private System.Windows.Forms.Label Herkulex3_Temperature_label;
        private System.Windows.Forms.Label Herkulex2_Temperature_label;
        private System.Windows.Forms.Label Herkulex1_Temperature_label;
        private System.Windows.Forms.Label Herkulex5_Position_label;
        private System.Windows.Forms.Label Herkulex4_Position_label;
        private System.Windows.Forms.Label Herkulex3_Position_label;
        private System.Windows.Forms.Label Herkulex2_Position_label;
        private System.Windows.Forms.Label Herkulex1_Position_label;
        private System.Windows.Forms.Label Herkulex5_Torque_label;
        private System.Windows.Forms.Label Herkulex4_Torque_label;
        private System.Windows.Forms.Label Herkulex3_Torque_label;
        private System.Windows.Forms.Label Herkulex2_Torque_label;
        private System.Windows.Forms.Label Herkulex1_Torque_label;
        private System.Windows.Forms.Label Herkulex5_Status_label;
        private System.Windows.Forms.Label Herkulex4_Status_label;
        private System.Windows.Forms.Label Herkulex3_Status_label;
        private System.Windows.Forms.Label Herkulex2_Status_label;
        private System.Windows.Forms.Label Herkulex1_Status_label;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label Sensor_Firmware_Number_label;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label Motor_Firmware_Number_label;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.GroupBox Robot_Control_groupBox;
        private System.Windows.Forms.Label Linear_Velocity_label;
        private System.Windows.Forms.Label Angular_Velocity_label;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.VScrollBar Linear_Velocity_vScrollBar;
        private System.Windows.Forms.HScrollBar Angular_Velocity_vScrollBar;
        private System.Windows.Forms.GroupBox Ind_Control_groupBox;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.VScrollBar Linear_Motor_Position_vScrollBar;
        private System.Windows.Forms.Label Rear_Right_label;
        private System.Windows.Forms.HScrollBar Front_Left_hScrollBar;
        private System.Windows.Forms.Label Mid_Right_label;
        private System.Windows.Forms.HScrollBar Mid_Left_hScrollBar;
        private System.Windows.Forms.Label Front_Right_label;
        private System.Windows.Forms.HScrollBar Rear_Left_hScrollBar;
        private System.Windows.Forms.Label Rear_Left_label;
        private System.Windows.Forms.HScrollBar Front_Right_hScrollBar;
        private System.Windows.Forms.Label Mid_Left_label;
        private System.Windows.Forms.HScrollBar Mid_Right_hScrollBar;
        private System.Windows.Forms.Label Front_Left_label;
        private System.Windows.Forms.HScrollBar Rear_Right_hScrollBar;
        private System.Windows.Forms.Label Linear_Motor_Position_label;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label Rear_Right_Ticks_label;
        private System.Windows.Forms.Label Mid_Right_Ticks_label;
        private System.Windows.Forms.Label Front_Right_Ticks_label;
        private System.Windows.Forms.Label Rear_Left_Ticks_label;
        private System.Windows.Forms.Label Mid_Left_Ticks_label;
        private System.Windows.Forms.Label Front_Left_Ticks_label;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label Potentiometer_label;
        private System.Windows.Forms.Label Linear_Position_Ticks_label;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label Hardstop_Timer_label;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.VScrollBar Hardstop_Timer_vScrollBar;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.RadioButton F5_Dir_radioButton;
        private System.Windows.Forms.RadioButton F4_Dir_radioButton;
        private System.Windows.Forms.RadioButton F3_Dir_radioButton;
        private System.Windows.Forms.RadioButton F2_Dir_radioButton;
        private System.Windows.Forms.RadioButton F1_Dir_radioButton;
        private System.Windows.Forms.RadioButton F0_Dir_radioButton;
        private System.Windows.Forms.CheckBox F5_Get_checkBox;
        private System.Windows.Forms.CheckBox F4_Get_checkBox;
        private System.Windows.Forms.CheckBox F3_Get_checkBox;
        private System.Windows.Forms.CheckBox F2_Get_checkBox;
        private System.Windows.Forms.CheckBox F1_Get_checkBox;
        private System.Windows.Forms.CheckBox F0_Get_checkBox;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.RadioButton F5_Set_radioButton;
        private System.Windows.Forms.RadioButton F4_Set_radioButton;
        private System.Windows.Forms.RadioButton F3_Set_radioButton;
        private System.Windows.Forms.RadioButton F2_Set_radioButton;
        private System.Windows.Forms.RadioButton F1_Set_radioButton;
        private System.Windows.Forms.RadioButton F0_Set_radioButton;
        private System.Windows.Forms.CheckBox Hardstop_Status_checkBox;
        private System.Windows.Forms.RadioButton Control_Stream_radioButton;
        private System.Windows.Forms.RadioButton Ind_Motor_Stream_radioButton;
        private System.Windows.Forms.RadioButton Hardstop_radioButton;
        private System.Windows.Forms.RadioButton Herkulex_radioButton;
        private System.Windows.Forms.RadioButton Sensor_radioButton;
        private System.Windows.Forms.RadioButton Herkulex5_Brake_radioButton;
        private System.Windows.Forms.RadioButton Herkulex4_Brake_radioButton;
        private System.Windows.Forms.RadioButton Herkulex3_Brake_radioButton;
        private System.Windows.Forms.RadioButton Herkulex2_Brake_radioButton;
        private System.Windows.Forms.RadioButton Herkulex1_Brake_radioButton;
        private System.Windows.Forms.RadioButton Herkulex5_Off_radioButton;
        private System.Windows.Forms.RadioButton Herkulex4_Off_radioButton;
        private System.Windows.Forms.RadioButton Herkulex3_Off_radioButton;
        private System.Windows.Forms.RadioButton Herkulex2_Off_radioButton;
        private System.Windows.Forms.RadioButton Herkulex1_Off_radioButton;
        private System.Windows.Forms.RadioButton Herkulex5_On_radioButton;
        private System.Windows.Forms.RadioButton Herkulex4_On_radioButton;
        private System.Windows.Forms.RadioButton Herkulex3_On_radioButton;
        private System.Windows.Forms.RadioButton Herkulex2_On_radioButton;
        private System.Windows.Forms.RadioButton Herkulex1_On_radioButton;
        private System.Windows.Forms.RadioButton Motor_radioButton;
        private System.Windows.Forms.Label Right_Wheel_Value_label;
        private System.Windows.Forms.Label Left_Wheel_Value_label;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label Herkulex_Play_Time_label;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.HScrollBar Herkulex_Play_Time_hScrollBar;
        private System.Windows.Forms.RadioButton B3_radioButton;
        private System.Windows.Forms.RadioButton B2_radioButton;
        private System.Windows.Forms.RadioButton B1_radioButton;
        private System.Windows.Forms.RadioButton B0_radioButton;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.GroupBox Linear_groupBox;
        private System.Windows.Forms.VScrollBar Linear_Motor_Velocity_vScrollBar;
        private System.Windows.Forms.Label Linear_Motor_Velocity_label;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Button Find_Serial_button;
        private System.Windows.Forms.Label Sensor_Board_COM_label;
        private System.Windows.Forms.Label Motor_Board_COM_label;
        private System.Windows.Forms.Button Herkulex5_clear_button;
        private System.Windows.Forms.Button Herkulex4_clear_button;
        private System.Windows.Forms.Button Herkulex3_clear_button;
        private System.Windows.Forms.Button Herkulex2_clear_button;
        private System.Windows.Forms.Button Herkulex1_clear_button;
        private System.Windows.Forms.Label label37;
    }
}

