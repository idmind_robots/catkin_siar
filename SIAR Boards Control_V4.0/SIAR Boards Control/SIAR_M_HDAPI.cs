﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.IO.Ports;
using System.Threading;


namespace HWAPI_M.HwBoard
{
      public class SIAR_M_Board
    {
        private SerialPort serial = null;
        public int Serial_ports_open = 0;

        //Read 
        public int Front_Left_Ticks = 0;
        public int Front_Right_Ticks = 0;
        public int Mid_Left_Ticks = 0;
        public int Mid_Right_Ticks = 0;
        public int Rear_Left_Ticks = 0;
        public int Rear_Right_Ticks = 0;

        public int Linear_Motor_Position = 0;
        public int Linear_Motor_Potentiometer = 0;

        public byte Hardstop_Status = 0;
        public byte Hardstop_Time = 0;

        public byte Fan_Control = 0;

        public char[] FirmwareBoard = new char[25];


        public SIAR_M_Board(string port, int baudRate) 
        {
            this.serial = new SerialPort(port, baudRate, Parity.None, 8, StopBits.One);
            this.serial.ReadTimeout = -1;
            this.serial.WriteTimeout = -1;
            Serial_ports_open = 1;            
            try
            {
                this.serial.Open();
            }
            catch (Exception ex)
            {
                Serial_ports_open = -1;
            }
        }

        public void Disconnect()
        {
            if (Serial_ports_open == 1)
            {
                serial.Close();
                Serial_ports_open = 0;
            }
        }

        /*********************************************/
        //          SIAR Motor SET Functions         */
        /*********************************************/

        /*********************************************/
        /*      SET_Motor_Velocities_Control         */
        /*    [0X56][Front_Left_H][Front_Left_L]     */
        /*          [Front_Right_H][Front_Right_L]   */
        /*          [Mid_Left_H][Mid_Left_L]         */
        /*          [Mid_Right_H][Mid_Right_L]       */
        /*          [Rear_Left_H][Rear_Left_L]       */
        /*          [Rear_Right_H][Rear_Right_L]     */
        /*    receive:                               */
        /*    [0x56][Sent_number][Chsum_H][Chsum_L]  */
        /*********************************************/



        public int SET_Motor_Velocities_Control(int Front_Left, int Front_Right, int Mid_Left, int Mid_Right, int Rear_Left, int Rear_Right) //SIAR SET Motor Velocities Control
        {
            int Check_sum=0;
            int Bytes_sum;

            byte[] writebuffer = new byte[13];
            serial.DiscardInBuffer();

            writebuffer[0] = 0x56;
            writebuffer[1] = (byte)(Front_Left >> 8);
            writebuffer[2] = (byte)(Front_Left & 0x00FF);
            writebuffer[3] = (byte)(Front_Right >> 8);
            writebuffer[4] = (byte)(Front_Right & 0x00FF);

            writebuffer[5] = (byte)(Mid_Left >> 8);
            writebuffer[6] = (byte)(Mid_Left & 0x00FF);
            writebuffer[7] = (byte)(Mid_Right >> 8);
            writebuffer[8] = (byte)(Mid_Right & 0x00FF);

            writebuffer[9] = (byte)(Rear_Left >> 8);
            writebuffer[10] = (byte)(Rear_Left & 0x00FF);
            writebuffer[11] = (byte)(Rear_Right >> 8);
            writebuffer[12] = (byte)(Rear_Right & 0x00FF);

            serial.Write(writebuffer, 0, writebuffer.Length);
            int contador = 0;


            while (serial.BytesToRead < 4 && contador < 32000) contador++;
            while (serial.BytesToRead < 4) contador++;
            if (serial.BytesToRead == 4)
            {
                byte[] buffer = new byte[30];
                serial.Read(buffer, 0, serial.BytesToRead);
                Check_sum=(int) buffer[2]*0x0100+(int) buffer[3]*0x0001;
                Bytes_sum=(int) buffer[0]+(int) buffer[1];
                if (buffer[0]==0x56 && Check_sum==Bytes_sum)
                {
                    return(1);
                }
                else return(-1);
            }
            else return(-1);
        }
        
        /*********************************************/
        /*      SET_Linear_Motor_Position_Control    */
        /*    [0X30][Linear_Position_H]              */
        /*          [Linear_Position_L]              */
        /*    where:                                 */
        /*      [Linear_Position_H:Linear_Position_L]*/
        /*      is reference value between [0,1024] */
        /*    receive:                               */
        /*    [0x30][Sent_number][Chsum_H][Chsum_L]  */
        /*********************************************/

        public int SET_Linear_Motor_Position_Control(int Linear_Motor_Position) //SET Linear Motor Position Control
        {
            int Check_sum = 0;
            int Bytes_sum;

            byte[] writebuffer = new byte[3];
            serial.DiscardInBuffer();

            writebuffer[0] = 0x30;
            writebuffer[1] = (byte)(Linear_Motor_Position >> 8);
            writebuffer[2] = (byte)(Linear_Motor_Position & 0x00FF);


            serial.Write(writebuffer, 0, writebuffer.Length);
            int contador = 0;


            while (serial.BytesToRead < 4 && contador < 12000) contador++;
            if (serial.BytesToRead == 4)
            {
                byte[] buffer = new byte[30];
                serial.Read(buffer, 0, serial.BytesToRead);
                Check_sum = (int)buffer[2] * 0x0100 + (int)buffer[3] * 0x0001;
                Bytes_sum = (int)buffer[0] + (int)buffer[1];
                if (buffer[0] == 0x30 && Check_sum == Bytes_sum)
                {
                    return (1);
                }
                else return (-1);
            }
            else return (-1);
        }


        /*********************************************/
        /*      SET_Linear_Motor_Velocity_Control    */
        /*    [0X31][Linear_M_Vel_H]                 */
        /*          [Linear_M_Vel_L]                 */
        /*    where:                                 */
        /*      [Linear_M_Vel_H:Linear_M_Vel_L]      */
        /*      is reference value between [0,1024]  */
        /*    receive:                               */
        /*    [0x31][Sent_number][Chsum_H][Chsum_L]  */
        /*********************************************/


        public int SET_Linear_Motor_Velocity_Control(int Linear_Motor_Velocity) //SET Linear Motor Velocity Control
        {
            int Check_sum = 0;
            int Bytes_sum = 0;

            byte[] writebuffer = new byte[3];
            serial.DiscardInBuffer();

            writebuffer[0] = 0x31;
            writebuffer[1] = (byte)(Linear_Motor_Velocity >> 8);
            writebuffer[2] = (byte)(Linear_Motor_Velocity & 0x00FF);


            serial.Write(writebuffer, 0, writebuffer.Length);
            int contador = 0;

            while (serial.BytesToRead < 4 && contador < 12000) contador++;
            if (serial.BytesToRead == 4)
            {
                byte[] buffer = new byte[30];
                serial.Read(buffer, 0, serial.BytesToRead);
                Check_sum = (int)buffer[2] * 0x0100 + (int)buffer[3] * 0x0001;
                Bytes_sum = (int)buffer[0] + (int)buffer[1];
                if (buffer[0] == 0x31 && Check_sum == Bytes_sum)
                {
                    return (1);
                }
                else return (-1);
            }
            else return (-1);
        }

        /*********************************************/
        /*      SET_Hardstop                         */
        /*    [0X57]                                 */
        /*    Send a Quick Stop command              */
        /*    receive:                               */
        /*    [0x57][Sent_number][Chsum_H][Chsum_L]  */
        /*********************************************/

        public int SET_Hardstop() //SET Hardstop Time Control
        {
            int Check_sum = 0;
            int Bytes_sum = 0;

            byte[] writebuffer = new byte[1];
            serial.DiscardInBuffer();

            writebuffer[0] = 0x57;

            serial.Write(writebuffer, 0, writebuffer.Length);
            int contador = 0;

            while (serial.BytesToRead < 4 && contador < 12000) contador++;
            if (serial.BytesToRead == 4)
            {
                byte[] buffer = new byte[30];
                serial.Read(buffer, 0, serial.BytesToRead);
                Check_sum = (int)buffer[2] * 0x0100 + (int)buffer[3] * 0x0001;
                Bytes_sum = (int)buffer[0] + (int)buffer[1];
                if (buffer[0] == 0x57 && Check_sum == Bytes_sum)
                {
                    return (1);
                }
                else return (-1);
            }
            else return (-1);
        }


        /*********************************************/
        /*      SET_Hardstop_Time_Control            */
        /*    [0X58][Hardstop_Time]                  */
        /*    Hardstop_Time between [0,255]s         */
        /*    receive:                               */
        /*    [0x58][Sent_number][Chsum_H][Chsum_L]  */
        /*********************************************/

        public int SET_Hardstop_Time_Control(byte Hardstop_Time) //SET Hardstop Time Control
        {
            int Check_sum = 0;
            int Bytes_sum = 0;

            byte[] writebuffer = new byte[2];
            serial.DiscardInBuffer();

            writebuffer[0] = 0x58;
            writebuffer[1] = Hardstop_Time;

            serial.Write(writebuffer, 0, writebuffer.Length);
            int contador = 0;

            while (serial.BytesToRead < 4 && contador < 12000) contador++;
            if (serial.BytesToRead == 4)
            {
                byte[] buffer = new byte[30];
                serial.Read(buffer, 0, serial.BytesToRead);
                Check_sum = (int)buffer[2] * 0x0100 + (int)buffer[3] * 0x0001;
                Bytes_sum = (int)buffer[0] + (int)buffer[1];
                if (buffer[0] == 0x58 && Check_sum == Bytes_sum)
                {
                    return (1);
                }
                else return (-1);
            }
            else return (-1);
        }

        /*********************************************/
        /*      SET_FAN_Control                      */
        /*    [0X53][Fan_Control]                    */
        /*    Fan_Control = 1 FAN Motor ON           */
        /*    Fan_Control = 0 FAN Motor OFF          */
        /*    receive:                               */
        /*    [0x53][Sent_number][Chsum_H][Chsum_L]  */
        /*********************************************/

        public int SET_FAN_Control(byte Fan_Control) //SET Fan Control
        {
            int Check_sum = 0;
            int Bytes_sum = 0;

            byte[] writebuffer = new byte[2];
            serial.DiscardInBuffer();

            writebuffer[0] = 0x53;
            writebuffer[1] = Fan_Control;

            serial.Write(writebuffer, 0, writebuffer.Length);
            int contador = 0;

            while (serial.BytesToRead < 4 && contador < 12000) contador++;
            if (serial.BytesToRead == 4)
            {
                byte[] buffer = new byte[30];
                serial.Read(buffer, 0, serial.BytesToRead);
                Check_sum = (int)buffer[2] * 0x0100 + (int)buffer[3] * 0x0001;
                Bytes_sum = (int)buffer[0] + (int)buffer[1];
                if (buffer[0] == 0x53 && Check_sum == Bytes_sum)
                {
                    return (1);
                }
                else return (-1);
            }
            else return (-1);
        }

        /*********************************************/
        //             SIAR Motor GET Function       */
        /*********************************************/
        /*********************************************/
        /*      Get_Velocity_Motor_Ticks             */
        /*    [0X4A]                                 */
        /*    receive:                               */
        /*    [0x4A][Frt_Lf_Tick_H][Frt_Lf_Tick_L]   */
        /*          [Frt_Rt_Tick_H][Frt_Rt_Tick_L]   */
        /*          [Mid_Lf_Tick_H][Mid_Lf_Tick_L]   */
        /*          [Mid_Rt_Tick_H][Mid_Rt_Tick_L]   */
        /*          [Rear_Lf_Tick_H][Rear_Lf_Tick_L] */
        /*          [Rear_Rt_Tick_H][Rear_Rt_Tick_L] */
        /*          [Sent_number][Chsum_H][Chsum_L]  */
        /*********************************************/

        public int Get_Velocity_Motor_Ticks()
        {
            int Check_sum = 0;
            int Bytes_sum = 0;
            serial.DiscardInBuffer();

            byte[] writebuffer = new byte[1];


            writebuffer[0] = 0x4A;

            serial.Write(writebuffer, 0, writebuffer.Length);
            int contador = 0;
            while (serial.BytesToRead < 16 && contador < 30000) contador++;
            if ( serial.BytesToRead == 16)
            {


                byte[] buffer = new byte[30];
                serial.Read(buffer, 0, serial.BytesToRead);
                Check_sum=0;
                Bytes_sum=0;

                for (int i=0; i<14; i++) Bytes_sum+=(int)buffer[i];
                Check_sum = (int)buffer[14]*0x0100 + (int)buffer[15]*0x0001;

                if (buffer[0] == 0x4A && Check_sum == Bytes_sum)
                {
                    Front_Left_Ticks = (int)buffer[1] * 0x0100 + (int)buffer[2] * 0x0001;
                    if (Front_Left_Ticks > 32768) Front_Left_Ticks -= 0XFFFF;
                    Front_Right_Ticks = (int)buffer[3] * 0x0100 + (int)buffer[4] * 0x0001;
                    if (Front_Right_Ticks > 32768) Front_Right_Ticks -= 0XFFFF;

                    Mid_Left_Ticks = (int)buffer[5] * 0x0100 + (int)buffer[6] * 0x0001;
                    if (Mid_Left_Ticks > 32768) Mid_Left_Ticks -= 0XFFFF;
                    Mid_Right_Ticks = (int)buffer[7] * 0x0100 + (int)buffer[8] * 0x0001;
                    if (Mid_Right_Ticks > 32768) Mid_Right_Ticks -= 0XFFFF;

                    Rear_Left_Ticks = (int)buffer[9] * 0x0100 + (int)buffer[10] * 0x0001;
                    if (Rear_Left_Ticks > 32768) Rear_Left_Ticks -= 0XFFFF;
                    Rear_Right_Ticks = (int)buffer[11] * 0x0100 + (int)buffer[12] * 0x0001;
                    if (Rear_Right_Ticks > 32768) Rear_Right_Ticks -= 0XFFFF;

                    return (1);
                }
                else return (-1);
                
            }
            return (-1);
        }

        /*********************************************/
        /*      Get_Velocity_Motor_Ticks             */
        /*    [0X32]                                 */
        /*    receive:                               */
        /*    [0x32][Frt_Lf_Tick_H][Frt_Lf_Tick_L]   */
        /*          [Frt_Rt_Tick_H][Frt_Rt_Tick_L]   */
        /*          [Mid_Lf_Tick_H][Mid_Lf_Tick_L]   */
        /*          [Mid_Rt_Tick_H][Mid_Rt_Tick_L]   */
        /*          [Rear_Lf_Tick_H][Rear_Lf_Tick_L] */
        /*          [Rear_Rt_Tick_H][Rear_Rt_Tick_L] */
        /*          [Sent_number][Chsum_H][Chsum_L]  */
        /*    where:                                 */
        /*    [X_Tick_H:X_Tick_L] are the counted    */
        /*    ticks between two readings             */ 
        /*********************************************/

        public int Get_Linear_Motor_Position()
        {
            int Check_sum = 0;
            int Bytes_sum = 0;
            serial.DiscardInBuffer();

            byte[] writebuffer = new byte[1];

            writebuffer[0] = 0x32;

            serial.Write(writebuffer, 0, writebuffer.Length);
            int contador = 0;
            while (serial.BytesToRead < 6 && contador < 32000) contador++;
            if (serial.BytesToRead == 6)
            {
                byte[] buffer = new byte[30];
                serial.Read(buffer, 0, serial.BytesToRead);
                Check_sum = 0;
                Bytes_sum = 0;

                for (int i = 0; i < 4; i++) Bytes_sum += (int)buffer[i];
                Check_sum = (int)buffer[4] * 0x0100 + (int)buffer[5] * 0x0001;

                if (buffer[0] == 0x32 && Check_sum == Bytes_sum)
                {
                    Linear_Motor_Position = (int)buffer[1] * 0x0100 + (int)buffer[2] * 0x0001;
                    if (Linear_Motor_Position > 32768) Linear_Motor_Position -= 0XFFFF;
                    return (1);
                }
                else return (-1);
            }
            return (-1);
        }

        /*********************************************/
        /*      Get_Linear_Motor_Potentiometer       */
        /*    [0X33]                                 */
        /*    receive:                               */
        /*    [0x33][Linear_Motor_Pot_H]             */
        /*          [Linear_Motor_Pot_L]             */
        /*          [Sent_number][Chsum_H][Chsum_L]  */
        /*    where:                                 */
        /*    [Linear_Motor_Pot_H:Linear_Motor_Pot_L]*/
        /*    is the Potentiometer value of the      */
        /*    linear motor position                  */
        /*********************************************/

        public int Get_Linear_Motor_Potentiometer()
        {
            int Check_sum = 0;
            int Bytes_sum = 0;
            serial.DiscardInBuffer();

            byte[] writebuffer = new byte[1];

            writebuffer[0] = 0x33;

            serial.Write(writebuffer, 0, writebuffer.Length);
            int contador = 0;
            while (serial.BytesToRead < 6 && contador < 32000) contador++;
            if (serial.BytesToRead == 6)
            {
                byte[] buffer = new byte[30];
                serial.Read(buffer, 0, serial.BytesToRead);
                Check_sum = 0;
                Bytes_sum = 0;

                for (int i = 0; i < 4; i++) Bytes_sum += (int)buffer[i];
                Check_sum = (int)buffer[4] * 0x0100 + (int)buffer[5] * 0x0001;

                if (buffer[0] == 0x33 && Check_sum == Bytes_sum)
                {
                    Linear_Motor_Potentiometer = (int)buffer[1] * 0x0100 + (int)buffer[2] * 0x0001;
                    if (Linear_Motor_Potentiometer > 32768) Linear_Motor_Potentiometer -= 0XFFFF;
                    return (1);
                }
                else return (-1);
            }
            return (-1);
        }

        /*********************************************/
        /*      Get_Hardstop_Status                  */
        /*    [0X59]                                 */
        /*    receive:                               */
        /*    [0x59][Hardstop_Status]                */
        /*    where:                                 */
        /*    [Hardstop_Status] = 0 Not activated    */
        /*    [Hardstop_Status] = 1 Activated        */
        /*********************************************/

        public int Get_Hardstop_Status()
        {
            int Check_sum = 0;
            int Bytes_sum = 0;
            serial.DiscardInBuffer();

            byte[] writebuffer = new byte[1];

            writebuffer[0] = 0x59;

            serial.Write(writebuffer, 0, writebuffer.Length);
            int contador = 0;
            while (serial.BytesToRead < 5 && contador < 12000) contador++;
            if (serial.BytesToRead == 5)
            {
                byte[] buffer = new byte[30];
                serial.Read(buffer, 0, serial.BytesToRead);
                Check_sum = 0;
                Bytes_sum = 0;

                for (int i = 0; i < 3; i++) Bytes_sum += (int)buffer[i];
                Check_sum = (int)buffer[3] * 0x0100 + (int)buffer[4] * 0x0001;

                if (buffer[0] == 0x59 && Check_sum == Bytes_sum)
                {
                    Hardstop_Status = (byte)buffer[1];
                    return (1);
                }
                else return (-1);
            }
            return (-1);
        }

        /*********************************************/
        /*      Get_Hardstop_Time                    */
        /*    [0X58]                                 */
        /*    receive:                               */
        /*    [0x58][Hardstop_Time]                  */
        /*    where:                                 */
        /*    [Hardstop_Time] between [0,255]s       */
        /*********************************************/

        public int Get_Hardstop_Time()
        {
            int Check_sum = 0;
            int Bytes_sum = 0;
            serial.DiscardInBuffer();

            byte[] writebuffer = new byte[1];

            writebuffer[0] = 0x58;

            serial.Write(writebuffer, 0, writebuffer.Length);
            int contador = 0;
            while (serial.BytesToRead < 5 && contador < 12000) contador++;
            if (serial.BytesToRead == 5)
            {
                byte[] buffer = new byte[30];
                serial.Read(buffer, 0, serial.BytesToRead);
                Check_sum = 0;
                Bytes_sum = 0;

                for (int i = 0; i < 3; i++) Bytes_sum += (int)buffer[i];
                Check_sum = (int)buffer[3] * 0x0100 + (int)buffer[4] * 0x0001;

                if (buffer[0] == 0x58 && Check_sum == Bytes_sum)
                {
                    Hardstop_Time = (byte)buffer[1];
                    return (1);
                }
                else return (-1);
            }
            return (-1);
        }

        /*********************************************/
        /*      Get_Fan_Control                      */
        /*    [0X54]                                 */
        /*    receive:                               */
        /*    [0x54][Fan_Control]                    */
        /*    where:                                 */
        /*    [Fan_Control] =1 Fan Activated         */
        /*    [Fan_Control] =0 Fan Inactive          */
        /*********************************************/

        public int Get_Fan_Control()
        {
            int Check_sum = 0;
            int Bytes_sum = 0;
            serial.DiscardInBuffer();

            byte[] writebuffer = new byte[1];

            writebuffer[0] = 0x54;

            serial.Write(writebuffer, 0, writebuffer.Length);
            int contador = 0;
            while (serial.BytesToRead < 5 && contador < 12000) contador++;
            if (serial.BytesToRead == 5)
            {
                byte[] buffer = new byte[30];
                serial.Read(buffer, 0, serial.BytesToRead);
                Check_sum = 0;
                Bytes_sum = 0;

                for (int i = 0; i < 3; i++) Bytes_sum += (int)buffer[i];
                Check_sum = (int)buffer[3] * 0x0100 + (int)buffer[4] * 0x0001;

                if (buffer[0] == 0x54 && Check_sum == Bytes_sum)
                {
                    Fan_Control = (byte)buffer[1];
                    return (1);
                }
                else return (-1);
            }
            return (-1);
        }

        // 0x20  GET  Firmware version number
        /*********************************************/
        /*        Get_firmware_version_number        */
        /*    [0X20]                                 */
        /*    receive:                               */
        /*    [0x20][FirmWare-> 25 Bytes]            */
        /*    [Sent_number][Chsum_H][Chsum_L]        */
        /*    where FirmWare:                        */
        /*     "Board2 fw 1.00 2017/07/12"           */
        /*********************************************/

        public int Get_firmware_version_number()  //Firmware Version
        {
            int i = 0;
            serial.DiscardInBuffer();

            serial.Write(new byte[] { 0x20 }, 0, 1);
            int Comunication_counter = 0;
            int Communication_error = 0;
            while (serial.BytesToRead < 29 && Comunication_counter < 20000) Comunication_counter++;
            if (serial.BytesToRead == 29)
            {
                byte[] buffer = new byte[serial.BytesToRead];
                serial.Read(buffer, 0, (int)buffer.Length);
                if (buffer[0] == 0x20)
                {

                    Communication_error = 0;
                    for (i = 0; i < 25; i++)
                    {
                        FirmwareBoard[i] = Convert.ToChar(buffer[i + 1]);
                    }

                }
                else Communication_error = -1;
            }
            else
                Communication_error = -1;
            return (Communication_error);
        }



    }
}
