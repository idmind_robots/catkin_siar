﻿using System;
using System.Windows.Forms;

namespace SIAR_Boards_Control
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }


        HWAPI_M.HwBoard.SIAR_M_Board SIAR_Motor_Board;
        HWAPI_S.HwBoard.SIAR_S_Board SIAR_Sensor_Board;

        byte Aux_Dir_Control = 0xFF;
        byte Aux_Set_Control = 0xFF;
        byte Lights_Control = 0xFF;

        byte Herkulex1_Torque_Control = 0xFF;
        byte Herkulex2_Torque_Control = 0xFF;
        byte Herkulex3_Torque_Control = 0xFF;
        byte Herkulex4_Torque_Control = 0xFF;
        byte Herkulex5_Torque_Control = 0xFF;

        byte Herkulex1_Position_Control = 0xFF;
        byte Herkulex2_Position_Control = 0xFF;
        byte Herkulex3_Position_Control = 0xFF;
        byte Herkulex4_Position_Control = 0xFF;
        byte Herkulex5_Position_Control = 0xFF;


        byte Herkulex1_Clear_Control = 0xFF;
        byte Herkulex2_Clear_Control = 0xFF;
        byte Herkulex3_Clear_Control = 0xFF;
        byte Herkulex4_Clear_Control = 0xFF;
        byte Herkulex5_Clear_Control = 0xFF;

        byte Ind_Velocity_Control = 0xFF;
        byte Robot_Velocity_Control = 0xFF;

        byte Linear_Motor_Position_Control = 0xFF;
        byte Linear_Motor_Velocity_Control = 0xFF;

        byte HardStop_Control = 0xFF;
        byte Hardstop_Time_Control = 0xFF;        

        byte Auxiliary_Pins_Direction = 0x3F;
        byte Auxiliary_Pins_Value = 0x00;
        byte Lights = 0x00;

        byte Herkulex1_Torque = 0x00;
        byte Herkulex2_Torque = 0x00;
        byte Herkulex3_Torque = 0x00;
        byte Herkulex4_Torque = 0x00;
        byte Herkulex5_Torque = 0x00;

        int Herkulex1_Position = 512;
        int Herkulex2_Position = 512;
        int Herkulex3_Position = 512;
        int Herkulex4_Position = 512;
        int Herkulex5_Position = 512;

        byte Herkulex_Playtime = 100;

        int Angular_Velocity = 0;
        int Linear_Velocity = 0;

        int Left_Wheel_Velocity = 0;
        int Right_Wheel_Velocity = 0;

        int Linear_Motor_Position = 0;
        int Linear_Motor_Velocity = 0;

        int Front_Left_Wheel_Velocity;
        int Mid_Left_Wheel_Velocity;
        int Rear_Left_Wheel_Velocity;

        int Front_Right_Wheel_Velocity;
        int Mid_Right_Wheel_Velocity;
        int Rear_Right_Wheel_Velocity;


        int Hardstop_Timer = 0;

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (Sensor_radioButton.Checked == true)
            {
                SIAR_Sensor_Board.Get_System_Power_Supply();
                Elect_V_label.Text = String.Format("{0} V",SIAR_Sensor_Board.Elect_V/10.0);
                Elect_I_label.Text = String.Format("{0} mA", SIAR_Sensor_Board.Elect_I);
                Motor_V_label.Text = String.Format("{0} V", SIAR_Sensor_Board.Motor_V/10.0);
                Motor_I_label.Text = String.Format("{0} mA", SIAR_Sensor_Board.Motor_I);
                Charger_V_label.Text = String.Format("{0} V", SIAR_Sensor_Board.Charger_V/10.0);
                SIAR_Sensor_Board.Get_Batteries_Voltage();
                Electronic_Battery_Voltage_label.Text = String.Format("{0} V",SIAR_Sensor_Board.Elect_Bat_Voltage);//10.0);
                Motor_Battery_Voltage_label.Text = String.Format("{0} V", SIAR_Sensor_Board.Motor_Bat_Voltage/10.0);
                SIAR_Sensor_Board.Get_Instantaneous_Current();
                Instant_Elect_Current_label.Text = String.Format("{0} mA",SIAR_Sensor_Board.Instantaneous_Elect_Current);
                Instant_Motor_Current_label.Text = String.Format("{0} mA", SIAR_Sensor_Board.Instantaneous_Motor_Current);
                SIAR_Sensor_Board.Get_Integrated_Current();
                Integrated_Elect_Current_label.Text = String.Format("{0} mA", SIAR_Sensor_Board.Integrated_Elect_Current);
                Integrated_Motor_Current_label.Text = String.Format("{0} mA", SIAR_Sensor_Board.Integrated_Motor_Current);
                SIAR_Sensor_Board.Get_Level();
                Elect_Level_label.Text = String.Format("{0} %",SIAR_Sensor_Board.Elect_Level);
                Motor_Level_label.Text = String.Format("{0} %", SIAR_Sensor_Board.Motor_Level);
                Elect_Bat_Min_label.Text = String.Format("{0} min",SIAR_Sensor_Board.Elect_Bat_Min);
                Motor_Bat_Min_label.Text = String.Format("{0} min", SIAR_Sensor_Board.Motor_Bat_Min);

                SIAR_Sensor_Board.Get_Auxiliary_Pins_Values();
                if ((SIAR_Sensor_Board.Auxiliary_Pins_Values & 0x01) != 0) F0_Get_checkBox.Checked = true; else F0_Get_checkBox.Checked = false;
                if ((SIAR_Sensor_Board.Auxiliary_Pins_Values & 0x02) != 0) F1_Get_checkBox.Checked = true; else F1_Get_checkBox.Checked = false;
                if ((SIAR_Sensor_Board.Auxiliary_Pins_Values & 0x04) != 0) F2_Get_checkBox.Checked = true; else F2_Get_checkBox.Checked = false;
                if ((SIAR_Sensor_Board.Auxiliary_Pins_Values & 0x08) != 0) F3_Get_checkBox.Checked = true; else F3_Get_checkBox.Checked = false;
                if ((SIAR_Sensor_Board.Auxiliary_Pins_Values & 0x10) != 0) F4_Get_checkBox.Checked = true; else F4_Get_checkBox.Checked = false;
                if ((SIAR_Sensor_Board.Auxiliary_Pins_Values & 0x20) != 0) F5_Get_checkBox.Checked = true; else F5_Get_checkBox.Checked = false;

                if (Aux_Dir_Control != 0xFF) { if (SIAR_Sensor_Board.SET_Aux_Pins_Direction(Auxiliary_Pins_Direction)== 1) Aux_Dir_Control = 0x00; }
                if (Aux_Set_Control != 0xFF) { if (SIAR_Sensor_Board.SET_Aux_Pins_Values(Auxiliary_Pins_Value) == 1) Aux_Set_Control = 0x00; }
                if (Lights_Control != 0xFF) {if (SIAR_Sensor_Board.SET_Lights(Lights) == 1) Lights_Control=0xFF;}

                if (Herkulex_radioButton.Checked == true)
                {
                    SIAR_Sensor_Board.Get_Herkulex_Status_Error();
                    Herkulex1_Status_label.Text = String.Format("{0}", SIAR_Sensor_Board.Herkulex1_Status);
                    Herkulex2_Status_label.Text = String.Format("{0}", SIAR_Sensor_Board.Herkulex2_Status);
                    Herkulex3_Status_label.Text = String.Format("{0}", SIAR_Sensor_Board.Herkulex3_Status);
                    Herkulex4_Status_label.Text = String.Format("{0}", SIAR_Sensor_Board.Herkulex4_Status);
                    Herkulex5_Status_label.Text = String.Format("{0}", SIAR_Sensor_Board.Herkulex5_Status);
                    System.Threading.Thread.Sleep(20);
                    SIAR_Sensor_Board.Get_Herkulex_Torque();
                    Herkulex1_Torque_label.Text = String.Format("{0}", SIAR_Sensor_Board.Herkulex1_Torque);
                    Herkulex2_Torque_label.Text = String.Format("{0}", SIAR_Sensor_Board.Herkulex2_Torque);
                    Herkulex3_Torque_label.Text = String.Format("{0}", SIAR_Sensor_Board.Herkulex3_Torque);
                    Herkulex4_Torque_label.Text = String.Format("{0}", SIAR_Sensor_Board.Herkulex4_Torque);
                    Herkulex5_Torque_label.Text = String.Format("{0}", SIAR_Sensor_Board.Herkulex5_Torque);
                    System.Threading.Thread.Sleep(20);
                    SIAR_Sensor_Board.Get_Herkulex_Position();
                    Herkulex1_Position_label.Text = String.Format("{0}", SIAR_Sensor_Board.Herkulex1_Position);
                    Herkulex2_Position_label.Text = String.Format("{0}", SIAR_Sensor_Board.Herkulex2_Position);
                    Herkulex3_Position_label.Text = String.Format("{0}", SIAR_Sensor_Board.Herkulex3_Position);
                    Herkulex4_Position_label.Text = String.Format("{0}", SIAR_Sensor_Board.Herkulex4_Position);
                    Herkulex5_Position_label.Text = String.Format("{0}", SIAR_Sensor_Board.Herkulex5_Position);
                    System.Threading.Thread.Sleep(20);
                    SIAR_Sensor_Board.Get_Herkulex_Temperature();
                    Herkulex1_Temperature_label.Text = String.Format("{0}", SIAR_Sensor_Board.Herkulex1_Temperature);
                    Herkulex2_Temperature_label.Text = String.Format("{0}", SIAR_Sensor_Board.Herkulex2_Temperature);
                    Herkulex3_Temperature_label.Text = String.Format("{0}", SIAR_Sensor_Board.Herkulex3_Temperature);
                    Herkulex4_Temperature_label.Text = String.Format("{0}", SIAR_Sensor_Board.Herkulex4_Temperature);
                    Herkulex5_Temperature_label.Text = String.Format("{0}", SIAR_Sensor_Board.Herkulex5_Temperature);
                   
                    
                    if (Herkulex1_Torque_Control != 0xFF) { if (SIAR_Sensor_Board.SET_Herkulex_Torque(0x00, Herkulex1_Torque) == 1) Herkulex1_Torque_Control = 0xFF; }                    
                    if (Herkulex2_Torque_Control != 0xFF) { if (SIAR_Sensor_Board.SET_Herkulex_Torque(0x01, Herkulex2_Torque) == 1) Herkulex2_Torque_Control = 0xFF; }
                    if (Herkulex3_Torque_Control != 0xFF) { if (SIAR_Sensor_Board.SET_Herkulex_Torque(0x02, Herkulex3_Torque) == 1) Herkulex3_Torque_Control = 0xFF; }
                    if (Herkulex4_Torque_Control != 0xFF) { if (SIAR_Sensor_Board.SET_Herkulex_Torque(0x03, Herkulex4_Torque) == 1) Herkulex4_Torque_Control = 0xFF; }
                    if (Herkulex5_Torque_Control != 0xFF) { if (SIAR_Sensor_Board.SET_Herkulex_Torque(0x04, Herkulex5_Torque) == 1) Herkulex5_Torque_Control = 0xFF; }

                    if (Herkulex1_Position_Control != 0xFF) { if (SIAR_Sensor_Board.SET_Herkulex_Position(0x00, Herkulex1_Position,Herkulex_Playtime) == 1) Herkulex1_Position_Control = 0xFF;}                    
                    if (Herkulex2_Position_Control != 0xFF) { if (SIAR_Sensor_Board.SET_Herkulex_Position(0x01, Herkulex2_Position, Herkulex_Playtime) == 1) Herkulex2_Position_Control = 0xFF; }
                    if (Herkulex3_Position_Control != 0xFF) { if (SIAR_Sensor_Board.SET_Herkulex_Position(0x02, Herkulex3_Position, Herkulex_Playtime) == 1) Herkulex3_Position_Control = 0xFF; }
                    if (Herkulex4_Position_Control != 0xFF) { if (SIAR_Sensor_Board.SET_Herkulex_Position(0x03, Herkulex4_Position, Herkulex_Playtime) == 1) Herkulex4_Position_Control = 0xFF; }
                    if (Herkulex5_Position_Control != 0xFF) { if (SIAR_Sensor_Board.SET_Herkulex_Position(0x04, Herkulex5_Position, Herkulex_Playtime) == 1) Herkulex5_Position_Control = 0xFF; }


                    if (Herkulex1_Clear_Control != 0xFF) { if (SIAR_Sensor_Board.SET_Herkulex_Clear_Status_error(0x00) == 1)  Herkulex1_Clear_Control = 0xFF;}
                    if (Herkulex2_Clear_Control != 0xFF) { if (SIAR_Sensor_Board.SET_Herkulex_Clear_Status_error(0x01) == 1)  Herkulex2_Clear_Control = 0xFF; }
                    if (Herkulex3_Clear_Control != 0xFF) { if (SIAR_Sensor_Board.SET_Herkulex_Clear_Status_error(0x02) == 1)  Herkulex3_Clear_Control = 0xFF; }
                    if (Herkulex4_Clear_Control != 0xFF) { if (SIAR_Sensor_Board.SET_Herkulex_Clear_Status_error(0x03) == 1)  Herkulex4_Clear_Control = 0xFF; }
                    if (Herkulex5_Clear_Control != 0xFF) { if (SIAR_Sensor_Board.SET_Herkulex_Clear_Status_error(0x04) == 1)  Herkulex5_Clear_Control = 0xFF; }




/*
                    Herkulex1_Torque_Control = 0xFF;
                    Herkulex2_Torque_Control = 0xFF;
                    Herkulex3_Torque_Control = 0xFF;
                    Herkulex4_Torque_Control = 0xFF;
                    Herkulex5_Torque_Control = 0xFF;

                    Herkulex1_Position_Control = 0xFF;
                    Herkulex2_Position_Control = 0xFF;
                    Herkulex3_Position_Control = 0xFF;
                    Herkulex4_Position_Control = 0xFF;
                    Herkulex5_Position_Control = 0xFF;
  */    
                }
            }
            if (Motor_radioButton.Checked == true)
            {

              
                
            


                if (Ind_Motor_Stream_radioButton.Checked == true)
                {
                    if (Ind_Velocity_Control != 0xFF) { if (SIAR_Motor_Board.SET_Motor_Velocities_Control(Front_Left_Wheel_Velocity, Front_Right_Wheel_Velocity, Mid_Left_Wheel_Velocity, Mid_Right_Wheel_Velocity, Rear_Left_Wheel_Velocity, Rear_Right_Wheel_Velocity) == 1) Ind_Velocity_Control = 0xFF; }
                }
                else if (Control_Stream_radioButton.Checked == true)                    
                {

                    //if (Robot_Velocity_Control != 0xFF) { if (SIAR_Motor_Board.SET_Motor_Velocities_Control(Front_Left_Wheel_Velocity, Front_Right_Wheel_Velocity, Mid_Left_Wheel_Velocity, Mid_Right_Wheel_Velocity, Rear_Left_Wheel_Velocity, Rear_Right_Wheel_Velocity) == 1) Robot_Velocity_Control = 0xFF; }
                    SIAR_Motor_Board.SET_Motor_Velocities_Control(Front_Left_Wheel_Velocity, Front_Right_Wheel_Velocity, Mid_Left_Wheel_Velocity, Mid_Right_Wheel_Velocity, Rear_Left_Wheel_Velocity, Rear_Right_Wheel_Velocity);
                }

                if (Linear_Motor_Position_Control != 0xFF) {
                    if (SIAR_Motor_Board.SET_Linear_Motor_Position_Control(Linear_Motor_Position) == 1) 
                            Linear_Motor_Position_Control = 0xFF;
                }
                if (Linear_Motor_Velocity_Control != 0xFF) { if (SIAR_Motor_Board.SET_Linear_Motor_Velocity_Control(Linear_Motor_Velocity) == 1) Linear_Motor_Velocity_Control = 0xFF; }

                if (HardStop_Control != 0xFF){if (Hardstop_radioButton.Checked == true) SIAR_Motor_Board.SET_Hardstop();else HardStop_Control = 0xFF;}
                if (Hardstop_Time_Control != 0xFF) {if (SIAR_Motor_Board.SET_Hardstop_Time_Control((byte)Hardstop_Timer) == 1) Hardstop_Time_Control = 0xFF; }


                 SIAR_Motor_Board.Get_Linear_Motor_Position();
                 Linear_Position_Ticks_label.Text = String.Format("{0}", SIAR_Motor_Board.Linear_Motor_Position);
                 SIAR_Motor_Board.Get_Linear_Motor_Potentiometer();
                 Potentiometer_label.Text = String.Format("{0}", SIAR_Motor_Board.Linear_Motor_Potentiometer);
                

                SIAR_Motor_Board.Get_Velocity_Motor_Ticks();
                Front_Left_Ticks_label.Text = String.Format("{0}", SIAR_Motor_Board.Front_Left_Ticks);
                Front_Right_Ticks_label.Text = String.Format("{0}", SIAR_Motor_Board.Front_Right_Ticks);
                Mid_Left_Ticks_label.Text = String.Format("{0}", SIAR_Motor_Board.Mid_Left_Ticks);
                Mid_Right_Ticks_label.Text = String.Format("{0}", SIAR_Motor_Board.Mid_Right_Ticks);
                Rear_Left_Ticks_label.Text = String.Format("{0}", SIAR_Motor_Board.Rear_Left_Ticks);
                Rear_Right_Ticks_label.Text = String.Format("{0}", SIAR_Motor_Board.Rear_Right_Ticks);
                
            }
        }

        private void F0_Dir_radioButton_Click(object sender, EventArgs e)
        {
            if (F0_Dir_radioButton.Checked == false)
            {
                Auxiliary_Pins_Direction |= 0x01; F0_Set_radioButton.Enabled = false; F0_Dir_radioButton.Checked = true;
            }
            else
            {
                Auxiliary_Pins_Direction &= 0x3E; F0_Set_radioButton.Enabled = true; F0_Dir_radioButton.Checked = false;
            }
            Aux_Dir_Control = 1;
        }

        private void F1_Dir_radioButton_Click(object sender, EventArgs e)
        {
            if (F1_Dir_radioButton.Checked == false) { Auxiliary_Pins_Direction |= 0x02; F1_Set_radioButton.Enabled = false; F1_Dir_radioButton.Checked = true; }
            else { Auxiliary_Pins_Direction &= 0x3D; F1_Set_radioButton.Enabled = true; F1_Dir_radioButton.Checked = false; }
            Aux_Dir_Control = 1;

        }

        private void F2_Dir_radioButton_Click(object sender, EventArgs e)
        {
            if (F2_Dir_radioButton.Checked == false) { Auxiliary_Pins_Direction |= 0x04; F2_Set_radioButton.Enabled = false; F2_Dir_radioButton.Checked = true; }
            else { Auxiliary_Pins_Direction &= 0x3B; F2_Set_radioButton.Enabled = true; F2_Dir_radioButton.Checked = false; }
            Aux_Dir_Control = 1;

        }

        private void F3_Dir_radioButton_Click(object sender, EventArgs e)
        {
            if (F3_Dir_radioButton.Checked == false) { Auxiliary_Pins_Direction |= 0x08; F3_Set_radioButton.Enabled = false; F3_Dir_radioButton.Checked = true; }
            else { Auxiliary_Pins_Direction &= 0x37; F3_Set_radioButton.Enabled = true; F3_Dir_radioButton.Checked = false; }
            Aux_Dir_Control = 1;

        }

        private void F4_Dir_radioButton_Click(object sender, EventArgs e)
        {

            if (F4_Dir_radioButton.Checked == false) { Auxiliary_Pins_Direction |= 0x10; F4_Set_radioButton.Enabled = false; F4_Dir_radioButton.Checked = true; }
            else { Auxiliary_Pins_Direction &= 0x2F; F4_Set_radioButton.Enabled = true; F4_Dir_radioButton.Checked = false; }
            Aux_Dir_Control = 1;
        }

        private void F5_Dir_radioButton_Click(object sender, EventArgs e)
        {
            if (F5_Dir_radioButton.Checked == false) { Auxiliary_Pins_Direction |= 0x20; F5_Set_radioButton.Enabled = false; F5_Dir_radioButton.Checked = true; }
            else { Auxiliary_Pins_Direction &= 0x1F; F5_Set_radioButton.Enabled = true; F5_Dir_radioButton.Checked = false; }
            Aux_Dir_Control = 1;

        }

        private void F0_Set_radioButton_Click(object sender, EventArgs e)
        {
            if (F0_Set_radioButton.Checked == false) { Auxiliary_Pins_Value |= 0x01; F0_Set_radioButton.Checked = true; }
            else { Auxiliary_Pins_Value &= 0x3E; F0_Set_radioButton.Checked = false; }
            Aux_Set_Control = 1;
        }

        private void F1_Set_radioButton_Click(object sender, EventArgs e)
        {
            if (F1_Set_radioButton.Checked == false) { Auxiliary_Pins_Value |= 0x02; F1_Set_radioButton.Checked = true; }
            else { Auxiliary_Pins_Value &= 0x3D; F1_Set_radioButton.Checked = false; }
            Aux_Set_Control = 1;
        }

        private void F2_Set_radioButton_Click(object sender, EventArgs e)
        {
            if (F2_Set_radioButton.Checked == false) { Auxiliary_Pins_Value |= 0x04; F2_Set_radioButton.Checked = true; }
            else { Auxiliary_Pins_Value &= 0x3B; F2_Set_radioButton.Checked = false; }
            Aux_Set_Control = 1;

        }

        private void F3_Set_radioButton_Click(object sender, EventArgs e)
        {
            if (F3_Set_radioButton.Checked == false) { Auxiliary_Pins_Value |= 0x08; F3_Set_radioButton.Checked = true; }
            else { Auxiliary_Pins_Value &= 0x37; F3_Set_radioButton.Checked = false; }
            Aux_Set_Control = 1;

        }

        private void F4_Set_radioButton_Click(object sender, EventArgs e)
        {
            if (F4_Set_radioButton.Checked == false) { Auxiliary_Pins_Value |= 0x10; F4_Set_radioButton.Checked = true; }
            else { Auxiliary_Pins_Value &= 0x2F; F4_Set_radioButton.Checked = false; }
            Aux_Set_Control = 1;

        }

        private void F5_Set_radioButton_Click(object sender, EventArgs e)
        {
            if (F5_Set_radioButton.Checked == false) { Auxiliary_Pins_Value |= 0x20; F5_Set_radioButton.Checked = true; }
            else { Auxiliary_Pins_Value &= 0x1F; F5_Set_radioButton.Checked = false; }
            Aux_Set_Control = 1;

        }

        private void Herkulex1_On_radioButton_Click(object sender, EventArgs e)
        {
            Herkulex1_Torque = 0x60;
            Herkulex1_On_radioButton.Checked = true;
            Herkulex1_Off_radioButton.Checked = false;
            Herkulex1_Brake_radioButton.Checked = false;
            Herkulex1_Torque_Control = 0x01;
        }

        private void Herkulex1_Off_radioButton_Click(object sender, EventArgs e)
        {
            Herkulex1_Torque = 0x00;
            Herkulex1_On_radioButton.Checked = false;
            Herkulex1_Off_radioButton.Checked = true;
            Herkulex1_Brake_radioButton.Checked = false;
            Herkulex1_Torque_Control = 0x01;
        }

        private void Herkulex1_Brake_radioButton_Click(object sender, EventArgs e)
        {
            Herkulex1_Torque = 0x40;
            Herkulex1_On_radioButton.Checked = false;
            Herkulex1_Off_radioButton.Checked = false;
            Herkulex1_Brake_radioButton.Checked = true;
            Herkulex1_Torque_Control = 0x01;
        }

        private void Herkulex2_On_radioButton_Click(object sender, EventArgs e)
        {
            Herkulex2_Torque = 0x60;
            Herkulex2_On_radioButton.Checked = true;
            Herkulex2_Off_radioButton.Checked = false;
            Herkulex2_Brake_radioButton.Checked = false;
            Herkulex2_Torque_Control = 0x01;
        }

        private void Herkulex2_Off_radioButton_Click(object sender, EventArgs e)
        {
            Herkulex2_Torque = 0x00;
            Herkulex2_On_radioButton.Checked = false;
            Herkulex2_Off_radioButton.Checked = true;
            Herkulex2_Brake_radioButton.Checked = false;
            Herkulex2_Torque_Control = 0x01;
        }

        private void Herkulex2_Brake_radioButton_Click(object sender, EventArgs e)
        {
            Herkulex2_Torque = 0x40;
            Herkulex2_On_radioButton.Checked = false;
            Herkulex2_Off_radioButton.Checked = false;
            Herkulex2_Brake_radioButton.Checked = true;
            Herkulex2_Torque_Control = 0x01;
        }

        private void Herkulex3_On_radioButton_Click(object sender, EventArgs e)
        {
            Herkulex3_Torque = 0x60;
            Herkulex3_On_radioButton.Checked = true;
            Herkulex3_Off_radioButton.Checked = false;
            Herkulex3_Brake_radioButton.Checked = false;
            Herkulex3_Torque_Control = 0x01;
        }

        private void Herkulex3_Off_radioButton_Click(object sender, EventArgs e)
        {
            Herkulex3_Torque = 0x00;
            Herkulex3_On_radioButton.Checked = false;
            Herkulex3_Off_radioButton.Checked = true;
            Herkulex3_Brake_radioButton.Checked = false;
            Herkulex3_Torque_Control = 0x01;
        }

        private void Herkulex3_Brake_radioButton_Click(object sender, EventArgs e)
        {
            Herkulex3_Torque = 0x40;
            Herkulex3_On_radioButton.Checked = false;
            Herkulex3_Off_radioButton.Checked = false;
            Herkulex3_Brake_radioButton.Checked = true;
            Herkulex3_Torque_Control = 0x01;
        }

        private void Herkulex4_On_radioButton_Click(object sender, EventArgs e)
        {
            Herkulex4_Torque = 0x60;
            Herkulex4_On_radioButton.Checked = true;
            Herkulex4_Off_radioButton.Checked = false;
            Herkulex4_Brake_radioButton.Checked = false;
            Herkulex4_Torque_Control = 0x01;
        }

        private void Herkulex4_Off_radioButton_Click(object sender, EventArgs e)
        {
            Herkulex4_Torque = 0x00;
            Herkulex4_On_radioButton.Checked = false;
            Herkulex4_Off_radioButton.Checked = true;
            Herkulex4_Brake_radioButton.Checked = false;
            Herkulex4_Torque_Control = 0x01;
        }

        private void Herkulex4_Brake_radioButton_Click(object sender, EventArgs e)
        {
            Herkulex4_Torque = 0x40;
            Herkulex4_On_radioButton.Checked = false;
            Herkulex4_Off_radioButton.Checked = false;
            Herkulex4_Brake_radioButton.Checked = true;
            Herkulex4_Torque_Control = 0x01;
        }

        private void Herkulex5_On_radioButton_Click(object sender, EventArgs e)
        {
            Herkulex5_Torque = 0x60;
            Herkulex5_On_radioButton.Checked = true;
            Herkulex5_Off_radioButton.Checked = false;
            Herkulex5_Brake_radioButton.Checked = false;
            Herkulex5_Torque_Control = 0x01;
        }

        private void Herkulex5_Off_radioButton_Click(object sender, EventArgs e)
        {
            Herkulex5_Torque = 0x00;
            Herkulex5_On_radioButton.Checked = false;
            Herkulex5_Off_radioButton.Checked = true;
            Herkulex5_Brake_radioButton.Checked = false;
            Herkulex5_Torque_Control = 0x01;
        }

        private void Herkulex5_Brake_radioButton_Click(object sender, EventArgs e)
        {
            Herkulex5_Torque = 0x40;
            Herkulex5_On_radioButton.Checked = false;
            Herkulex5_Off_radioButton.Checked = false;
            Herkulex5_Brake_radioButton.Checked = true;
            Herkulex5_Torque_Control = 0x01;
        }

        private void Herkulex1_Position_hScrollBar_Scroll(object sender, ScrollEventArgs e)
        {
            Herkulex1_Position = Herkulex1_Position_hScrollBar.Value;
            Herkulex1_Position_Value_Label.Text = String.Format("{0}", Herkulex1_Position);
            Herkulex1_Position_Control = 0x01;
        }

        private void Herkulex2_Position_hScrollBar_Scroll(object sender, ScrollEventArgs e)
        {
            Herkulex2_Position = Herkulex2_Position_hScrollBar.Value;
            Herkulex2_Position_Value_Label.Text = String.Format("{0}", Herkulex2_Position);
            Herkulex2_Position_Control = 0x01;
        }

        private void Herkulex3_Position_hScrollBar_Scroll(object sender, ScrollEventArgs e)
        {
            Herkulex3_Position = Herkulex3_Position_hScrollBar.Value;
            Herkulex3_Position_Value_Label.Text = String.Format("{0}", Herkulex3_Position);
            Herkulex3_Position_Control = 0x01;
        }

        private void Herkulex4_Position_hScrollBar_Scroll(object sender, ScrollEventArgs e)
        {
            Herkulex4_Position = Herkulex4_Position_hScrollBar.Value;
            Herkulex4_Position_Value_Label.Text = String.Format("{0}", Herkulex4_Position);
            Herkulex4_Position_Control = 0x01;
        }

        private void Herkulex5_Position_hScrollBar_Scroll(object sender, ScrollEventArgs e)
        {
            Herkulex5_Position = Herkulex5_Position_hScrollBar.Value;
            Herkulex5_Position_Value_Label.Text = String.Format("{0}", Herkulex5_Position);
            Herkulex5_Position_Control = 0x01;
        }

        private void Herkulex_radioButton_Click(object sender, EventArgs e)
        {
            if (Herkulex_radioButton.Checked == false)
            {
                Herkulex_radioButton.Checked=true;
                groupBox4.Enabled = true;
            }
            else
            {
                Herkulex_radioButton.Checked = false;
                groupBox4.Enabled = false;
            }
        }

        private void Sensor_radioButton_Click(object sender, EventArgs e)
        {
            if (Sensor_radioButton.Checked == false)
            {
                Sensor_radioButton.Checked = true;
                Sensor_groupBox.Enabled = true;
            }
            else
            {
                Sensor_radioButton.Checked = false;
                Sensor_groupBox.Enabled = false;
            }

        }

        private void Motor_radioButton_Click(object sender, EventArgs e)
        {
            if (Motor_radioButton.Checked == false)
            {
                Motor_radioButton.Checked = true;
                Motor_groupBox.Enabled = true;
            }
            else
            {
                Motor_radioButton.Checked = false;
                Motor_groupBox.Enabled = false;
            }
        }

        private void Ind_Motor_Stream_radioButton_Click(object sender, EventArgs e)
        {
            Ind_Motor_Stream_radioButton.Checked = true;
            Control_Stream_radioButton.Checked = false;
            //Ind_Control_groupBox.Enabled = true;
            //Robot_Control_groupBox.Enabled = false;

        }

        private void Control_Stream_radioButton_Click(object sender, EventArgs e)
        {
            Ind_Motor_Stream_radioButton.Checked = false;
            Control_Stream_radioButton.Checked = true;
            //Ind_Control_groupBox.Enabled = false;
            //Robot_Control_groupBox.Enabled = true;
        }

        private void Front_Left_hScrollBar_Scroll(object sender, ScrollEventArgs e)
        {
            if (Ind_Motor_Stream_radioButton.Checked == true)
            {
                Left_Wheel_Velocity = Front_Left_hScrollBar.Value;
                Front_Left_label.Text = String.Format("{0}", Front_Left_hScrollBar.Value);
                Ind_Velocity_Control = 0x01;
            }
            else
            {
                Front_Left_hScrollBar.Value = Front_Left_Wheel_Velocity;
            }
        }

        private void Mid_Left_hScrollBar_Scroll(object sender, ScrollEventArgs e)
        {
            if (Ind_Motor_Stream_radioButton.Checked == true)
            {
                Mid_Left_label.Text = String.Format("{0}", Mid_Left_hScrollBar.Value);
                Ind_Velocity_Control = 0x01;
            }
            else
            {
                Mid_Left_hScrollBar.Value = Mid_Left_Wheel_Velocity;
            }

        }

        private void Rear_Left_hScrollBar_Scroll(object sender, ScrollEventArgs e)
        {
            if (Ind_Motor_Stream_radioButton.Checked == true)
            {
                Rear_Left_label.Text = String.Format("{0}", Rear_Left_hScrollBar.Value);
                Ind_Velocity_Control = 0x01;
            }
            else
            {
                Rear_Left_hScrollBar.Value = Rear_Left_Wheel_Velocity;
            }


        }

        private void Front_Right_hScrollBar_Scroll(object sender, ScrollEventArgs e)
        {
            if (Ind_Motor_Stream_radioButton.Checked == true)
            {
                Front_Right_label.Text = String.Format("{0}", Front_Right_hScrollBar.Value);
                Ind_Velocity_Control = 0x01;
            }
            else
            {
                Front_Right_hScrollBar.Value = Front_Right_Wheel_Velocity;
            }

        }

        private void Mid_Right_hScrollBar_Scroll(object sender, ScrollEventArgs e)
        {
            if (Ind_Motor_Stream_radioButton.Checked == true)
            {
                Mid_Right_label.Text = String.Format("{0}", Mid_Right_hScrollBar.Value);
                Ind_Velocity_Control = 0x01;
            }
            else
            {
                Mid_Right_hScrollBar.Value = Mid_Right_Wheel_Velocity;
            }

        }

        private void Rear_Right_hScrollBar_Scroll(object sender, ScrollEventArgs e)
        {
            if (Ind_Motor_Stream_radioButton.Checked == true)
            {
                Rear_Right_label.Text = String.Format("{0}", Rear_Right_hScrollBar.Value);
                Ind_Velocity_Control = 0x01;
            }
            else
            {
                Rear_Right_hScrollBar.Value = Rear_Right_Wheel_Velocity;
            }
        }
       
        private void Angular_Velocity_vScrollBar_Scroll(object sender, ScrollEventArgs e)
        {
            if (Control_Stream_radioButton.Checked == true)
            {
                Angular_Velocity = Angular_Velocity_vScrollBar.Value;
                Angular_Velocity_label.Text = String.Format("{0}", Angular_Velocity);

                Wheel_Control(Linear_Velocity, Angular_Velocity);

                Front_Left_Wheel_Velocity = Left_Wheel_Velocity;
                Mid_Left_Wheel_Velocity = Left_Wheel_Velocity;
                Rear_Left_Wheel_Velocity = Left_Wheel_Velocity;

                Front_Right_Wheel_Velocity = Right_Wheel_Velocity;
                Mid_Right_Wheel_Velocity = Right_Wheel_Velocity;
                Rear_Right_Wheel_Velocity = Right_Wheel_Velocity;

                Front_Left_hScrollBar.Value = Front_Left_Wheel_Velocity;
                Mid_Left_hScrollBar.Value = Mid_Left_Wheel_Velocity;
                Rear_Left_hScrollBar.Value = Rear_Left_Wheel_Velocity;

                Front_Right_hScrollBar.Value = Front_Right_Wheel_Velocity;
                Mid_Right_hScrollBar.Value = Mid_Right_Wheel_Velocity;
                Rear_Right_hScrollBar.Value = Rear_Right_Wheel_Velocity;

                Front_Left_label.Text = string.Format("{0}", Front_Left_hScrollBar.Value);
                Mid_Left_label.Text = string.Format("{0}", Mid_Left_hScrollBar.Value);
                Rear_Left_label.Text = string.Format("{0}", Rear_Left_hScrollBar.Value);

                Front_Right_label.Text = string.Format("{0}", Front_Right_hScrollBar.Value);
                Mid_Right_label.Text = string.Format("{0}", Mid_Right_hScrollBar.Value);
                Rear_Right_label.Text = string.Format("{0}", Rear_Right_hScrollBar.Value);

                Robot_Velocity_Control = 0x01;
            }
            else
            {
                Linear_Velocity_vScrollBar.Value = Linear_Velocity;
                Angular_Velocity_vScrollBar.Value = Angular_Velocity;
            }
        }
        
        void Wheel_Control(int Linear_V, int Angular_V)
        {
            if (Angular_V>=0)
            {
                Left_Wheel_Velocity = Linear_V + Angular_V; ;
                Right_Wheel_Velocity =-( Linear_V-Angular_V);      //Estava positivo          
            }
            else
            {
                Right_Wheel_Velocity = Linear_V - Angular_V;
                Left_Wheel_Velocity =-( Linear_V+Angular_V);      //Estava positivo
            }
            Left_Wheel_Value_label.Text = String.Format("{0}", Left_Wheel_Velocity);
            Right_Wheel_Value_label.Text = String.Format("{0}", Right_Wheel_Velocity);                
        }

        private void Linear_Velocity_vScrollBar_Scroll(object sender, ScrollEventArgs e)
        {
            if (Control_Stream_radioButton.Checked == true)
            {
                Linear_Velocity = -Linear_Velocity_vScrollBar.Value;
                Linear_Velocity_label.Text = String.Format("{0}", Linear_Velocity);

                Wheel_Control(Linear_Velocity, Angular_Velocity);


                Front_Left_Wheel_Velocity = Left_Wheel_Velocity;
                Mid_Left_Wheel_Velocity = Left_Wheel_Velocity;
                Rear_Left_Wheel_Velocity = Left_Wheel_Velocity;

                Front_Right_Wheel_Velocity = Right_Wheel_Velocity;
                Mid_Right_Wheel_Velocity = Right_Wheel_Velocity;
                Rear_Right_Wheel_Velocity = Right_Wheel_Velocity;

                Front_Left_hScrollBar.Value = Front_Left_Wheel_Velocity;
                Mid_Left_hScrollBar.Value = Mid_Left_Wheel_Velocity;
                Rear_Left_hScrollBar.Value = Rear_Left_Wheel_Velocity;

                Front_Right_hScrollBar.Value = Front_Right_Wheel_Velocity;
                Mid_Right_hScrollBar.Value = Mid_Right_Wheel_Velocity;
                Rear_Right_hScrollBar.Value = Rear_Right_Wheel_Velocity;

                Front_Left_label.Text = string.Format("{0}", Front_Left_hScrollBar.Value);
                Mid_Left_label.Text = string.Format("{0}", Mid_Left_hScrollBar.Value);
                Rear_Left_label.Text = string.Format("{0}", Rear_Left_hScrollBar.Value);

                Front_Right_label.Text = string.Format("{0}", Front_Right_hScrollBar.Value);
                Mid_Right_label.Text = string.Format("{0}", Mid_Right_hScrollBar.Value);
                Rear_Right_label.Text = string.Format("{0}", Rear_Right_hScrollBar.Value);


                Robot_Velocity_Control = 0x01;
            }
            
        }

        private void Hardstop_Timer_vScrollBar_Scroll(object sender, ScrollEventArgs e)
        {
            Hardstop_Timer = Hardstop_Timer_vScrollBar.Maximum - Hardstop_Timer_vScrollBar.Value;
            Hardstop_Timer_label.Text = String.Format("{0} s",Hardstop_Timer/10.0);
            Hardstop_Time_Control = 0x01;
        }

        private void Hardstop_radioButton_Click(object sender, EventArgs e)
        {
            if (Hardstop_radioButton.Checked == false) Hardstop_radioButton.Checked = true;
            else Hardstop_radioButton.Checked = false;
            HardStop_Control = 0x01;
        }

        private void Herkulex_Play_Time_hScrollBar_Scroll(object sender, ScrollEventArgs e)
        {
            Herkulex_Play_Time_label.Text = String.Format("{0}", Herkulex_Play_Time_hScrollBar.Value);
            Herkulex_Playtime = (byte)Herkulex_Play_Time_hScrollBar.Value;
        }

        private void B0_radioButton_Click(object sender, EventArgs e)
        {
            if (B0_radioButton.Checked == false) { B0_radioButton.Checked = true; Lights |= 0x01; }
            else { B0_radioButton.Checked = false; Lights &= 0x0E; }
            Lights_Control = 0x01;
        }

        private void B1_radioButton_Click(object sender, EventArgs e)
        {
            if (B1_radioButton.Checked == false) { B1_radioButton.Checked = true; Lights |= 0x02; }
            else { B1_radioButton.Checked = false; Lights &= 0x0D; }
            Lights_Control = 0x01;
        }

        private void B2_radioButton_Click(object sender, EventArgs e)
        {
            if (B2_radioButton.Checked == false) { B2_radioButton.Checked = true; Lights |= 0x04; }
            else { B2_radioButton.Checked = false; Lights &= 0x0B; }
            Lights_Control = 0x01;
        }

        private void B3_radioButton_Click(object sender, EventArgs e)
        {
            if (B3_radioButton.Checked == false) { B3_radioButton.Checked = true; Lights |= 0x08; }
            else { B3_radioButton.Checked = false; Lights &= 0x07; }
            Lights_Control = 0x01;
        }

        private void Linear_Motor_Velocity_vScrollBar_Scroll(object sender, ScrollEventArgs e)
        {
            Linear_Motor_Velocity = Linear_Motor_Velocity_vScrollBar.Maximum - Linear_Motor_Velocity_vScrollBar.Value;
            Linear_Motor_Velocity_label.Text = String.Format("{0}", Linear_Motor_Velocity);
            Linear_Motor_Velocity_Control = 0x01;
        }

        private void Linear_Motor_Position_vScrollBar_Scroll(object sender, ScrollEventArgs e)
        {
            Linear_Motor_Position = Linear_Motor_Position_vScrollBar.Value;
            Linear_Motor_Position_label.Text = String.Format("{0}", Linear_Motor_Position);
            Linear_Motor_Position_Control = 0x01;
        }

       



        private void Find_Serial_button_Click(object sender, EventArgs e)
        {
            int number_Ports = 0;
            int NewPorts = 0;

            string[] theSerialPortNames = System.IO.Ports.SerialPort.GetPortNames();

            Sensor_radioButton.Enabled = false;
            Motor_radioButton.Enabled = false;
            Sensor_groupBox.Enabled = false;
            Motor_groupBox.Enabled = false;

            foreach (string port in theSerialPortNames)
            {
                comboBox1.Items.Add(port);
                number_Ports++;
            }
            for (int i = 0; i < number_Ports; i++)
            {
                SIAR_Motor_Board = new HWAPI_M.HwBoard.SIAR_M_Board(theSerialPortNames[i], 115200);
                System.Threading.Thread.Sleep(100);
                if (SIAR_Motor_Board.Serial_ports_open == 1)
                {
                    SIAR_Motor_Board.Get_firmware_version_number();
                    if (SIAR_Motor_Board.FirmwareBoard[5] == '1') { Sensor_Board_COM_label.Text = theSerialPortNames[i]; NewPorts++; Sensor_radioButton.Checked = true; Sensor_radioButton.Enabled = true; }
                    if (SIAR_Motor_Board.FirmwareBoard[5] == '2') { Motor_Board_COM_label.Text = theSerialPortNames[i]; NewPorts++; Motor_radioButton.Checked = true; Motor_radioButton.Enabled = true;  }
                }
                SIAR_Motor_Board.Disconnect();
            }
            if (NewPorts != 0) Connect_Button.Enabled = true;
            else Connect_Button.Enabled = false;

        
        }

        private void Connect_Button_Click(object sender, EventArgs e)
        {
            if (timer1.Enabled == false)
            {
                Connect_Button.Text = String.Format("Disconnect");
                Find_Serial_button.Enabled = false;
                if (Sensor_radioButton.Enabled == true && Sensor_radioButton.Checked == true)
                {
                    SIAR_Sensor_Board = new HWAPI_S.HwBoard.SIAR_S_Board(Sensor_Board_COM_label.Text, 115200);
                    if (SIAR_Sensor_Board.Serial_ports_open == 1)
                    {
                        Sensor_groupBox.Enabled = true;
                        SIAR_Sensor_Board.Get_firmware_version_number();
                        string s = new string(SIAR_Sensor_Board.FirmwareBoard);
                        Sensor_Firmware_Number_label.Text = String.Format("{0}", s);
                    }
                    else
                    {
                        Sensor_radioButton.Enabled = false;
                        Sensor_radioButton.Checked = false;
                    }
                }
                if (Motor_radioButton.Enabled == true && Motor_radioButton.Checked == true)
                {
                    SIAR_Motor_Board = new HWAPI_M.HwBoard.SIAR_M_Board(Motor_Board_COM_label.Text, 115200);
                    if (SIAR_Motor_Board.Serial_ports_open == 1)
                    {
                        Motor_groupBox.Enabled = true;
                        SIAR_Motor_Board.Get_firmware_version_number();
                        string s = new string(SIAR_Motor_Board.FirmwareBoard);
                        Motor_Firmware_Number_label.Text = String.Format("{0}", s);
                    }
                    else
                    {
                        Motor_radioButton.Enabled = false;
                        Motor_radioButton.Checked = false;
                    }
                }
                timer1.Enabled = true;
            }
            else
            {
                Connect_Button.Text = String.Format("Connect");
                if (SIAR_Motor_Board.Serial_ports_open == 1)
                {
                    SIAR_Motor_Board.Disconnect();
                }
                if (SIAR_Sensor_Board.Serial_ports_open == 1)
                {
                    SIAR_Sensor_Board.Disconnect();
                }
                Sensor_groupBox.Enabled = false;
                Motor_groupBox.Enabled = false;
                Find_Serial_button.Enabled = true;
                timer1.Enabled = false;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Herkulex_Play_Time_label.Text = String.Format("{0}",Herkulex_Play_Time_hScrollBar.Value.ToString());
        }

        private void Herkulex1_clear_button_Click(object sender, EventArgs e)
        {
            Herkulex1_Clear_Control = 0x01;
        }

        private void Herkulex2_clear_button_Click(object sender, EventArgs e)
        {
            Herkulex2_Clear_Control = 0x01;
        }

        private void Herkulex3_clear_button_Click(object sender, EventArgs e)
        {
            Herkulex3_Clear_Control = 0x01;
        }

        private void Herkulex4_clear_button_Click(object sender, EventArgs e)
        {
            Herkulex4_Clear_Control = 0x01;
        }

        private void Herkulex5_clear_button_Click(object sender, EventArgs e)
        {
            Herkulex5_Clear_Control = 0x01;
        }

        private void label38_Click(object sender, EventArgs e)
        {

        }

        


       
         
    }
}
