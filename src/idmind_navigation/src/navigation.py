#!/usr/bin/env python

import rospy
from std_msgs.msg import Float64
from geometry_msgs.msg import Twist
from idmind_messages.msg import Log, Velocities
from std_srvs.srv import Trigger, TriggerResponse
from numpy import pi, sin, cos, tan, arctan2, sign, sqrt

VERBOSE = 4
LOGS = 4


class NavigationNode:
    def __init__(self):
        self.logging = rospy.Publisher("/idmind_logging", Log, queue_size=10)

        #  ROS  #
        # Parameters
        success = False
        while not success:
            try:
                self.control_freq = rospy.get_param("/bot/control_freq", 20)
                self.wheel_radius = rospy.get_param("idmind_motors/wheel_radius", 0.125)  
                self.time_tolerance = rospy.get_param("idmind_motors/time_tolerance", 0.5)
                try:
                    self.max_linear_speed = rospy.get_param("/bot/max_vel")
                    self.max_linear_acc = rospy.get_param("/bot/max_linear_acc")
                    self.max_angular_acc = rospy.get_param("/bot/max_angular_acc")
                except KeyError:
                    self.max_linear_speed = rospy.get_param("idmind_motors/max_vel", 2.0)
                    self.max_linear_acc = rospy.get_param("idmind_motors/max_linear_acc", 2.0)
                    self.max_angular_acc = rospy.get_param("idmind_motors/max_angular_acc", 4.00)                    
                self.axis_dist = 0.3
                self.wheel_drive = rospy.get_param("idmind_motors/wheel_drive", "6wd")                
                success = True
            except:
                rospy.sleep(1)

        # Subscribers
        self.base_width = 0.53
        rospy.Subscriber("idmind_motors/width", Float64, self.handle_width)
        self.twist = Twist()
        self.last_twist = Twist()
        self.last_twist_time = rospy.Time.now()
        rospy.Subscriber("idmind_navigation/cmd_vel", Twist, self.handle_velocities)

        # Publishers
        self.motor_velocities = [0]*6
        self.velocities_pub = rospy.Publisher("idmind_motors/set_velocities", Velocities, queue_size=10)

        # Services
        self.ready = True
        rospy.Service("/idmind_navigation/ready", Trigger, self.report_ready)
        self.log("Node initiated", 3)

    #########################
    #  AUXILIARY FUNCTIONS  #
    #########################
    def log(self, msg, msg_level, log_level=-1, alert="info"):
        """
        Log function that publish in screen and in topic
        :param msg: Message to be published
        :param msg_level: Message level (1-10, where 1 is most important)
        :param log_level: Message level for logging (1-10, optional, -1 uses the same as msg_level)
        :param alert: Alert level of message - "info", "warn" or "error"
        :return:
        """
        if VERBOSE >= msg_level:
            if alert == "info":
                rospy.loginfo("{}: {}".format(rospy.get_name(), msg))
            elif alert == "warn":
                rospy.logwarn("{}: {}".format(rospy.get_name(), msg))
            elif alert == "error":
                rospy.logerr("{}: {}".format(rospy.get_name(), msg))
        if LOGS >= (log_level if log_level != -1 else msg_level):
            self.logging.publish(rospy.Time.now().to_sec(), rospy.get_name(), msg)

    ###############
    #  CALLBACKS  #
    ###############
    def report_ready(self, _req):
        """ Simple Service callback to show node is ready """
        return TriggerResponse(self.ready, rospy.get_name()+" is " + ("ready" if self.ready else "not ready"))

    def handle_velocities(self, msg):
        """ Callback to velocity messages published in /idmind_navigation/set_velocities. """
        self.twist = msg
        self.last_twist_time = rospy.Time.now()

    def handle_width(self, msg):
        """ Callback to width messages published in /idmind_motors/width. """
        self.base_width = msg.data

    ###################
    #   MAIN METHODS  #
    ###################
    def compute_velocity(self):
        """
        This method translates linear and angular velocities provided into wheel and steering commands
        :return:
        """
        
        # Get the new velocities
        v = [0]*6+[self.max_linear_speed]
        new_vx = self.twist.linear.x
        new_wz = self.twist.angular.z
        last_vx = self.last_twist.linear.x
        last_wz = self.last_twist.angular.z

        # Smooth the velocities. Limits will be applied at the end, for each wheel
        new_vx =max(min(new_vx, last_vx+self.max_linear_acc/self.control_freq), last_vx-self.max_linear_acc/self.control_freq)
        new_wz =max(min(new_wz, last_wz+self.max_angular_acc/self.control_freq), last_wz-self.max_angular_acc/self.control_freq)        
        self.last_twist.linear.x = new_vx
        self.last_twist.angular.z = new_wz

        # Compute speed for each wheel
        for idx in range(0, 6):
            # d is y distance from wheel to ICR (left wheels are closer to positive radius)
            d = (-self.base_width / 2) if (idx in [0, 2, 4]) else (self.base_width / 2)
            # s is x distance from wheel to ICR
            s = 0 if idx in [2, 3] else (-self.axis_dist if idx in [0, 1] else self.axis_dist)
            # alpha is the angle between the wheel and ICR
            alpha = arctan2(s, d)
            # Wheel Linear Velocity component to ensure turning
            wheel_dist = sqrt(pow(d, 2) + pow(s, 2)) / cos(alpha)            
            v[idx] = new_vx + new_wz * wheel_dist
        
        if VERBOSE > 4:
            print("=======================================")            
            print("V: {}| W: {}".format(new_vx, new_wz))
            print("\t{:.2f}\t||---||\t{:.2f}".format(v[0], v[1]))
            print("\t\t   |")
            print("\t{:.2f}\t||---||\t{:.2f}".format(v[2], v[3]))
            print("\t\t   |")
            print("\t{:.2f}\t||---||\t{:.2f}".format(v[4], v[5]))
            print("=======================================")

        # Normalize wheel velocities based on maximum velocity
        r = max([a / self.max_linear_speed for a in v])
        v_msg = Velocities()
        for idx in range(0, 6):
            v_msg.velocities[idx] = v[idx]
        self.velocities_pub.publish(v_msg)

    def shutdown(self):
        return True

    def start(self):
        r = rospy.Rate(self.control_freq)
        while not rospy.is_shutdown():
            try:
                self.compute_velocity()
                r.sleep()
            except KeyboardInterrupt:
                self.log("Shutdown by user", 1)
                break
            except Exception as err:
                self.log(err, 1, alert="error")
                r.sleep()
        self.shutdown()


if __name__ == "__main__":
    rospy.init_node("idmind_navigation")
    n = NavigationNode()
    n.start()
    rospy.loginfo("{}: Shutting down.".format(rospy.get_name()))
