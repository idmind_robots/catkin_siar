#!/usr/bin/env python

import rospy
from numpy import pi, sin, cos
from sensor_msgs.msg import Imu
from std_msgs.msg import Float64
from nav_msgs.msg import Odometry
from tf2_ros import TransformBroadcaster
from tf_conversions import transformations
from idmind_messages.msg import Log, Encoders
from geometry_msgs.msg import TransformStamped
from std_srvs.srv import Trigger, TriggerResponse, TriggerRequest

VERBOSE = 7
LOGS = 7
MAX_TRIES = 10


class OdometryNode:
    def __init__(self):
        self.control_freq = rospy.get_param("/bot/control_freq", 20)
        self.logging = rospy.Publisher("/idmind_logging", Log, queue_size=10)

        #  ROS  #
        # Parameters
        success = False
        while not success:
            try:
                self.control_freq = rospy.get_param("/bot/control_freq", 20)
                self.wheel_radius = rospy.get_param("idmind_motors/wheel_radius", 0.125)
                imu_topic = rospy.get_param("/siar/idmind_odometry/imu_topic", "imu")
                self.use_imu = rospy.get_param("/siar/idmind_odometry/use_imu", True)
                self.total_ticks = rospy.get_param("idmind_motors/ticks_turn", 213906)
                success = True
            except KeyError:
                self.log("Unable to load parameters", 5)
                rospy.sleep(1)

        # Subscribers
        self.encoders = rospy.wait_for_message("idmind_motors/encoders", Encoders)
        self.last_encoders = rospy.wait_for_message("idmind_motors/encoders", Encoders)
        rospy.Subscriber("idmind_motors/encoders", Encoders, self.handle_encoders)
        self.base_width = 0.53
        rospy.Subscriber("idmind_motors/width", Float64, self.handle_width)
        self.last_imu = rospy.Time.now()
        self.imu = Imu()
        self.imu.orientation.w = 1
        rospy.Subscriber(imu_topic, Imu, self.handle_imu)

        # Odometry
        odom_msg = Odometry()
        odom_msg.header.frame_id = "odom"
        odom_msg.header.stamp = rospy.Time.now()
        odom_msg.child_frame_id = "base_link"
        odom_msg.pose.pose.orientation.w = 1
        self.last_odom = odom_msg

        # Publishers
        # Publish odometry and broadcast odometry
        self.odom_broadcast = TransformBroadcaster()
        self.odometry_pub = rospy.Publisher("idmind_navigation/odometry", Odometry, queue_size=10)

        # Services
        self.odometry_restart_flag = False        
        rospy.Service("idmind_navigation/reset_odometry", Trigger, self.reset_odometry_callback)
        self.ready = True
        rospy.Service("idmind_odometry/ready", Trigger, self.report_ready)
        self.log("Node initiated", 3)

    #########################
    #  AUXILIARY FUNCTIONS  #
    #########################
    def log(self, msg, msg_level, log_level=-1, alert="info"):
        """
        Log function that publish in screen and in topic
        :param msg: Message to be published
        :param msg_level: Message level (1-10, where 1 is most important)
        :param log_level: Message level for logging (1-10, optional, -1 uses the same as msg_level)
        :param alert: Alert level of message - "info", "warn" or "error"
        :return:
        """
        if VERBOSE >= msg_level:
            if alert == "info":
                rospy.loginfo("{}: {}".format(rospy.get_name(), msg))
            elif alert == "warn":
                rospy.logwarn("{}: {}".format(rospy.get_name(), msg))
            elif alert == "error":
                rospy.logerr("{}: {}".format(rospy.get_name(), msg))
        if LOGS >= (log_level if log_level != -1 else msg_level):
            self.logging.publish(rospy.Time.now().to_sec(), rospy.get_name(), msg)

    ###############
    #  CALLBACKS  #
    ###############
    def report_ready(self, _req):
        """ Simple Service callback to show node is ready """
        return TriggerResponse(self.ready, rospy.get_name()+" is " + ("ready" if self.ready else "not ready"))

    def handle_encoders(self, msg):
        """ Callback to register the encoders values """
        self.encoders = msg

    def handle_imu(self, msg):
        """ Callback to save lastest Imu reading"""
        # TODO: confirm the orientation quaternion is valid, otherwise don't save!
        self.last_imu = rospy.Time.now()
        self.imu = msg

    def handle_width(self, msg):
        """ Callback to save current robot width """        
        self.base_width = msg.data

    def reset_odometry_callback(self, _req):
        self.odometry_restart_flag = True

        idx = 0
        while self.odometry_restart_flag:
            idx = idx + 1
            if idx == MAX_TRIES:
                return TriggerResponse(False, "Odometry may not have reset correctly after {} seconds".format(idx * 0.5))
            rospy.sleep(0.5)
        return TriggerResponse(True, "Odometry is reset")

    ###################
    #   MAIN METHODS  #
    ###################
    def publish_odometry(self):
        """ Method that computes and publishes robot odometry based on wheel and Imu information"""
        if self.odometry_restart_flag:
            x = 0.0
            y = 0.0            
            self.odometry_restart_flag = False
        else:
            x = self.last_odom.pose.pose.position.x
            y = self.last_odom.pose.pose.position.y
            last_q = [self.last_odom.pose.pose.orientation.x,
                    self.last_odom.pose.pose.orientation.y,
                    self.last_odom.pose.pose.orientation.z,
                    self.last_odom.pose.pose.orientation.w]

        odom_msg = Odometry()
        odom_msg.header.frame_id = "odom"
        odom_msg.header.stamp = rospy.Time.now()
        odom_msg.child_frame_id = "base_link"

        # Time interval
        dt = (odom_msg.header.stamp - self.last_odom.header.stamp).to_sec()
        if dt == 0:
            return

        # Linear velocity estimation
        ddist = [0]*6
        vw = [0]*6
        # If using encoders
        current_encoders = self.encoders
        for idx in range(0, 6):
            dticks = current_encoders.encoders[idx] - self.last_encoders.encoders[idx]
            dalpha = 2*pi*dticks/self.total_ticks
            ddist[idx] = dalpha*self.wheel_radius
            vw[idx] = (-1 if idx in [1, 3, 5] else 1)*ddist[idx]/dt
        vx = sum(vw)/len(vw)
        odom_msg.twist.twist.linear.x = vx

        # Use IMU readings for orientation
        if self.use_imu and ((rospy.Time.now()-self.imu.header.stamp).to_sec() < 1):
            current_q = [self.imu.orientation.x, self.imu.orientation.y, self.imu.orientation.z, self.imu.orientation.w]
            th = transformations.euler_from_quaternion(current_q)[2]
        # If not available, use wheel_odometry
        else:            
            self.log("Using wheel odometry - last imu reading {} secs ago".format((rospy.Time.now()-self.imu.header.stamp).to_sec()), 5)
            dright = -(ddist[1]+ddist[3]+ddist[5])/3
            dleft = (ddist[0]+ddist[2]+ddist[4])/3
            dth = (dright-dleft)/self.base_width
            last_th = transformations.euler_from_quaternion(last_q)[2]
            th = last_th + dth            

        # Compute next pose
        x += vx*cos(th)*dt
        y += vx*sin(th)*dt
        odom_msg.pose.pose.position.x = x
        odom_msg.pose.pose.position.y = y

        # odom_msg.pose.pose.orientation = self.imu.orientation  -- Removed due to errors in quaternions published
        q = transformations.quaternion_from_euler(0, 0, th)
        odom_msg.pose.pose.orientation.x = q[0]
        odom_msg.pose.pose.orientation.y = q[1]
        odom_msg.pose.pose.orientation.z = q[2]
        odom_msg.pose.pose.orientation.w = q[3]

        self.odometry_pub.publish(odom_msg)

        # Build Transform message for Broadcast
        transf_msg = TransformStamped()
        transf_msg.header.stamp = odom_msg.header.stamp
        transf_msg.header.frame_id = "odom"
        transf_msg.child_frame_id = "base_link"
        transf_msg.transform.translation.x = x
        transf_msg.transform.translation.y = y
        transf_msg.transform.rotation = odom_msg.pose.pose.orientation
        self.odom_broadcast.sendTransform(transf_msg)
        self.last_encoders = current_encoders
        self.last_odom = odom_msg
        return True

    @staticmethod
    def shutdown():
        return True

    def start(self):
        r = rospy.Rate(self.control_freq)
        while not rospy.is_shutdown():
            try:
                self.publish_odometry()
                r.sleep()
            except KeyboardInterrupt:
                self.log("Shutdown by user", 1)
                break
            except Exception as err:
                self.log(err, 1, alert="error")
                r.sleep()
        self.shutdown()


if __name__ == "__main__":
    rospy.init_node("idmind_odometry")
    o = OdometryNode()
    o.start()
    rospy.loginfo("{}: Shutting down.".format(rospy.get_name()))
