#!/usr/bin/env python

# from threading import Lock
import time
import subprocess
import serial.tools.list_ports
from idmind_serial2.idmind_serialport import IDMindSerial


class Sensors:
    """

    TODO: Include Herculex drivers
    """
    def __init__(self, address="", baudrate=115200, verbose=0, timeout=0.5):

        self.verbose = verbose
        self.system_electronic_battery_voltage = 0.
        self.system_motor_battery_voltage = 0.
        self.system_electronic_battery_current = 0.
        self.system_motor_battery_current = 0.
        self.cable_voltage = 0.
        self.electronic_battery_voltage = 0.
        self.motor_battery_voltage = 0.
        self.electronic_battery_current = 0.
        self.motor_battery_current = 0.
        self.electronic_battery_integrated_current = 0.
        self.motor_battery_integrated_current = 0.
        self.electronic_level = 0.
        self.electronic_remain_time = 0.
        self.motor_level = 0.
        self.motor_remain_time = 0.
        self.aux_pins_values = 0.
        self.firmware = ""

        connected = False
        while not connected:
            try:
                if len(address) > 0:
                    if self.verbose > 2:
                        print("Connecting to SensorsBoard on {}".format(address))
                    self.s = IDMindSerial(addr=address, baudrate=baudrate, timeout=timeout, verbose=self.verbose)
                    connected = True
                else:
                    self.search_connect()
                    connected = True
            except KeyboardInterrupt:
                print("\tInterrupted by user")
                raise KeyboardInterrupt
            except Exception as err:
                print("\tException occurred:\n\t\t{}".format(err))

        try:
            subprocess.check_call(['setserial', self.s.port, 'low_latency'])
        except subprocess.CalledProcessError as err:
            if self.verbose > 2:
                print("\tUnable to set low latency - {}".format(err.returncode))
            time.sleep(0.5)

    ##########################
    #     Serial methods     #
    ##########################
    def search_connect(self):
        if self.verbose > 2:
            print("Searching for SensorBoard")
        for addr in [comport.device for comport in serial.tools.list_ports.comports()]:
            # If the lsof call returns an output, then the port is already in use!
            try:
                subprocess.check_output(['lsof', '+wt', addr])
                continue
            except subprocess.CalledProcessError:
                try:
                    if self.verbose > 4:
                        print("Connecting to SensorsBoard on {}".format(addr))
                    self.s = IDMindSerial(addr=addr, baudrate=115200, timeout=0.5, verbose=self.verbose)
                    res = self.get_firmware()
                    if res:
                        if "Board1" in res:
                            if self.verbose > 3:
                                print("\tSensorBoard found in {}".format(addr))
                                time.sleep(0.5)
                            return
                        else:
                            continue
                    else:
                        continue
                    time.sleep(0.5)
                except Exception as err:
                    print("Exception detecting SensorBoard motor: {}".format(err))
        raise Exception("Sensor Board not found")

    def close(self):
        try:
            self.s.close()
            return True
        except Exception as err:
            print("\tException closing serial: {}".format(err))
            return False

    #######################
    #     SET methods     #
    #######################
    def set_lights(self, lights):
        """
        set_lights
        [0x60][Lights]
        Lights=[XXXXXCBA] where:
            A=1/0 Front Light ON/OFF
            B=1/0 Rear Light ON/OFF
            C=1/0 Camera Ring Light ON/OFF
        receive:
        [0x60][Sent_number][Chsum_H][Chsum_L]
        """
        msg = [0x60]
        msg.extend([lights])
        if self.s.send_command(bytearray(msg)) != 2:
            print("Error sending set_lights")
            return False
        else:
            sonar_buffer = self.s.read_command(4)
            if len(sonar_buffer) == 4:
                checksum = self.s.to_num(sonar_buffer[2:4])
                bytesum = ord(sonar_buffer[0]) + ord(sonar_buffer[1])
                if ord(sonar_buffer[0]) == ord(msg[0]) and bytesum == checksum:
                    return True
                else:
                    print("Error with set_lights response")
                    return False
            else:
                print("Error receiving set_lights response")
                return False

    def set_aux_pins_directions(self, aux_pins):
        """
        set_aux_pins_directions
        [0x61][aux_pins_direction]
        Aux_Pins_Direction=[XXFEDCBA] where:
            A to F - Pin F0 to F5
            A to F = 0=O output
            A to F = 1=O  input
        receive:
        [0x61][Sent_number][Chsum_H][Chsum_L]
        """
        msg = [0x61]
        msg.extend([aux_pins])
        if self.s.send_command(bytearray(msg)) != 2:
            print("Error sending set_aux_pins_directions")
            return False
        else:
            sonar_buffer = self.s.read_command(4)
            if len(sonar_buffer) == 4:
                checksum = self.s.to_num(sonar_buffer[2:4])
                bytesum = ord(sonar_buffer[0]) + ord(sonar_buffer[1])
                if ord(sonar_buffer[0]) == ord(msg[0]) and bytesum == checksum:
                    return True
                else:
                    print("Error with set_aux_pins_directions response")
                    return False
            else:
                print("Error receiving set_aux_pins_directions response")
                return False

    def set_aux_pins_values(self, aux_pins):
        """
        set_aux_pins_values
        [0x62][aux_pins_values]
        Aux_Pins_Direction=[XXFEDCBA] where:
            A to F - Pin F0 to F5
            A to F = 0=0V
            A to F = 1=5V
        receive:
        [0x62][Sent_number][Chsum_H][Chsum_L]
        """
        msg = [0x62]
        msg.extend([aux_pins])
        if self.s.send_command(bytearray(msg)) != 2:
            print("Error sending set_aux_pins_values")
            return False
        else:
            sonar_buffer = self.s.read_command(4)
            if len(sonar_buffer) == 4:
                checksum = self.s.to_num(sonar_buffer[2:4])
                bytesum = ord(sonar_buffer[0]) + ord(sonar_buffer[1])
                if ord(sonar_buffer[0]) == ord(msg[0]) and bytesum == checksum:
                    return True
                else:
                    print("Error with set_aux_pins_values response")
                    return False
            else:
                print("Error receiving set_aux_pins_values response")
                return False

    #######################
    #     GET methods     #
    #######################
    def get_system_power_supply(self):
        """
        Retrieves the system Voltage and Current of the Electronics and Motors power supply
        Send: [0X51]
        Receive: [0x51][Elect_Battery_Voltage_High][Elect_Battery_Voltage_Low]
                       [Elect_Battery_Current_High][Elect_Battery_Current_High]
                       [Motor_Battery_Voltage_High][Motor_Battery_Voltage_Low]
                       [Motor_Battery_Current_High][Motor_Battery_Current_High]
                       [Cable_Power_Voltage_High][Cable_Power_Voltage_Low]
                       [Sent_number][Chsum_H][Chsum_L]
        :return:
        """
        msg = [0x51]
        if self.s.send_command(bytearray(msg)) != 1:
            print("Error reading system_power_supply")
            return False
        else:
            sonar_buffer = self.s.read_command(14)
            if len(sonar_buffer) == 14:
                checksum = self.s.to_num(sonar_buffer[-2:])
                bytesum = sum([ord(a) for a in sonar_buffer[0:-2]])
                if ord(sonar_buffer[0]) == msg[0] and bytesum == checksum:
                    self.system_electronic_battery_voltage = self.s.to_num(sonar_buffer[1:3])
                    self.system_electronic_battery_current = self.s.to_num(sonar_buffer[3:5])
                    self.system_motor_battery_voltage = self.s.to_num(sonar_buffer[5:7])
                    self.system_motor_battery_current = self.s.to_num(sonar_buffer[7:9])
                    self.cable_voltage = self.s.to_num(sonar_buffer[9:11])
                    return [self.system_electronic_battery_voltage, self.system_motor_battery_voltage,
                            self.system_electronic_battery_current, self.system_motor_battery_current,
                            self.cable_voltage]
                else:
                    print("Error with reading system_power_supply response")
                    return False
            else:
                print("Error receiving system_power_supply response: len={}".format(len(sonar_buffer)))
                print("R: "),
                for c in sonar_buffer:
                    print("{} ".format(ord(c))),
                print("")
                return False

    def get_voltages(self):
        """
        Retrieves the batteries voltages
        Send: [0X52]
        Receive: [0x52][Elect_Battery_Voltage_High][Elect_Battery_Voltage_Low]
                       [Motor_Battery_Voltage_High][Motor_Battery_Voltage_Low]
                       [Sent_number][Chsum_H][Chsum_L]
        :return:
        """
        msg = [0x52]
        if self.s.send_command(bytearray(msg)) != 1:
            print("Error sending get_voltages")
            return False
        else:
            sonar_buffer = self.s.read_command(8)
            if len(sonar_buffer) == 8:
                checksum = self.s.to_num(sonar_buffer[-2:])
                bytesum = sum([ord(a) for a in sonar_buffer[0:-2]])
                if ord(sonar_buffer[0]) == msg[0] and bytesum == checksum:
                    self.electronic_battery_voltage = self.s.to_num(sonar_buffer[1:3])
                    self.motor_battery_voltage = self.s.to_num(sonar_buffer[3:5])
                    return [self.electronic_battery_voltage, self.motor_battery_voltage]
                else:
                    print("Error with reading get_voltages response")
                    return False
            else:
                print("Error receiving reading get_voltages response")
                return False

    def get_current(self):
        """
        Retrieves the instantaneous current
        Send: [0X53]
        Receive: [0x53][Elect_Battery_Current_High][Elect_Battery_Current_Low]
                       [Motor_Battery_Current_High][Motor_Battery_Current_Low]
                       [Sent_number][Chsum_H][Chsum_L]
        :return:
        """
        msg = [0x53]
        if self.s.send_command(bytearray(msg)) != 1:
            print("Error sending get_current")
            return False
        else:
            sonar_buffer = self.s.read_command(8)
            if len(sonar_buffer) == 8:
                checksum = self.s.to_num(sonar_buffer[-2:])
                bytesum = sum([ord(a) for a in sonar_buffer[0:-2]])
                if ord(sonar_buffer[0]) == msg[0] and bytesum == checksum:
                    self.electronic_battery_current = self.s.to_num(sonar_buffer[1:3])
                    self.motor_battery_current = self.s.to_num(sonar_buffer[3:5])
                    return [self.electronic_battery_current, self.motor_battery_current]
                else:
                    print("Error with reading get_current response")
                    return False
            else:
                print("Error receiving reading get_current response")
                return False

    def get_integrated_current(self):
        """
        Retrieves the integrated(?) current
        Send: [0X54]
        Receive: [0x54][Elect_Integ_Current_High][Elect_Integ_Current_Low]
                       [Motor_Integ_Current_High][Motor_Integ_Current_Low]
                       [Sent_number][Chsum_H][Chsum_L]
        :return:
        """
        msg = [0x54]
        if self.s.send_command(bytearray(msg)) != 1:
            print("Error sending get_integrated_current")
            return False
        else:
            sonar_buffer = self.s.read_command(8)
            if len(sonar_buffer) == 8:
                checksum = self.s.to_num(sonar_buffer[-2:])
                bytesum = sum([ord(a) for a in sonar_buffer[0:-2]])
                if ord(sonar_buffer[0]) == msg[0] and bytesum == checksum:
                    self.electronic_battery_integrated_current = self.s.to_num(sonar_buffer[1:3])
                    self.motor_battery_integrated_current = self.s.to_num(sonar_buffer[3:5])
                    return [self.electronic_battery_integrated_current, self.motor_battery_integrated_current]
                else:
                    print("Error with reading get_integrated_current response")
                    return False
            else:
                print("Error receiving reading get_integrated_current response")
                return False

    def get_level(self):
        """
        Retrieves the operation level and remaining operation time
        Send: [0X55]
        Receive: [0x55][E_Level][E_Remain_Time]
                       [M_Level][M_Remain_Time]
                       [Sent_number][Chsum_H][Chsum_L]
                where level is charge level of a battery and
                remaining time is in minutes
        :return:
        """
        msg = [0x55]
        if self.s.send_command(bytearray(msg)) != 1:
            print("Error sending get_level")
            return False
        else:
            sonar_buffer = self.s.read_command(8)
            if len(sonar_buffer) == 8:
                checksum = self.s.to_num(sonar_buffer[-2:])
                bytesum = sum([ord(a) for a in sonar_buffer[0:-2]])
                if ord(sonar_buffer[0]) == msg[0] and bytesum == checksum:
                    self.electronic_level = ord(sonar_buffer[1])
                    self.electronic_remain_time = ord(sonar_buffer[2])*60
                    self.motor_level = ord(sonar_buffer[3])
                    self.motor_remain_time = ord(sonar_buffer[4])*60
                    return [self.electronic_level, self.electronic_remain_time,
                            self.motor_level, self.motor_remain_time]
                else:
                    print("Error with reading get_level response")
                    return False
            else:
                print("Error receiving reading get_level response")
                return False

    def get_auxiliary_pins_values(self):
        """
        Retrieves the operation level and remaining operation time
        Send: [0X56]
        Receive: [0x56][Aux_Pins_Values]
                       [Sent_number][Chsum_H][Chsum_L]

            where Aux_Pins_Values=[XXFEDCBA]
                    A to F = 0 -> 0V, 1 -> 5V
        :return:
        """
        msg = [0x56]
        if self.s.send_command(bytearray(msg)) != 1:
            print("Error sending get_auxiliary_pins_values")
            return False
        else:
            sonar_buffer = self.s.read_command(5)
            if len(sonar_buffer) == 5:
                checksum = self.s.to_num(sonar_buffer[-2:])
                bytesum = sum([ord(a) for a in sonar_buffer[0:-2]])
                if ord(sonar_buffer[0]) == msg[0] and bytesum == checksum:
                    self.aux_pins_values = ord(sonar_buffer[1])
                    return self.aux_pins_values
                else:
                    print("Error with reading get_auxiliary_pins_values response")
                    return False
            else:
                print("Error receiving reading get_auxiliary_pins_values response")
                return False

    def get_firmware(self):
        msg = [0x20]
        if self.s.send_command(bytearray(msg)) != 1:
            if self.verbose > 5:
                print("Error reading firmware")
            return False
        else:
            sonar_buffer = self.s.read_command(29)
            if len(sonar_buffer) == 29:
                checksum = self.s.to_num(sonar_buffer[-2:])
                bytesum = sum([ord(a) for a in sonar_buffer[0:-2]])
                if ord(sonar_buffer[0]) == 0x20 and bytesum == checksum:
                    self.firmware = str(sonar_buffer[1:26])
                    return self.firmware
                else:
                    if self.verbose > 5:
                        print("Error with reading firmware response")
                    return False
            else:
                if self.verbose > 5:
                    print("Error receiving firmware response")
                return False


if __name__ == "__main__":
    s = Sensors()
