#!/usr/bin/env python

import rospy
from std_msgs.msg import Bool
from sensors_driver import Sensors
from idmind_messages.msg import Log, Voltages, SystemStatus, SafetyStatus
from std_srvs.srv import Trigger, TriggerResponse, TriggerRequest, SetBool, SetBoolResponse, SetBoolRequest

VERBOSE = 7
LOGS = 7
MAX_TRIES = 10


class SensorNode:
    def __init__(self):
        self.ready = False
        self.control_freq = rospy.get_param("/bot/control_freq", default=20)
        self.logging = rospy.Publisher("/idmind_logging", Log, queue_size=10)
        self.board = None
        if not self.connect():
            raise Exception("Failed to connect to board")
        self.fails = 0

        #########
        #  ROS  #
        #########
        # Publishers
        self.voltages = Voltages()
        self.voltages_pub = rospy.Publisher("idmind_sensors/voltages", Voltages, queue_size=10)

        # Services

        self.ready = True
        rospy.Service("/idmind_sensors/ready", Trigger, self.report_ready)
        self.log("Node initiated", 3)

    #########################
    #  AUXILIARY FUNCTIONS  #
    #########################
    def log(self, msg, msg_level, log_level=-1, alert="info"):
        """
        Log function that publish in screen and in topic
        :param msg: Message to be published
        :param msg_level: Message level (1-10, where 1 is most important)
        :param log_level: Message level for logging (1-10, optional, -1 uses the same as msg_level)
        :param alert: Alert level of message - "info", "warn" or "error"
        :return:
        """
        if VERBOSE >= msg_level:
            if alert == "info":
                rospy.loginfo("{}: {}".format(rospy.get_name(), msg))
            elif alert == "warn":
                rospy.logwarn("{}: {}".format(rospy.get_name(), msg))
            elif alert == "error":
                rospy.logerr("{}: {}".format(rospy.get_name(), msg))
        if LOGS >= (log_level if log_level != -1 else msg_level):
            self.logging.publish(rospy.Time.now().to_sec(), rospy.get_name(), msg)

    def connect(self):
        """
        Auxiliary method that handles connection to the board.
        After connection, the motor drives should be enabled and halted
        :return :
        """
        while not rospy.is_shutdown():
            try:
                self.board = Sensors(verbose=3)
                rospy.sleep(2)
                return True
            except KeyboardInterrupt:
                return False
        return True

    def reset_board(self):
        """
        Auxiliary method that handles a reset of the board.
        After connection, the motor drives should also be reset.
        :return:
        """
        self.ready = False
        try:
            self.board.close()
        except Exception:
            pass
        self.connect()
        self.ready = True
        self.fails = 0
        rospy.sleep(1)

    ###################
    #    CALLBACKS    #
    ###################
    def report_ready(self, _req):
        """ Simple Service callback to show node is ready """
        return TriggerResponse(self.ready, rospy.get_name()+" is " + ("ready" if self.ready else "not ready"))

    #########################
    #  Auxiliary functions  #
    #########################

    ####################
    #  Loop Functions  #
    ####################
    def update_data(self):
        """
        Method to be called at every loop, to update board info
        :return:
        """
        try:
            v = self.board.get_system_power_supply()
            if v:
                volt_msg = Voltages()
                volt_msg.electronic_battery_voltage = v[0]*0.1
                volt_msg.motor_battery_voltage = v[1]*0.1                
                volt_msg.electronic_battery_current = v[2]*0.1
                volt_msg.motor_battery_current = v[3]*0.1
                volt_msg.cable_power_voltage = v[4]*0.1                
                self.voltages_pub.publish(volt_msg)
            else:
                self.fails += 1
                self.log("Error reading voltages", 3)

        except Exception as err:
            self.fails += 1
            self.log("Exception updating data: {}".format(err), 2)

    def shutdown(self):
        """
        Shutdown rotine. Only close the node after the steering motor and wheels have been disabled.
        :return:
        """
        self.board.close()

    def start(self):
        r = rospy.Rate(self.control_freq)
        while not rospy.is_shutdown():
            # If the connection drops, try to reconnect
            if self.fails > 10:
                self.reset_board()
            try:
                self.update_data()
                r.sleep()
            except KeyboardInterrupt:
                self.log("Stopped by user", 1)
                r.sleep()
                break
            except Exception as err:
                self.log("Exception occurred: {}".format(err), 1)

        self.shutdown()


if __name__ == "__main__":
    rospy.init_node("idmind_sensors")
    s = SensorNode()
    s.start()
    rospy.loginfo("{}: Shutting down.".format(rospy.get_name()))