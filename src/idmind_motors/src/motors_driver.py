#!/usr/bin/env python

# from threading import Lock
import time
import subprocess
import serial.tools.list_ports
from idmind_serial2.idmind_serialport import IDMindSerial


class Motors:
    def __init__(self, address="", baudrate=115200, verbose=0, timeout=0.5):

        self.verbose = verbose
        self.front_left_ticks = 0
        self.front_right_ticks = 0
        self.mid_left_ticks = 0
        self.mid_right_ticks = 0
        self.back_left_ticks = 0
        self.back_right_ticks = 0

        self.linear_motor_position = 0
        self.linear_motor_potentiometer = 0

        self.hardstop_status = 0
        self.hardstop_timer = 0

        self.fan_control = 0

        self.firmware = ""

        connected = False
        while not connected:
            try:
                if len(address) > 0:
                    if self.verbose > 2:
                        print("Connecting to MotorsBoard on {}".format(address))
                    self.s = IDMindSerial(addr=address, baudrate=baudrate, timeout=timeout, verbose=self.verbose)
                    connected = True
                else:
                    self.search_connect()
                    connected = True
            except KeyboardInterrupt:
                print("\tInterrupted by user")
                raise KeyboardInterrupt
            except Exception as err:
                print("\tException occurred:\n\t\t{}".format(err))

        self.set_hardstop_time(5)
        
        try:
            subprocess.check_call(['setserial', self.s.port, 'low_latency'])
        except subprocess.CalledProcessError as err:
            if self.verbose > 2:
                print("\tUnable to set low latency - {}".format(err.returncode))
            time.sleep(0.5)

    ##########################
    #     Serial methods     #
    ##########################
    def search_connect(self):
        if self.verbose > 2:
            print("Searching for MotorsBoard")
        for addr in [comport.device for comport in serial.tools.list_ports.comports()]:
            # If the lsof call returns an output, then the port is already in use!
            try:
                subprocess.check_output(['lsof', '+wt', addr])
                continue
            except subprocess.CalledProcessError:
                try:
                    if self.verbose > 4:
                        print("Connecting to MotorsBoard on {}".format(addr))
                    self.s = IDMindSerial(addr=addr, baudrate=115200, timeout=0.5, verbose=self.verbose)
                    res = self.get_firmware()
                    if res:
                        if "Board2" in res:
                            if self.verbose > 3:
                                print("\MotorsBoard found in {}".format(addr))
                                time.sleep(0.5)
                            return
                        else:
                            continue
                    else:
                        continue                    
                except Exception as err:
                    print("Exception detecting MotorsBoard motor: {}".format(err))
        raise Exception("MotorsBoard not found")

    def close(self):
        try:
            self.s.close()
            return True
        except Exception as err:
            print("\tException closing serial: {}".format(err))
            return False

    #######################
    #     SET methods     #
    #######################
    def set_motor_velocities_control(self, front_left=0, front_right=0, mid_left=0, mid_right=0, back_left=0, back_right=0):
        """
        set_motor_velocities_control          
        [0X56][Front_Left_H][Front_Left_L]      
              [Front_Right_H][Front_Right_L]    
              [Mid_Left_H][Mid_Left_L]          
              [Mid_Right_H][Mid_Right_L]        
              [Rear_Left_H][Rear_Left_L]        
              [Rear_Right_H][Rear_Right_L]      
        receive:                                
        [0x56][Sent_number][Chsum_H][Chsum_L]

        :param front_left, front_right, mid_left, mid_right, back_left, back_right:
        :return True/False:
        """
        msg = [0x56]
        msg.extend(self.s.to_bytes(front_left))
        msg.extend(self.s.to_bytes(front_right))
        msg.extend(self.s.to_bytes(mid_left))
        msg.extend(self.s.to_bytes(mid_right))
        msg.extend(self.s.to_bytes(back_left))
        msg.extend(self.s.to_bytes(back_right))

        if self.s.send_command(bytearray(msg)) != 13:
            print("Error sending set_motor_velocities")
            return False
        else:
            velocities_buffer = self.s.read_command(4)
            if len(velocities_buffer) == 4:
                checksum = self.s.to_num(velocities_buffer[2:4])
                bytesum = ord(velocities_buffer[0]) + ord( velocities_buffer[1])
                if ord(velocities_buffer[0]) == msg[0] and bytesum == checksum:
                    return True
                else:
                    print("Error with set_motor_velocities response")
                    return False
            else:
                print("Error receiving set_motor_velocities response")
                return False

    def set_linear_motor_position_control(self, pos=0):
        """
        set_linear_motor_position_control          
        [0X30][Position_H][Position_L]      
        receive:                                
        [0x30][Sent_number][Chsum_H][Chsum_L]

        :param pos:
        :return True/False:
        """
        msg = [0x30]
        pos = max(min(1024, pos), 0)
        msg.extend(self.s.to_bytes(pos))

        if self.s.send_command(bytearray(msg)) != 3:
            print("Error sending set_linear_motor_position_control")
            return False
        else:
            linear_buffer = self.s.read_command(4)
            if len(linear_buffer) == 4:
                checksum = self.s.to_num(linear_buffer[2:4])
                bytesum = ord(linear_buffer[0]) + ord( linear_buffer[1])
                if ord(linear_buffer[0]) == msg[0] and bytesum == checksum:
                    return True
                else:
                    print("Error with set_linear_motor_position_control response")
                    return False
            else:
                print("Error receiving set_linear_motor_position_control response")
                return False

    def set_linear_motor_velocity_control(self, vel=0):
        """
        set_lineaset_linear_motor_velocity_controlr_motor_position_control          
        [0X31][Velocity_H][Velocity_L]      
        receive:                                
        [0x31][Sent_number][Chsum_H][Chsum_L]

        :param pos:
        :return True/False:
        """
        msg = [0x31]
        vel = max(min(1024, vel), 0)
        msg.extend(self.s.to_bytes(vel))

        if self.s.send_command(bytearray(msg)) != 3:
            print("Error sending set_linear_motor_velocity_control")
            return False
        else:
            linear_buffer = self.s.read_command(4)
            if len(linear_buffer) == 4:
                checksum = self.s.to_num(linear_buffer[2:4])
                bytesum = ord(linear_buffer[0]) + ord( linear_buffer[1])
                if ord(linear_buffer[0]) == msg[0] and bytesum == checksum:
                    return True
                else:
                    print("Error with set_linear_motor_velocity_control response")
                    return False
            else:
                print("Error receiving set_linear_motor_velocity_control response")
                return False

    def set_hardstop(self):
        """
        set_hardstop          
        [0X57]  
        receive:                                
        [0x57][Sent_number][Chsum_H][Chsum_L]

        :return True/False:
        """
        msg = [0x57]

        if self.s.send_command(bytearray(msg)) != 1:
            print("Error sending set_hardstop")
            return False
        else:
            hardstop_buffer = self.s.read_command(4)
            if len(hardstop_buffer) == 4:
                checksum = self.s.to_num(hardstop_buffer[2:4])
                bytesum = ord(hardstop_buffer[0]) + ord( hardstop_buffer[1])
                if ord(hardstop_buffer[0]) == msg[0] and bytesum == checksum:
                    return True
                else:
                    print("Error with set_hardstop response")
                    return False
            else:
                print("Error receiving set_hardstop response")
                return False   

    def set_hardstop_time(self,htime=100):
        """
        set_hardstop_time          
        [0X58][Hardstop_time] (between [0, 255]s)  
        receive:                                
        [0x58][Sent_number][Chsum_H][Chsum_L]

        :return True/False:
        """
        msg = [0x58, htime]

        if self.s.send_command(bytearray(msg)) != 2:
            print("Error sending set_hardstop_time")
            return False
        else:
            hardstop_buffer = self.s.read_command(4)
            if len(hardstop_buffer) == 4:
                checksum = self.s.to_num(hardstop_buffer[2:4])
                bytesum = ord(hardstop_buffer[0]) + ord( hardstop_buffer[1])
                if ord(hardstop_buffer[0]) == msg[0] and bytesum == checksum:
                    return True
                else:
                    print("Error with set_hardstop_time response")
                    return False
            else:
                print("Error receiving set_hardstop_time response")
                return False   

    def set_fan_control(self, enable=True):
        """
        set_fan_control          
        [0X53][enable]
        receive:                          
        [0x53][Sent_number][Chsum_H][Chsum_L]

        :return True/False:
        """
        msg = [0x58]
        msg.append(1 if enable else 0)

        if self.s.send_command(bytearray(msg)) != 2:
            print("Error sending set_fan_control")
            return False
        else:
            fan_buffer = self.s.read_command(4)
            if len(fan_buffer) == 4:
                checksum = self.s.to_num(fan_buffer[2:4])
                bytesum = ord(fan_buffer[0]) + ord( fan_buffer[1])
                if ord(fan_buffer[0]) == msg[0] and bytesum == checksum:
                    return True
                else:
                    print("Error with set_fan_control response")
                    return False
            else:
                print("Error receiving set_fan_control response")
                return False   

    #######################
    #     GET methods     #
    #######################
    def get_velocity_motor_ticks(self):
        """
            Get_Velocity_Motor_Ticks             
            [0X4A]                                 
            receive:                               
            [0x4A][Frt_Lf_Tick_H][Frt_Lf_Tick_L]   
                  [Frt_Rt_Tick_H][Frt_Rt_Tick_L]   
                  [Mid_Lf_Tick_H][Mid_Lf_Tick_L]   
                  [Mid_Rt_Tick_H][Mid_Rt_Tick_L]   
                  [Rear_Lf_Tick_H][Rear_Lf_Tick_L] 
                  [Rear_Rt_Tick_H][Rear_Rt_Tick_L] 
                  [Sent_number][Chsum_H][Chsum_L]  

        :return:
        """
        msg = [0x4A]
        if self.s.send_command(bytearray(msg)) != 1:
            print("Error reading motor ticks")
            return False
        else:
            ticks_buffer = self.s.read_command(16)
            if len(ticks_buffer) == 16:
                checksum = self.s.to_num(ticks_buffer[-2:])
                bytesum = sum([ord(a) for a in ticks_buffer[0:-2]])
                if ord(ticks_buffer[0]) == msg[0] and bytesum == checksum:
                    self.front_left_ticks += self.s.to_num(ticks_buffer[1:3])
                    self.front_right_ticks += self.s.to_num(ticks_buffer[3:5])
                    self.mid_left_ticks += self.s.to_num(ticks_buffer[5:7])
                    self.mid_right_ticks += self.s.to_num(ticks_buffer[7:9])
                    self.back_left_ticks += self.s.to_num(ticks_buffer[9:11])
                    self.back_right_ticks += self.s.to_num(ticks_buffer[11:13])
                    return [self.front_left_ticks, self.front_right_ticks, self.mid_left_ticks,
                            self.mid_right_ticks, self.back_left_ticks, self.back_right_ticks]
                else:
                    print("Error with motor ticks response")
                    return False
            else:
                print("Error receiving motor ticks response: len={}".format(len(ticks_buffer)))
                print("R: "),
                for c in ticks_buffer:
                    print("{} ".format(ord(c))),
                print("")
                return False

    def get_linear_motor_position(self):
        """
            get_linear_motor_position             
            [0x32]                                 
            receive:                               
            [0x32][Pos_H][Pos_L]   
                  [Sent_number][Chsum_H][Chsum_L]  

        :return:
        """
        msg = [0x32]
        if self.s.send_command(bytearray(msg)) != 1:
            print("Error reading linear motor ticks")
            return False
        else:
            ticks_buffer = self.s.read_command(6)
            if len(ticks_buffer) == 6:
                checksum = self.s.to_num(ticks_buffer[-2:])
                bytesum = sum([ord(a) for a in ticks_buffer[0:-2]])
                if ord(ticks_buffer[0]) == msg[0] and bytesum == checksum:
                    self.linear_motor_position = self.s.to_num(ticks_buffer[1:3])
                    return self.linear_motor_position
                else:
                    print("Error with linear motor ticks response")
                    return False
            else:
                print("Error receiving linear motor ticks response: len={}".format(len(ticks_buffer)))
                print("R: "),
                for c in ticks_buffer:
                    print("{} ".format(ord(c))),
                print("")
                return False

    def get_linear_motor_potentiometer(self):
        """
            get_linear_motor_potentiometer             
            [0x33]                                 
            receive:                               
            [0x33][Pot_H][Pot_L]   
                  [Sent_number][Chsum_H][Chsum_L]  

        :return:
        """
        msg = [0x33]
        if self.s.send_command(bytearray(msg)) != 1:
            print("Error reading get_linear_motor_potentiometer")
            return False
        else:
            ticks_buffer = self.s.read_command(6)
            if len(ticks_buffer) == 6:
                checksum = self.s.to_num(ticks_buffer[-2:])
                bytesum = sum([ord(a) for a in ticks_buffer[0:-2]])
                if ord(ticks_buffer[0]) == msg[0] and bytesum == checksum:
                    self.linear_motor_potentiometer = self.s.to_num(ticks_buffer[1:3])
                    return self.linear_motor_potentiometer
                else:
                    print("Error with get_linear_motor_potentiometer response")
                    return False
            else:
                print("Error receiving get_linear_motor_potentiometer response: len={}".format(len(ticks_buffer)))
                return False

    def get_hardstop_status(self):
        """
            get_hardstop_status             
            [0x59]                                 
            receive:                               
            [0x59][hardstop_status]   
                  [Sent_number][Chsum_H][Chsum_L]  

        :return:
        """
        msg = [0x59]
        if self.s.send_command(bytearray(msg)) != 1:
            print("Error reading get_hardstop_status")
            return False
        else:
            ticks_buffer = self.s.read_command(5)
            if len(ticks_buffer) == 5:
                checksum = self.s.to_num(ticks_buffer[-2:])
                bytesum = sum([ord(a) for a in ticks_buffer[0:-2]])
                if ord(ticks_buffer[0]) == msg[0] and bytesum == checksum:
                    self.hardstop_status = ord(ticks_buffer[1])
                    return self.hardstop_status
                else:
                    print("Error with get_hardstop_status response")
                    return False
            else:
                print("Error receiving get_hardstop_status response: len={}".format(len(ticks_buffer)))
                return False

    def get_hardstop_timer(self):
        """
            get_hardstop_timer         
            [0x58]                                 
            receive:                               
            [0x58][hardstop_timer]   
                  [Sent_number][Chsum_H][Chsum_L]  

        :return:
        """
        msg = [0x59]
        if self.s.send_command(bytearray(msg)) != 1:
            print("Error reading get_hardstop_timer")
            return False
        else:
            ticks_buffer = self.s.read_command(5)
            if len(ticks_buffer) == 5:
                checksum = self.s.to_num(ticks_buffer[-2:])
                bytesum = sum([ord(a) for a in ticks_buffer[0:-2]])
                if ord(ticks_buffer[0]) == msg[0] and bytesum == checksum:
                    self.hardstop_timer = ord(ticks_buffer[1])
                    return self.hardstop_timer
                else:
                    print("Error with get_hardstop_timer response")
                    return False
            else:
                print("Error receiving get_hardstop_timer response: len={}".format(len(ticks_buffer)))
                return False

    def get_fan_control(self):
        """
            get_fan_control             
            [0x54]                                 
            receive:                               
            [0x54][fan_control_status]   
                  [Sent_number][Chsum_H][Chsum_L]  

        :return:
        """
        msg = [0x54]
        if self.s.send_command(bytearray(msg)) != 1:
            print("Error reading get_fan_control")
            return False
        else:
            ticks_buffer = self.s.read_command(5)
            if len(ticks_buffer) == 5:
                checksum = self.s.to_num(ticks_buffer[-2:])
                bytesum = sum([ord(a) for a in ticks_buffer[0:-2]])
                if ord(ticks_buffer[0]) == msg[0] and bytesum == checksum:
                    self.fan_control = ord(ticks_buffer[1])
                    return self.fan_control
                else:
                    print("Error with get_fan_control response")
                    return False
            else:
                print("Error receiving get_fan_control response: len={}".format(len(ticks_buffer)))
                return False

    def get_firmware(self):
        """
            Get_firmware_version_number        
            [0X20]                                 
            receive:                               
            [0x20][FirmWare-> 25 Bytes]            
            [Sent_number][Chsum_H][Chsum_L]        
            where FirmWare:                        
             "Board2 fw 1.00 2017/07/12"                   
        """

        msg = [0x20]
        if self.s.send_command(bytearray(msg)) != 1:
            if self.verbose > 5:
                print("Error reading firmware")
            return False
        else:
            firmware_buffer = self.s.read_command(29)
            if len(firmware_buffer) == 29:
                checksum = self.s.to_num(firmware_buffer[-2:])
                bytesum = sum([ord(a) for a in firmware_buffer[0:-2]])
                if ord(firmware_buffer[0]) == 0x20 and bytesum == checksum:
                    self.firmware = str(firmware_buffer[1:26])
                    return self.firmware
                else:
                    if self.verbose > 5:
                        print("Error with reading firmware response")
                    return False
            else:
                if self.verbose > 5:
                    print("Error receiving firmware response")
                return False


if __name__ == "__main__":
    s = Motors()
