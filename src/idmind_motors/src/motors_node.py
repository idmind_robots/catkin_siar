#!/usr/bin/env python

import rospy
from numpy import pi
from motors_driver import Motors
from std_msgs.msg import Float64, Bool
from sensor_msgs.msg import JointState
from idmind_messages.msg import Log, Velocities, Encoders
from idmind_messages.srv import SetCenter, SetCenterResponse
from std_srvs.srv import Trigger, TriggerResponse, TriggerRequest, SetBool, SetBoolResponse

VERBOSE = 7
LOGS = 7
MAX_TRIES = 10


class MotorNode:
    """
    Node responsible for interface between ROS and motors drivers.
    """
    def __init__(self):
        self.ready = False
        self.control_freq = rospy.get_param("/bot/control_freq", 30)
        self.logging = rospy.Publisher("/idmind_logging", Log, queue_size=10)
        self.board = None
        if not self.connect():
            raise Exception("Failed to connect to board")
        self.fails = 0

        #########
        #  ROS  #
        #########
        # Variables 
        self.wheel_radius = rospy.get_param("idmind_motors/wheel_radius", 0.125)
        self.meter_tick = rospy.get_param("idmind_motors/meter_tick", 1. / 190000) # This gives linear displacement per tick
        self.last_encoders = [0]*6
        self.last_encoders_time = rospy.Time.now()
        self.velocities = [0.]*6
        self.new_center = False
        self.center_goal = 0.0
        self.new_hardstop = False
        self.hardstop = False
        self.new_fan = False
        self.fan = False

        # Subscribers
        rospy.Subscriber("idmind_motors/set_velocities", Velocities, self.handle_velocities)

        # Publishers
        # TODO: Publish in Joint State Publisher
        self.encoders_pub = rospy.Publisher("idmind_motors/encoders", Encoders, queue_size=10)
        self.joint_pub = rospy.Publisher("idmind_motors/joint_states", JointState, queue_size=10)
        self.center_pub = rospy.Publisher("idmind_motors/center", Float64, queue_size=10)
        self.width_pub = rospy.Publisher("idmind_motors/width", Float64, queue_size=10)
        # self.linear_pot_pub = rospy.Publisher("idmind_motors/linear_potentiometer", Float64, queue_size=10)
        self.hardstop_pub = rospy.Publisher("idmind_motors/hardstop", Bool, queue_size=10)
        self.fan_pub = rospy.Publisher("idmind_motors/fan_control", Bool, queue_size=10)
        
        # Services
        # rospy.Service("idmind_motors/set_center", SetCenter, self.handle_center)        
        rospy.Service("idmind_motors/set_hardstop", Trigger, self.handle_hardstop)
        rospy.Service("idmind_motors/set_fan", SetBool, self.handle_fan)

        self.ready = True
        rospy.Service("idmind_motors/ready", Trigger, self.report_ready)
        self.log("Node initiated", 3)

    #########################
    #  AUXILIARY FUNCTIONS  #
    #########################
    def log(self, msg, msg_level, log_level=-1, alert="info"):
        """
        Log function that publish in screen and in topic
        :param msg: Message to be published
        :param msg_level: Message level (1-10, where 1 is most important)
        :param log_level: Message level for logging (1-10, optional, -1 uses the same as msg_level)
        :param alert: Alert level of message - "info", "warn" or "error"
        :return:
        """
        if VERBOSE >= msg_level:
            if alert == "info":
                rospy.loginfo("{}: {}".format(rospy.get_name(), msg))
            elif alert == "warn":
                rospy.logwarn("{}: {}".format(rospy.get_name(), msg))
            elif alert == "error":
                rospy.logerr("{}: {}".format(rospy.get_name(), msg))
        if LOGS >= (log_level if log_level != -1 else msg_level):
            self.logging.publish(rospy.Time.now().to_sec(), rospy.get_name(), msg)

    def connect(self):
        """
        Auxiliary method that handles connection to the board.
        After connection, the motor drives should be enabled and halted
        :return :
        """
        while not rospy.is_shutdown():
            try:
                self.board = Motors(verbose=4)
                rospy.sleep(2)
                return True
            except KeyboardInterrupt:
                return False
        return True

    def reset_board(self):
        """
        Auxiliary method that handles a reset of the board.        
        :return:
        """
        self.ready = False
        try:
            self.board.close()
        except Exception:
            pass
        self.connect()
        self.ready = True
        self.fails = 0
        rospy.sleep(1)

    ###################
    #    CALLBACKS    #
    ###################
    def report_ready(self, _req):
        """ Simple Service callback to show node is ready """
        return TriggerResponse(self.ready, rospy.get_name()+" is " + ("ready" if self.ready else "not ready"))

    def handle_velocities(self, msg):
        """
        Callback that receives and stores individual wheel velocities in m/s
        :param msg:
        :return:
        """
        self.velocities = msg.velocities

    def handle_center(self, req):
        """
        Service callback that receives and stores goal width for the wheel linear motor
        :param msg:
        :return:
        """        
        self.center_goal = max(min(req.position, 0.205), -0.205)
        self.new_center = True
        return SetCenterResponse(True, "Center position is changing.")

    def handle_hardstop(self, _req):
        """        
        Service callback that enables hardstop
        :param msg:
        :return:
        """
        self.hardstop = True
        self.new_hardstop = True
        return TriggerResponse(True, "Hardstop has been enabled")

    def handle_fan(self, req):
        """        
        Service callback that enables hardstop
        :param msg:
        :return:
        """
        self.fan = req.data
        self.new_fan = True
        return SetBoolResponse(True, "Fan has been {}".format("enabled" if req.data else "disabled"))

    #########################
    #  Auxiliary functions  #
    #########################

    ####################
    #  Loop Functions  #
    ####################
    def update_data(self):
        """
        Method to be called at every loop, to update board info
        - Velocity Motor Ticks
        - Linear Motor Position
        - Linear Motor Potentiometer
        - Hardstop Status
        - Fan Control
        :return:
        """

        try:
            encoders = self.board.get_velocity_motor_ticks()
            if encoders:
                # [front_left, front_right, mid_left, mid_right, back_left, back_right]
                enc_msg = Encoders()
                enc_msg.encoders = encoders
                self.encoders_pub.publish(enc_msg)
            else:
                self.fails += 1
                self.log("Error reading encoders", 3, alert="error")
        except Exception as err:
            self.fails += 1
            self.log("Exception updating encoders {}".format(err), 3, alert="error")

        try:
            if encoders:
                msg = JointState()
                msg.name = ['front_left', 'front_right', 'mid_left', 'mid_right', 'back_left', 'back_right']
                curr_time = rospy.Time.now()
                for idx in range(0, len(encoders)):
                    msg.position.append(encoders[idx]*self.meter_tick/self.wheel_radius)
                    msg.velocity.append((encoders[idx]-self.last_encoders[idx])*self.meter_tick/(self.wheel_radius*(curr_time-self.last_encoders_time).to_sec()))
                msg.effort = [0]*6
                self.joint_pub.publish(msg)
                self.last_encoders = encoders
                self.last_encoders_time = curr_time

        except Exception as err:
            self.fails += 1
            self.log("Exception publishing to joint states: {}".format(err), 3, alert="error")

        """
        This block is disabled while the linear motor is not working
        try:
            e = self.board.get_linear_motor_position()            
            if e is not False:
                center = (e-39)/(-78/0.41)
                width = (0.32*e/40 + 0.53) if e < 40 else (-0.29*e/38 + 1.15)                
                self.center_pub.publish(Float64(center))                
                self.width_pub.publish(Float64(width))
            else:
                self.fails += 1
                self.log("Error reading linear encoders", 3, alert="error")
        except Exception as err:
            self.fails += 1
            self.log("Exception updating linear encoders {}".format(err), 3, alert="error")
        """
        self.center_pub.publish(Float64(0.05)) 
        self.width_pub.publish(Float64(0.76))

        try:
            e = self.board.get_hardstop_status()
            if e is not False:                                      
                self.hardstop_pub.publish(Bool(e))
            else:
                self.fails += 1
                self.log("Error reading hardstop status", 3, alert="error")
        except Exception as err:
            self.fails += 1
            self.log("Exception updating hardstop status: {}".format(err), 3, alert="error")

        try:
            e = self.board.get_fan_control()
            if e:                                      
                self.fan_pub.publish(Bool(e))
            else:
                self.fails += 1
                self.log("Error reading fan status", 3, alert="error")
        except Exception as err:
            self.fails += 1
            self.log("Exception updating fan status: {}".format(err), 3, alert="error")
        
        return True

    def send_commands(self):
        """
        Method to be called at every loop, to send commands to the board
        - Velocity Motor
        - Linear Motor Position
        - Hardstop Status
        - Fan Control
        :return:
        """
        
        # Ref is 1m/s=1671, but tops at 1100
        wheel_vel = [0]*6
        for i in range(0, 6):
            wheel_vel[i] = max(min(self.velocities[i] * 1671, 1100), -1100)
            if i in [1,3,5]:
                wheel_vel[i] = -wheel_vel[i]        
        self.board.set_motor_velocities_control(wheel_vel[0], wheel_vel[1],wheel_vel[2],wheel_vel[3],wheel_vel[4],wheel_vel[5])

        if self.new_center:
            # 0 -> 0.205, 78 -> -0.205
            lin_pos = int((78./2.) + self.center_goal * (-78./0.41))            
            self.new_center = not self.board.set_linear_motor_position_control(lin_pos)            

        if self.new_hardstop:
            self.new_hardstop = not self.board.set_hardstop()
        
        if self.new_fan:
            self.new_fan = not self.board.set_fan_control(self.fan)

        return True

    def shutdown(self):
        """
        Shutdown rotine. Only close the node after the steering motor and wheels have been disabled.
        :return:
        """

        self.board.close()

    def start(self):
        r = rospy.Rate(self.control_freq)
        while not rospy.is_shutdown():
            # If the connection drops, try to reconnect
            if self.fails > 10:
                self.reset_board()
            try:
                self.update_data()
                self.send_commands()
                r.sleep()
            except KeyboardInterrupt:
                self.log("Stopped by user", 1)
                r.sleep()
                break
            except Exception as err:
                self.log("Exception occurred: {}".format(err), 1)

        self.shutdown()


if __name__ == "__main__":
    rospy.init_node("idmind_motors")
    s = MotorNode()
    s.start()
    rospy.loginfo("{}: Shutting down.".format(rospy.get_name()))