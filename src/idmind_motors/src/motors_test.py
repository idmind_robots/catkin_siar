#!/usr/bin/env python

from motors_driver import Motors
import time

m = Motors()
print m.get_firmware()

print "#### MOTORS ###"
for motor in range(0, 6):
    v = [0]*6
    v[motor] = 200
    for i in range(0, 50):
        m.set_motor_velocities_control(v[0], v[1], v[2], v[3], v[4], v[5])
        print m.get_velocity_motor_ticks()
        time.sleep(0.1)

print "### LINEAR MOTOR ###"
print "Set Linear Motor Position to max width: {}".format(m.set_linear_motor_position_control(39))
while(abs(m.get_linear_motor_position()-39) > 3):
    time.sleep(1)
print "Set Linear Motor Position to min width: {}".format(m.set_linear_motor_position_control(0))
m.set_linear_motor_position_control(0)
while(abs(m.get_linear_motor_position()) > 3):
    time.sleep(1)


print "### HARDSTOP ###"
print "Set Hadstop timer to 5sec - {}".format(m.set_hardstop_time(5))
time.sleep(0.5)
print "Hardstop timer: {}".format(m.get_hardstop_timer())
print "Enabling Hardstop - {}".format(m.set_hardstop())
time.sleep(1)
print "\t Hardstop is {}".format("enabled" if m.get_hardstop_status() else "disabled")
while m.get_hardstop_status():
    time.sleep(0.1)
print "\t Hardstop is {}".format("enabled" if m.get_hardstop_status() else "disabled")    

# Fan Control?
print "### FAN CONTROL ###"
print "Disable Fan Control: {}".format(m.set_fan_control(False))
time.sleep(1)
print "\tFan Control is {}".format("enabled" if m.get_fan_control() else "disabled")
print "Enable Fan Control: {}".format(m.set_fan_control(True))
time.sleep(1)
print "\tFan Control is {}".format("enabled" if m.get_fan_control() else "disabled")
