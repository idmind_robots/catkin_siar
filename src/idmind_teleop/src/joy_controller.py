#!/usr/bin/env python

import rospy
from std_msgs.msg import String, Float32
from sensor_msgs.msg import Joy
from geometry_msgs.msg import Twist
from idmind_messages.msg import Log
from idmind_messages.srv import SetCenter, SetCenterRequest
from std_srvs.srv import Trigger, TriggerRequest, SetBool, SetBoolRequest

LOGS = 7
VERBOSE = 7

joy = {"x":
        {"buttons": {"A": 0, "B": 1, "X": 2, "Y": 3, "LB": 4,
                     "RB": 5, "BACK": 6, "START": 7, "LOGI": 8, "LJOY": 9, "RJOY": 10},
         "axis": {"XLJoy": 0, "YLJoy": 1, "LT": 2, "XRJoy": 3, "YRJoy": 4, "RT": 5, "XArrows": 6, "YArrows": 7}, },
       "d": {
        "buttons": {"X": 0, "A": 1, "B": 2, "Y": 3, "LB": 4,
                    "RB": 5, "LT": 6, "RT": 7, "BACK": 8, "START": 9, "LJOY": 10, "RJOY": 11},
        "axis": {"XArrows": 4, "YArrows": 5, "XRJoy": 2, "YRJoy": 3, "XLJoy": 0, "YLJoy": 1},
       }, }


class Teleop:
    """
    Class responsible for interpreting joystick commands
    """
    def __init__(self):

        # Robot parameters
        self.control_freq = 20
        self.logging = rospy.Publisher("/idmind_logging", Log, queue_size=10)

        self.log("Loading params from /idmind_motors", 5)
        self.kinematics = rospy.get_param("/bot/kinematics", default="6wd")
        self.max_vel = rospy.get_param("idmind_motors/max_vel", default=0.5)
        self.curr_max_vel = self.max_vel
        self.curr_max_rot = self.max_vel
        self.max_rot = rospy.get_param("idmind_motors/max_vel", default=0.5)

        ################
        #  Navigation  #
        ################
        self.twist = Twist()
        self.last_joy = Joy()            
        self.twist_pub = rospy.Publisher("idmind_navigation/cmd_vel", Twist, queue_size=10)            

        rospy.Subscriber("joy", Joy, self.update_joy)
        self.start_motors_trigger = False
        self.stop_motors_trigger = False
        self.halt_motors_trigger = False
        self.unhalt_motors_trigger = False
        self.base_center = 0
        self.new_base_center = False
        #self.base_center_service = rospy.ServiceProxy("idmind_motors/set_center", SetCenter)

        self.log("Node is initialized".format(rospy.get_name()), 5)

    #########################
    #  AUXILIARY FUNCTIONS  #
    #########################
    def log(self, msg, msg_level, log_level=-1, alert="info"):
        """
        Log function that publish in screen and in topic
        :param msg: Message to be published
        :param msg_level: Message level (1-10, where 1 is most important)
        :param log_level: Message level for logging (1-10, optional, -1 uses the same as msg_level)
        :param alert: Alert level of message - "info", "warn" or "error"
        :return:
        """
        if VERBOSE >= msg_level:
            if alert == "info":
                rospy.loginfo("{}: {}".format(rospy.get_name(), msg))
            elif alert == "warn":
                rospy.logwarn("{}: {}".format(rospy.get_name(), msg))
            elif alert == "error":
                rospy.logerr("{}: {}".format(rospy.get_name(), msg))
        if LOGS >= (log_level if log_level != -1 else msg_level):
            self.logging.publish(rospy.Time.now().to_sec(), rospy.get_name(), msg)

    ###############
    #  CALLBACKS  #
    ###############
    def update_joy(self, msg):
        # Define which mode the joystick is using
        if len(msg.axes) == 8:
            mode = "x"
        elif len(msg.axes) == 6:
            mode = "d"
        else:
            self.log("{}: Incorrect joy message type".format(rospy.get_name()), 2, alert="error")
            return

        # Translate command to robot velocities - Added a "dead man switch"
        new_vel = Twist()
        new_vel.linear.x = msg.axes[joy[mode]["axis"]["YLJoy"]] * self.curr_max_vel
        new_vel.linear.y = msg.axes[joy[mode]["axis"]["XLJoy"]] * self.curr_max_vel
        new_vel.angular.z = msg.axes[joy[mode]["axis"]["XRJoy"]] * self.curr_max_rot
        if msg.buttons[joy[mode]["buttons"]["Y"]] and not self.last_joy.buttons[joy[mode]["buttons"]["Y"]]:
            self.curr_max_vel = min(2*self.curr_max_vel, self.max_vel)
            self.curr_max_rot = min(2*self.curr_max_rot, self.max_rot)
        else:
            self.curr_max_vel = 0.5

        self.twist = new_vel        

        # SIAR SPECIFIC COMMANDS
        if msg.buttons[joy[mode]["buttons"]["START"]]:
            self.log("Activated motors", 2)
            self.start_motors_trigger = True
        if msg.buttons[joy[mode]["buttons"]["BACK"]]:
            self.log("Stopped motors", 2)
            self.twist_pub.publish(Twist())
            self.start_motors_trigger = False
        # Width Control ranges [-0.205, 0.205]
        #if self.start_motors_trigger:
        #    # Maximum width
        #    if msg.buttons[joy[mode]["buttons"]["X"]] and not self.new_base_center:
        #        self.base_center = 0.0
        #        self.new_base_center = True
        #    # Fine control
        #    elif msg.axes[joy[mode]["axis"]["YArrows"]] and not self.new_base_center:
        #        self.base_center += 0.05*msg.axes[joy[mode]["axis"]["YArrows"]]
        #        self.base_center = max(min(self.base_center, 0.205), -0.205)
        #        self.new_base_center = True

        # Save the current msg in order to detect rise/fall events of buttons
        self.last_joy = msg

    def start(self):
        r = rospy.Rate(self.control_freq)
        while not rospy.is_shutdown():
            try:
                if self.start_motors_trigger:
                    self.twist_pub.publish(self.twist)                
                #if self.new_base_center:
                #    req = SetCenterRequest()
                #    req.position = self.base_center
                #    self.base_center_service(req)
                #    self.new_base_center = False
                r.sleep()
            except KeyboardInterrupt or rospy.exceptions.ROSInterruptException:
                self.log("Node shutting down", 3, alert="warn")
                break


if __name__ == "__main__":
    rospy.init_node("idmind_teleop")
    t = Teleop()
    t.start()
